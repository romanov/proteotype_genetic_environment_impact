import sys
sys.path.append('/g/bork1/romanov/scripts/')
from wp_import_modules import *
from utils import *

import sklearn
import matplotlib as mpl
mpl.rcParams["axes.grid"]=True;mpl.rcParams["axes.facecolor"]="white"
mpl.rcParams["grid.color"]="#E7EBEF";mpl.rcParams["grid.linestyle"]="solid";mpl.rcParams["grid.alpha"]=0.7
plt.rcParams['svg.fonttype'] = 'none'


class supp_figure1a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		make_density_graph_plot = kwargs.get("make_density_graph",True)
		make_boxplot_plot = kwargs.get("make_boxplot",True)

		nameList = ["gygi1","gygi3","battle_protein",
					"wu","mann_all_log2","yibo","tiannan",
					"primateRNA","primatePRO","nci60",
					"gygi2","battle_rna","battle_ribo",
					'coloCa','tcga_breast','tcga_ovarian',
					'bxd_protein']

		print("load_data")
		corr_dict = supp_figure1a.load_data(nameList, output_folder)
		print('make_supplementary_plot')
		supp_figure1a.make_supplementary_plot(nameList, corr_dict, output_folder)

		'''
		print("get_significancies")
		pval_dict, new_pval_dict = supp_figure1a.get_significancies(nameList, output_folder)
		'''

	@staticmethod
	def load_data(nameList, output_folder):
		folder = output_folder

		corr_dict = dict()
		for name in nameList:
			print(name)
			for ty in ["all","700","other"]:
				print(ty)
				if ty=="all" or ty=="700":
					if name == 'mann_all_log2' or name=='tiannan':
						file_name = "data/string_correlations_FINAL" + ty + "_" + name + ".json"
					else:
						file_name = "data/string_correlations_" + ty + "_" + name + ".json"
					with open(folder + file_name) as data_file:    
						correlation_values = json.load(data_file)
					corr_dict.setdefault(name + ":" + ty,[])
					corr_dict[name + ":" + ty] = correlation_values
				else:
					if name == 'mann_all_log2' or name=='tiannan':
						file_name = "data/other_string_correlations_FINALall_" + name + ".json"
					else:
						file_name = "data/other_string_correlations_all_" + name + ".json"
					with open(folder + file_name) as data_file:    
						correlation_values = json.load(data_file)
					corr_dict.setdefault(name + ":" + ty,[])
					corr_dict[name + ":" + ty] = correlation_values
		return corr_dict

	@staticmethod
	def make_supplementary_plot(nameList, corr_dict, output_folder):
		folder = output_folder

		for name in nameList:
			print(name)
			dataList = list()
			for ty in ["700","all","other"]:
				key = name + ":" + ty
				dataList.append(corr_dict[key])
				if ty == '700':
					best_string_correlation_values = list(corr_dict[key])
				elif ty == 'all':
					string_correlation_values = list(corr_dict[key])
				else:
					other_correlation_values = list(corr_dict[key])

			'''
			with open(folder + "string_correlations_all_" + name + ".json") as data_file:    
				string_correlation_values = json.load(data_file)
			with open(folder + "string_correlations_700_" + name + ".json") as data_file:    
				best_string_correlation_values = json.load(data_file)
			with open(folder + "other_string_correlations_all_" + name + ".json") as data_file:    
				other_correlation_values = json.load(data_file)
			'''

			sns.set_style("white")
			plt.rcParams["axes.grid"] = True

			plt.clf()
			fig = plt.figure(figsize=(5,5))
			gs = gridspec.GridSpec(10,10)
			ax = plt.subplot(gs[0:7,0:])
			plottingFacade.func_plotDensities_border(ax, other_correlation_values, facecolor="grey")
			plottingFacade.func_plotDensities_border(ax, string_correlation_values, facecolor="orange")
			plottingFacade.func_plotDensities_border(ax, best_string_correlation_values, facecolor="#EE7600")
			ax.set_xlim(-1,1)
			ax.set_xticklabels([])
			plt.tick_params(axis="y",which="both",left="off",right="off",labelsize=10)
		
			ax = plt.subplot(gs[7:,0:])
			bp = ax.boxplot(dataList,notch=0,sym="",vert=0,patch_artist=True,widths=(0.5,0.5,0.5))
			plt.setp(bp['medians'], color="black")
			plt.setp(bp['whiskers'], color="black",linestyle="-")
			for i,patch in enumerate(bp['boxes']):
				if i==0:
					patch.set_facecolor("#EE7600")	
				elif i==1:
					patch.set_facecolor("orange")	
				else:
					patch.set_facecolor("#D8D8D8")	
				patch.set_edgecolor("black")
				patch.set_alpha(1)
			ax.set_xlim(-1,1)
			ax.set_yticklabels([])
			plt.tick_params(axis="y",which="both",left="off",right="off",labelsize=15)
			plt.savefig(output_folder + "figures/suppFig1a_string_correlation_recovery_" + name + ".pdf",
						bbox_inches="tight", dpi = 400)

	@staticmethod
	def get_significancies(nameList, output_folder):
		folder = output_folder

		pval_dict = dict()
		for name in nameList:
			print(name)
			dataList = list()
			for ty in ["700","all","other"]:
				key = name + ":" + ty
				dataList.append(corr_dict[key])
				if ty == '700':
					best_string_correlation_values = list(corr_dict[key])
				elif ty == 'all':
					string_correlation_values = list(corr_dict[key])
				else:
					other_correlation_values = list(corr_dict[key])

			pval_distribution_mann = list()
			for i in xrange(1,1000):
				pval_mann_all = scipy.stats.mannwhitneyu(random.sample(other_correlation_values,1000),
														 random.sample(string_correlation_values,1000))[1]
				pval_distribution_mann.append(pval_mann_all)

			pval_distribution_mann_best = list()
			for i in xrange(1,1000):
				pval_mann_best = scipy.stats.mannwhitneyu(random.sample(other_correlation_values,1000),
														  random.sample(best_string_correlation_values,1000))[1]
				pval_distribution_mann_best.append(pval_mann_best)
			print(np.mean(pval_distribution_mann))
			print(np.mean(pval_distribution_mann_best))

			pval_distribution_ttest = list()
			pval_distribution_wc = list()
			for i in xrange(1,10000):
				pval_ttest_all = scipy.stats.ttest_ind(random.sample(other_correlation_values,1000),
													   random.sample(string_correlation_values,1000))[1]
				pval_wc_all = scipy.stats.ranksums(random.sample(other_correlation_values,1000),
												   random.sample(string_correlation_values,1000))[1]
				pval_distribution_wc.append(pval_wc_all)
				pval_distribution_ttest.append(pval_ttest_all)
			pvalCorrs_wc = utilsFacade.correct_pvalues(pval_distribution_wc)
			pvalCorrs_ttest = utilsFacade.correct_pvalues(pval_distribution_ttest)
			pval_dict.setdefault(name + ":all",[]).append({"wc":pvalCorrs_wc,
														   "ttest":pvalCorrs_ttest})

			pval_distribution_ttest = list()
			pval_distribution_wc = list()
			for i in xrange(1,10000):
				pval_ttest_all = scipy.stats.ttest_ind(random.sample(other_correlation_values,1000),
													   random.sample(best_string_correlation_values,1000))[1]
				pval_wc_all = scipy.stats.ranksums(random.sample(other_correlation_values,1000),
												   random.sample(best_string_correlation_values,1000))[1]
				pval_distribution_wc.append(pval_wc_all)
				pval_distribution_ttest.append(pval_ttest_all)
			pvalCorrs_wc = utilsFacade.correct_pvalues(pval_distribution_wc)
			pvalCorrs_ttest = utilsFacade.correct_pvalues(pval_distribution_ttest)
			pval_dict.setdefault(name + ":700",[]).append({"wc":pvalCorrs_wc,
														   "ttest":pvalCorrs_ttest})

		new_pval_dict = dict()
		for key in pval_dict:
			ttest = list(pval_dict[key][0]["ttest"])
			wc = list(pval_dict[key][0]["wc"])
			new_pval_dict.setdefault(key,[])
			new_pval_dict[key].append({"ttest":ttest,"wc":wc})
		with open(output_folder + 'suppFig1A_stringRecovery_correlations_pvalues.json', 'w') as outfile:
		    json.dump(new_pval_dict, outfile)

		for key in sorted(pval_dict.keys()):
			wc_correlation_values = pval_dict[key][0]["wc"]
			ttest_correlation_values = pval_dict[key][0]["ttest"]
			print(key,"ttest",max(ttest_correlation_values))
			print(key,"wc",max(wc_correlation_values))
		return pval_dict,new_pval_dict

	@staticmethod
	def read_significancies(output_folder):
		with open(output_folder + 'suppFig1A_stringRecovery_correlations_pvalues.json', 'rb') as infile:
		    pval_dict = json.load(infile)

class supp_figure2a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		
		print('SUPP-FIGURE2A: main_supp_figure2a_bins_boxplots_effect')
		supp_figure2a.main_supp_figure2a_bins_boxplots_effect(output_folder = output_folder)

	@staticmethod
	def main_supp_figure2a_bins_boxplots_effect(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/')

		print('load_data')
		data_dict = supp_figure2a.load_data(output_folder)

		print('make_boxplots')
		supp_figure2a.make_boxplots(data_dict, output_folder)

	@staticmethod
	def load_data(folder):
		complex_data_mann = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_mann_all_log2_complex_data.tsv.gz')
		complex_data_gygi2 = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_gygi2_complex_data.tsv.gz')
		complex_data_gygi3 = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_gygi3_complex_data.tsv.gz')
		complex_data_gygi1 = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_gygi1_complex_data.tsv.gz')
		complex_data_bp = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_battle_protein_complex_data.tsv.gz')
		complex_data_br = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_battleRNA_complex_data.tsv.gz')
		complex_data_bribo = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_battleRibo_complex_data.tsv.gz')
		complex_data_coloCancer = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_coloCancer_complex_data.tsv.gz')
		complex_data_breastCancer = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_breastCancer_complex_data.tsv.gz')
		complex_data_ovarianCancer = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFig2a_wpc_ovarianCancer_complex_data.tsv.gz')
		return {'mann':complex_data_mann, 'gygi2':complex_data_gygi2,
				'gygi3':complex_data_gygi3, 'gygi1':complex_data_gygi1,
				'bp': complex_data_bp, 'br':complex_data_br,
				'bribo': complex_data_bribo, 'colo': complex_data_coloCancer,
				'breast':complex_data_breastCancer,
				'ovarian':complex_data_ovarianCancer}
	
	@staticmethod
	def bin_abundances_variances(complex_data, keyword):
		complex_data = complex_data.copy()
		complex_data = complex_data.T
		complex_data = complex_data.sort_values(keyword)

		zscore_list = list(complex_data[keyword])
		corr_list = list(complex_data['corr'])
		bin_list = list()
		for i in xrange(0,5):
			bin_list.append([(i)*20,(20+(i)*20)])
		bin_list = [(0,25),(25,50),(50,75),(75,100)]
		zscore_dict = dict()
		for binL in bin_list:
			c1,c2 = binL
			temp = corr_list[c1:c2]
			zscore_dict.setdefault(str(c1)+':'+str(c2),[]).append(utilsFacade.finite(temp))
		return zscore_dict

	@staticmethod
	def make_boxplots(data_dict, folder):
		complex_data_gygi3 = data_dict['gygi3']
		complex_data_gygi1 = data_dict['gygi1']
		complex_data_gygi2 = data_dict['gygi2']
		complex_data_bp = data_dict['bp']
		complex_data_br = data_dict['br']
		complex_data_bribo = data_dict['bribo']
		complex_data_mann = data_dict['mann']
		complex_data_coloCancer = data_dict['colo']
		complex_data_breastCancer = data_dict['breast']
		complex_data_ovarianCancer = data_dict['ovarian']

		bin_list = list()
		for i in xrange(0,5):
			bin_list.append(str((i)*20)+':'+str((20+(i)*20)))
		bin_list = ['0:25','25:50','50:75','75:100']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		name_list = ['gygi1','gygi2','gygi3',
					 'battleRNA','battleRibo',
					 'battle_protein','mann_all_log2',
					 'coloCancer','breastCancer',
					 'ovarianCancer']
		data_list = [complex_data_gygi1, complex_data_gygi2,
					 complex_data_gygi3, complex_data_br,
					 complex_data_bribo, complex_data_bp, complex_data_mann,
					 complex_data_coloCancer, complex_data_breastCancer,
					 complex_data_ovarianCancer]

		for n,d in zip(name_list, data_list):
			d = d.T
			d = d[d.num>=0]
			d = d.T

			print(n)
			zscore_dict_abundances = supp_figure2a.bin_abundances_variances(d,'zscore')
			zscore_dict_variances = supp_figure2a.bin_abundances_variances(d,'var_zscore')

			dataList = list()
			var_dataList = list()
			median_abundances = list()
			median_variances = list()
			for dbin in bin_list:
				dataList.append(zscore_dict_abundances[dbin][0])
				var_dataList.append(zscore_dict_variances[dbin][0])
				median_abundances.append(np.mean(zscore_dict_abundances[dbin][0]))
				median_variances.append(np.mean(zscore_dict_variances[dbin][0]))

			positions = list(np.arange(1,len(bin_list)/5.0+5,0.5)[0:(len(bin_list))])
			widths = [0.4]*len(bin_list)

			plt.clf()
			fig = plt.figure(figsize = (7,4))
			ax = fig.add_subplot(121)
			bp = ax.boxplot(dataList,notch=0,sym="",vert=1,
							patch_artist=True,widths=widths,
							positions = positions)
			plt.setp(bp['medians'], color="black")
			plt.setp(bp['whiskers'], color="black")
			nList = list()
			for i,patch in enumerate(bp['boxes']):
				patch.set_facecolor("lightgrey")	
				patch.set_edgecolor("black")
				patch.set_alpha(0.6)
				sub_group = dataList[i]
				x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
				ax.scatter(x,sub_group, color='white', alpha=0.9,edgecolor="black",s=40)
				nList.append(len(dataList[i]))
			pvals = list()
			combiList = utilsFacade.getCombinations(np.arange(0,len(dataList),1))
			for combi in combiList:
				c1,c2 = combi
				pval = scipy.stats.ttest_ind(dataList[c1],dataList[c2])[1]
				pvals.append(str(c1)+':'+str(c2)+'='+str(pval))
			ax.set_title('\n'.join(pvals), fontsize =5)
			ax.set_ylim(-0.5,1)
			ax.set_xticklabels(nList)

			ax = fig.add_subplot(122)
			bp = ax.boxplot(var_dataList,notch=0,sym="",vert=1,
							patch_artist=True,widths=widths,
							positions = positions)
			plt.setp(bp['medians'], color="black")
			plt.setp(bp['whiskers'], color="black")
			nList = list()
			for i,patch in enumerate(bp['boxes']):
				patch.set_facecolor("lightgrey")	
				patch.set_edgecolor("black")
				patch.set_alpha(0.6)
				sub_group = var_dataList[i]
				x = numpy.random.normal(positions[i], 0.04, size=len(var_dataList[i]))
				ax.scatter(x,sub_group, color='white', alpha=0.9,edgecolor="black",s=40)
				nList.append(len(dataList[i]))
			df_var = d.T[['corr','var_zscore']]
			df_var = df_var.dropna()
			pvals = list()
			combiList = utilsFacade.getCombinations(np.arange(0,len(var_dataList),1))
			for combi in combiList:
				c1,c2 = combi
				pval = scipy.stats.ttest_ind(var_dataList[c1],var_dataList[c2])[1]
				pvals.append(str(c1)+':'+str(c2)+'='+str(pval))
			ax.set_title('\n'.join(pvals), fontsize =5)
			ax.set_ylim(-0.5,1)
			ax.set_xticklabels(nList)

			plt.savefig(folder + 'figures/suppFig2a_boxplots_overview_' + n + '.pdf',
						bbox_inches = 'tight', dpi = 400)

class supp_figure2b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		num_subunits = kwargs.get('num_subunits',0)
		
		print('SUPP-FIGURE2B: main_supp_figure2b_boxplot_randomControl')
		supp_figure2b.main_supp_figure2b_boxplot_randomControl(output_folder = output_folder,
															   num_subunits = num_subunits)

	@staticmethod
	def main_supp_figure2b_boxplot_randomControl(**kwargs):
		#output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/')
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		num_subunits = kwargs.get('num_subunits',0)

		print('prepare_data_for_boxplot')
		bp_data_dict = supp_figure2b.prepare_data_for_boxplot(output_folder,
					   num_subunits = num_subunits)
		print('plot_boxplot')
		supp_figure2b.plot_boxplot(bp_data_dict, output_folder)

	@staticmethod
	def get_randomized_complex_set(data, name, output_folder):
		all_random_corrDict = dict()
		all_random_dict = dict()
		
		if name == 'mann':
			data = wp.com_mann_all_log2.copy()
			quant_cols = utilsFacade.filtering(list(data.columns), 'quant_')
			qdata = data[quant_cols]
			qdata = utilsFacade.quantileNormalize2(qdata)
			qdata['ComplexName'] = pd.Series(list(data.ComplexName), index = qdata.index)
			data = qdata.copy()

		d = data.copy()
		n = name
		complexSet = list(set(d['ComplexName']))
		quant_list = utilsFacade.filtering(d, 'quant', condition = 'startswith')
		quant_data = d[quant_list]
		all_random_corrs = list()
		all_random_corrDict = dict((e1,list()) for e1 in complexSet)
		count = 0
		for complexID in complexSet:
			print(n, count, complexID)
			sub = d[d.ComplexName==complexID]
			complex_proteins = list(set(sub.index))
			other_sub = d[~d.index.isin(complex_proteins)]
			other_proteins = list(other_sub.index)
			corr_list = list()
			for i in xrange(0,10):
				random_proteins = random.sample(other_proteins, len(complex_proteins))
				quant_sub = quant_data.T[random_proteins].T
				quant_sub = quant_sub.drop_duplicates()
				corr_data = quant_sub.T.corr()
				corr_values = utilsFacade.get_correlation_values(corr_data)
				corr_list.append(np.median(corr_values))
			all_random_corrs.append(corr_list)
			all_random_corrDict[complexID] = corr_list
			count+=1
		all_random_corrs = utilsFacade.flatten(all_random_corrs)

		all_random_corrDict['ALL'] = utilsFacade.finite(all_random_corrs)
		json.dump(all_random_corrDict, open(output_folder + n + '_randomized_complexSet_dict.json','wb'))
		return all_random_corrDict, all_random_corrs

	@staticmethod
	def get_randomized_complex_set_reshuffled_data(data, name, output_folder):
		all_random_corrDict = dict()
		all_random_dict = dict()

		if name == 'mann':
			data = wp.com_mann_all_log2.copy()
			quant_cols = utilsFacade.filtering(list(data.columns), 'quant_')
			qdata = data[quant_cols]
			qdata = utilsFacade.quantileNormalize2(qdata)
			qdata['ComplexName'] = pd.Series(list(data.ComplexName), index = qdata.index)
			data = qdata.copy()

		d = data.copy()
		n = name
		complexSet = list(set(d['ComplexName']))
		quant_list = utilsFacade.filtering(d, 'quant', condition = 'startswith')
		quant_data = d[quant_list]
		all_random_corrs = list()
		all_random_corrDict = dict((e1,list()) for e1 in complexSet)
		count = 0
		for complexID in complexSet:
			print(n, count, complexID)
			sub = d[d.ComplexName==complexID]
			complex_proteins = list(set(sub.index))
			other_sub = d[~d.index.isin(complex_proteins)]
			other_proteins = list(other_sub.index)
			corr_list = list()
			for i in xrange(0,10):
				random_proteins = random.sample(other_proteins, len(complex_proteins))
				quant_sub = quant_data.T[random_proteins].T
				quant_sub = quant_sub.drop_duplicates()
				df_list = list()
				for q,row in quant_sub.iterrows():
					temp = list(row)
					random.shuffle(temp)
					df_list.append(temp)
				new_quant_sub = pd.DataFrame(df_list)
				corr_data = new_quant_sub.T.corr()
				corr_values = utilsFacade.get_correlation_values(corr_data)
				corr_list.append(np.median(corr_values))
			all_random_corrs.append(corr_list)
			all_random_corrDict[complexID] = corr_list
			count+=1
		all_random_corrs = utilsFacade.flatten(all_random_corrs)

		all_random_corrDict['ALL'] = utilsFacade.finite(all_random_corrs)
		json.dump(all_random_corrDict,
			open(output_folder + n + '_randomized_complexSet_reshuffledData_dict.json','wb'))
		return all_random_corrDict, all_random_corrs

	@staticmethod
	def get_real_complex_set(data, name, output_folder):
		all_real_corrDict = dict()
		all_real_dict = dict()

		if name == 'mann':
			data = wp.com_mann_all_log2.copy()
			quant_cols = utilsFacade.filtering(list(data.columns), 'quant_')
			qdata = data[quant_cols]
			qdata = utilsFacade.quantileNormalize2(qdata)
			qdata['ComplexName'] = pd.Series(list(data.ComplexName), index = qdata.index)
			data = qdata.copy()

		d = data.copy()
		n = name
		complexSet = list(set(d['ComplexName']))
		quant_list = utilsFacade.filtering(d, 'quant', condition = 'startswith')
		quant_data = d[quant_list]
		all_real_corrs = list()
		all_real_corrDict = dict((e1,list()) for e1 in complexSet)
		count = 0
		for complexID in complexSet:
			print(n, count, complexID)
			sub = d[d.ComplexName==complexID]
			complex_proteins = list(set(sub.index))
			quant_sub = quant_data.T[complex_proteins].T
			quant_sub = quant_sub.drop_duplicates()
			corr_data = quant_sub.T.corr()
			corr_values = utilsFacade.get_correlation_values(corr_data)
			all_real_corrs.append(np.median(corr_values))
			all_real_corrDict[complexID] = np.median(corr_values)
			count+=1

		all_real_corrDict['ALL'] = utilsFacade.finite(all_real_corrs)
		json.dump(all_real_corrDict,open(output_folder + n + '_real_complexSet_dict.json','wb'))
		return all_real_corrDict, all_real_corrs

	@staticmethod
	def prepare_data_for_boxplot(output_folder, **kwargs):
		num_subunits = kwargs.get('num_subunits',0)
		name_list = ['gygi1','gygi2','gygi3','battle_rna',
					 'battle_ribo','battle_protein',
					 'mann','tcgaColo','tcgaBreast',
					 'tcgaOvarian']

		bp_data_dict = dict()
		for n in name_list:
			print(n)
			real_complexDict = json.load(open(output_folder + 'data/' + n + '_real_complexSet_dict.json','rb'))
			randomized_complexDict = json.load(open(output_folder + 'data/' + n + '_randomized_complexSet_dict.json','rb'))
			randomized_shuffled_complexDict = json.load(open(output_folder + 'data/' + n + '_randomized_complexSet_reshuffledData_dict.json','rb'))
			real_complexes = set(real_complexDict.keys())
			randomized_complexes = set(randomized_complexDict.keys())
			randomized_shuffled_complexes = set(randomized_shuffled_complexDict.keys())
			complexList = set.intersection(*[real_complexes, randomized_complexes, 
						  randomized_shuffled_complexes])

			randomized_shuffled_list = list()
			randomized_list = list()
			real_list = list()
			for complexID in complexList:
				if complexID!='ALL':
					randomized_shuffled = randomized_shuffled_complexDict[complexID]
					randomized = randomized_complexDict[complexID]
					real = real_complexDict[complexID]
					randomized_shuffled_list.append(randomized_shuffled)
					randomized_list.append(randomized)
					real_list.append(real)
			randomized_shuffled_list = utilsFacade.finite(utilsFacade.flatten(randomized_shuffled_list))
			randomized_list = utilsFacade.finite(utilsFacade.flatten(randomized_list))
			real_list = filter(lambda a:str(a)!='nan',real_list)

			bp_data_dict[n] = [randomized_shuffled_list, randomized_list, real_list]
		return bp_data_dict

	@staticmethod
	def plot_boxplot(bp_data_dict, output_folder):
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		name_list = ['gygi1','gygi2','gygi3','battle_rna',
					 'battle_ribo','battle_protein',
					 'mann','tcgaColo','tcgaBreast',
					 'tcgaOvarian']

		positions = [1,1.5,2]
		plt.clf()
		fig = plt.figure(figsize = (15,4))
		gs = gridspec.GridSpec(6,30)
		for n,name in enumerate(name_list):
			dataList = bp_data_dict[name]# [bp_data_dict[name][0], bp_data_dict[name][2]]
			ax = plt.subplot(gs[0:,(3*n):(3*n)+3])
			bp = ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
					widths=[0.45,0.45,0.45], positions = positions)
			plt.setp(bp['medians'], color="black")
			plt.setp(bp['whiskers'], color="black")
			pval1 = scipy.stats.ranksums(dataList[0], dataList[-1])[1]
			pval2 = scipy.stats.ranksums(dataList[1], dataList[-1])[1]
			print(name, pval1, pval2)

			for i,patch in enumerate(bp['boxes']):
				patch.set_facecolor("lightgrey")	
				patch.set_edgecolor("black")
				patch.set_alpha(0.6)
				x = numpy.random.normal(positions[i], 0.04, size=len(dataList[2]))
				ax.scatter(x, random.sample(dataList[i],len(dataList[2])), color='white',
						   alpha=0.9, edgecolor="black",s=10)
			ax.set_ylim(-0.7,1)
			if n>0:
				ax.set_yticklabels([])
		plt.savefig(output_folder + 'figures/suppFigure2B_boxplot_random_complex_variability_all.pdf',
					bbox_inches = 'tight', dpi = 400)

class supp_figure3:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")

		print('SUPP-FIGURE3: main_supp_figure2c_GOenrichment')
		supp_figure3.main_supp_figure2c_GOenrichment(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure2c_GOenrichment(**kwargs):
		folder = kwargs.get('folder',"/g/scb2/bork/romanov/wpc/wpc_package/")
		output_folder = kwargs.get('output_folder',"/g/scb2/bork/romanov/wpc/wpc_package/")

		print('load_data')
		go_dict = supp_figure3.load_data(output_folder)
		print('make_go_plot_scatter')
		supp_figure3.make_go_barplot(go_dict, output_folder)

	@staticmethod
	def load_input_df(output_folder):
		data_list = [wp.com_gygi1, wp.com_gygi2, wp.com_gygi3,
					 wp.com_battle_rna, wp.com_battle_ribo,
					 wp.com_battle_protein, wp.com_mann_all_log2,
					 wp.com_colo_cancer, wp.com_tcgaBreast,
					 wp.com_tcgaOvarian]
		df = DataFrameAnalyzer.getFile(output_folder ,'figure2B_underlying_data.tsv')

		mean_df = df.mean()
		stable_df = mean_df[mean_df>0]
		variable_df = mean_df[mean_df<0]
		complex_list = list(df.columns)
		stable_protein_list = list()
		stable_protein_dict = dict()

		for complexID in list(stable_df.index):
			print(complexID)
			protein_list = list()
			for d in data_list:
				sub = d[d.ComplexName==complexID]
				protein_list.append([item.upper() for item in list(sub.index)])
			protein_list = list(set(utilsFacade.flatten(protein_list)))
			stable_protein_dict[complexID] = protein_list
			stable_protein_list.append(protein_list)

		variable_protein_list = list()
		variable_protein_dict = dict()
		for complexID in list(variable_df.index):
			print(complexID)
			protein_list = list()
			for d in data_list:
				sub = d[d.ComplexName==complexID]
				protein_list.append([item.upper() for item in list(sub.index)])
			protein_list = list(set(utilsFacade.flatten(protein_list)))
			variable_protein_dict[complexID] = protein_list
			variable_protein_list.append(protein_list)

		o = open(output_folder + 'suppFigure2c_stable_protein_list.tsv','wb')
		stable_proteins = map(str,list(set(utilsFacade.flatten(stable_protein_list))))
		m = Mapper(stable_proteins, input = 'symbol', output = 'entrezgene')
		stable_entrezgenes = map(str,map(int,utilsFacade.finite(list(m.trans_df['entrezgene']))))
		exportText = '\n'.join(stable_entrezgenes)
		o.write(exportText)
		o.close()

		o = open(output_folder + 'suppFigure2c_variable_protein_list.tsv','wb')
		variable_proteins = map(str,list(set(utilsFacade.flatten(variable_protein_list))))
		m = Mapper(variable_proteins, input = 'symbol', output = 'entrezgene')
		variable_entrezgenes = map(str,map(int,utilsFacade.finite(list(m.trans_df['entrezgene']))))
		exportText = '\n'.join(variable_entrezgenes)
		o.write(exportText)
		o.close()

	@staticmethod
	def load_data(output_folder):
		category_list = ['GOTERM_BP_DIRECT','GOTERM_MF_DIRECT','GOTERM_CC_DIRECT']
		stable_data = DataFrameAnalyzer.getFile(output_folder,
					  'data/wpc_suppFigure2C_goEnrichment_stableProteins_DAVID6_8.txt')
		variable_data = DataFrameAnalyzer.getFile(output_folder,
						'data/wpc_suppFigure2C_goEnrichment_variableProteins_DAVID6_8.txt')

		stable_data = stable_data[stable_data.index.isin(category_list)]
		variable_data = variable_data[variable_data.index.isin(category_list)]
		#stable_data = stable_data[stable_data.FDR<=0.01]
		#variable_data = variable_data[variable_data.FDR<=0.01]
		go_dict = dict((e1,list()) for e1 in ['BP','CC','MF'])
		for category in category_list:
			stable_sub = stable_data[stable_data.index == category]
			variable_sub = variable_data[variable_data.index == category]

			stable_sub = stable_sub[stable_sub.Count>=50]
			stable_sub = stable_sub[stable_sub['%']>=10]

			variable_sub = variable_sub[variable_sub.Count>=50]
			variable_sub = variable_sub[variable_sub['%']>=10]

			term_list = [item.split('~')[1] for item in list(stable_sub['Term'])]
			stable_sub['go_term'] = pd.Series(term_list, index = stable_sub.index)
			term_list = [item.split('~')[1] for item in list(variable_sub['Term'])]
			variable_sub['go_term'] = pd.Series(term_list, index = variable_sub.index)
			go_dict[category.split('_')[1]].append((stable_sub, variable_sub))
		return go_dict
	
	@staticmethod
	def make_go_barplot(go_dict, output_folder):
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		for category in go_dict:
			stable_data, variable_data = go_dict[category][0]
			stable_data = stable_data[stable_data.FDR<0.01]
			variable_data = variable_data[variable_data.FDR<0.01]
			all_terms = set.intersection(*[set(stable_data.go_term), set(variable_data.go_term)])

			stable_data = stable_data.sort_values('Fold Enrichment', ascending = False)
			stable_fe_list = list(stable_data['Fold Enrichment'])
			label_list = list(stable_data.go_term)
			variable_fe_list = list()
			for term in list(stable_data.go_term):
				if term in all_terms:
					variable_fe_list.append(list(variable_data[variable_data.go_term==term]['Fold Enrichment'])[0])
				else:
					variable_fe_list.append(0)
			variable_other_data = variable_data[~variable_data.go_term.isin(list(stable_data.go_term))]
			variable_other_data = variable_other_data.sort_values('Fold Enrichment', ascending = True)
			for term in list(variable_other_data.go_term):
				variable_fe_list.append(list(variable_other_data[variable_other_data.go_term==term]['Fold Enrichment'])[0])
				label_list.append(term)
			stable_length = len(stable_fe_list)
			stable_fe_list = stable_fe_list + [0]*(len(variable_fe_list)-stable_length)

			sc_stable,colors_stable = colorFacade.get_specific_color_gradient(plt.cm.Blues, np.array(stable_fe_list))
			sc_variable,colors_variable = colorFacade.get_specific_color_gradient(plt.cm.Reds, np.array(variable_fe_list))

			plt.clf()
			fig = plt.figure()
			ax = fig.add_subplot(111)
			y_pos = np.arange(len(label_list))
			ax.barh(y_pos, list((-1)*np.array(stable_fe_list)), align='center',color=colors_stable, ecolor='black')
			ax.barh(y_pos, list(variable_fe_list), align='center',color=colors_variable, ecolor='black')
			#ax.set_yticks(y_pos)
			ax.set_yticklabels([])
			ax2 = ax.twinx()
			plt.yticks(list(xrange(len(label_list))))
			ax2.set_yticklabels(label_list)
			#ax.invert_yaxis()  # labels read top-to-bottom
			ax.set_xlabel('Fold Enrichment')
			ax.set_title('GO:'+str(category))
			ax.set_ylim(-1,len(label_list)+1)
			ax2.set_ylim(-1,len(label_list)+1)
			plt.savefig(output_folder + 'figures/suppFigure3_' + category + '_complexes_DAVID6_8.pdf',
						bbox_inches = 'tight', dpi = 400)

class supp_figure4a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE4A: main_supp_figure4a_go_histograms')
		supp_figure4a.main_supp_figure3a_go_histograms(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure3a_go_histograms(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('plot_BP_go_output')
		supp_figure4a.plot_BP_go_output(folder  = folder, output_folder = output_folder)
		print('plot_CC_go_output')
		supp_figure4a.plot_CC_go_output(folder  = folder, output_folder = output_folder)

	@staticmethod
	def plot_BP_go_output(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/BP_go_output_fdrData_male_female_difference_DAVID6_8.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/BP_go_output_feData_male_female_difference_DAVID6_8.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/BP_go_output_sizeData_male_female_difference_DAVID6_8.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['mq','fq']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['ms','fs']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['mq','fq']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['ms','fs']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['mq','fq']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['ms','fs']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('mq', ascending = False)
		q1 = quant_fe_data[quant_fe_data.mq>0]
		q2 = quant_fe_data[quant_fe_data.mq<=0]
		q2 = q2.sort_values('fq')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('ms', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data.ms>0]
		q2 = stoch_fe_data[stoch_fe_data.ms<=0]
		q2 = q2.sort_values('fs')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq, 'male')
		female_my_cmap = colorFacade.make_colormap(female_seq, 'female')

		male_quant_fe_list = list(quant_fe_data.mq)
		female_quant_fe_list = list(quant_fe_data.fq)
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data.mq))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data.fq))
		male_stoch_fe_list = list(stoch_fe_data.ms)
		female_stoch_fe_list = list(stoch_fe_data.fs)
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data.ms))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data.fs))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'BP_abundances_male_female_barplot_DAVID6_8.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(output_folder + 'figures/suppFig4a_BP_stoichiometry_male_female_barplot_DAVID6_8.pdf',
					bbox_inches = 'tight', dpi = 300)

	@staticmethod
	def plot_CC_go_output(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/CC_go_output_fdrData_male_female_difference_DAVID6_8.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/CC_go_output_feData_male_female_difference_DAVID6_8.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/CC_go_output_sizeData_male_female_difference_DAVID6_8.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['mq','fq']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['ms','fs']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['mq','fq']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['ms','fs']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['mq','fq']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['ms','fs']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('mq', ascending = False)
		q1 = quant_fe_data[quant_fe_data.mq>0]
		q2 = quant_fe_data[quant_fe_data.mq<=0]
		q2 = q2.sort_values('fq')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('ms', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data.ms>0]
		q2 = stoch_fe_data[stoch_fe_data.ms<=0]
		q2 = q2.sort_values('fs')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq, 'male')
		female_my_cmap = colorFacade.make_colormap(female_seq,'female')

		male_quant_fe_list = list(quant_fe_data.mq)
		female_quant_fe_list = list(quant_fe_data.fq)
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data.mq))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data.fq))
		male_stoch_fe_list = list(stoch_fe_data.ms)
		female_stoch_fe_list = list(stoch_fe_data.fs)
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data.ms))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data.fs))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'CC_abundances_male_female_barplot_DAVID6_8.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(output_folder + 'figures/suppFig4a_CC_stoichiometry_male_female_barplot_DAVID6_8.pdf',
					bbox_inches = 'tight', dpi = 300)

class supp_figure4b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE4B: supp_figure4b_complex_scatter_abundances')
		supp_figure4b.main_figure4b_complex_scatter_abundances(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE4B: supp_figure4b_subunit_scatter_abundances')
		supp_figure4b.main_figure4b_subunit_scatter_abundances(folder = folder, output_folder = output_folder)

	@staticmethod
	def export_data(**kwargs):
		ffolder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		folder = '/g/scb2/bork/romanov/wpc/'
		diet_data = DataFrameAnalyzer.getFile(folder,'gygi3_complex_DIET_total_abundance_difference.tsv')
		sex_data = DataFrameAnalyzer.getFile(folder,'gygi3_complex_total_abundance_difference.tsv')

		sex_data.to_csv(ffolder + 'data/fig5d_gygi3_complex_SEX_total_abundance_difference.tsv.gz', sep = '\t', compression = 'gzip')
		diet_data.to_csv(ffolder + 'data/fig5d_gygi3_complex_DIET_total_abundance_difference.tsv.gz', sep = '\t', compression = 'gzip')

		folder = '/g/scb2/bork/romanov/wpc/'
		fileName = 'gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		gender_data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		fileName = 'gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		diet_data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		diet_data.to_csv(ffolder + 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz', sep = '\t', compression = 'gzip')
		return sex_data, diet_data

	@staticmethod
	def prepare_data_complex_scatter_abundances(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		dat = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_gygi3.tsv.gz')
		diet_data = DataFrameAnalyzer.open_in_chunks(folder,'data/fig5d_gygi3_complex_DIET_total_abundance_difference.tsv.gz')
		sex_data = DataFrameAnalyzer.open_in_chunks(folder,'data/fig5d_gygi3_complex_SEX_total_abundance_difference.tsv.gz')

		key_list = list(sex_data.index)
		relevant_complexes = list()
		for key in key_list:
			complexID = key.split(':')[0]
			goldComplex = complexDict[complexID]['goldComplex'][0]
			if len(dat[dat.complex_search==key])>=5 and goldComplex=='yes':
				relevant_complexes.append(key)

		sex_data = sex_data.T[relevant_complexes].T
		diet_data = diet_data.T[relevant_complexes].T
		diet_cohen_list = list(diet_data.cohen_distance)
		sex_cohen_list = list(sex_data.cohen_distance)

		sex_colors = ['green' if item<=0.01 else 'white' for item in utilsFacade.correct_pvalues(list(sex_data.ttest_pval))]
		diet_colors = ['grey' if item<=0.01 else 'white' for item in utilsFacade.correct_pvalues(list(diet_data.ttest_pval))]
		colors = list()
		for s,d in zip(sex_colors, diet_colors):
			if s=='green' and d=='white':
				colors.append('lightgreen')
			elif s=='white' and d== 'grey':
				colors.append('grey')
			elif s=='green' and d=='grey':
				colors.append('green')
			else:
				colors.append('white')

		df = pd.DataFrame({'sex':sex_cohen_list,'diet':diet_cohen_list})
		df.index = relevant_complexes
		df['color'] = pd.Series(colors, index = df.index)

		df.to_csv(folder + 'data/fig5d_gygi3_complex_total_abundance_difference.tsv.gz', sep = '\t', compression = 'gzip')

		################################################################################################################

		gender_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz')
		diet_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz')
		gender_data = gender_data.drop(['C2','C9','Psd3'], axis = 0)
		diet_data = diet_data.drop(['C2','C9','Psd3'], axis = 0)

		gender_stoch_data = gender_data[gender_data.analysis_type=='stoch_male-female']
		diet_stoch_data = diet_data[diet_data.analysis_type=='stoch_highfat-chow']
		gender_stoch_dict = gender_stoch_data['logFC'].to_dict()
		diet_stoch_dict = diet_stoch_data['logFC'].to_dict()
		gender_pval_dict = gender_stoch_data['pval.adj'].to_dict()
		diet_pval_dict = diet_stoch_data['pval.adj'].to_dict()
		protein_list = list(set(gender_stoch_data.index).intersection(set(diet_stoch_data.index)))
		gender_stoch_list = list()
		diet_stoch_list = list()
		gender_pval_list = list()
		diet_pval_list = list()
		colors = list()

		for protein in protein_list:
			gender_stoch_list.append(gender_stoch_dict[protein])
			diet_stoch_list.append(diet_stoch_dict[protein])
			if gender_pval_dict[protein]<=0.01 and diet_pval_dict[protein]<=0.01:
				colors.append('darkgreen')
			elif gender_pval_dict[protein]<=0.01 and diet_pval_dict[protein]>0.01:
				colors.append('lightgreen')
			elif gender_pval_dict[protein]>0.01 and diet_pval_dict[protein]<=0.01:
				colors.append('grey')
			else:
				colors.append('white')
			gender_pval_list.append(gender_pval_dict[protein])
			diet_pval_list.append(diet_pval_dict[protein])

		df = pd.DataFrame({'gender':gender_stoch_list, 'diet':diet_stoch_list,
						   'color':colors, 'gender_pval':gender_pval_list, 'diet_pval':diet_pval_list})
		df.index = protein_list
		df.to_csv(folder + 'data/fig5d_gygi3_subunit_stoichiometry_difference.tsv.gz', sep = '\t', compression = 'gzip')

		return df

	@staticmethod
	def main_figure4b_complex_scatter_abundances(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder,'data/fig5d_gygi3_complex_total_abundance_difference.tsv.gz')
		sex_cohen_list = list(df['sex'])
		diet_cohen_list = list(df['diet'])
		colors = list(df['color'])

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(sex_cohen_list, diet_cohen_list, edgecolor = 'black',
		 	color = colors, alpha = 0.7, s = 80)
		plottingFacade.add_trendline(ax, sex_cohen_list, diet_cohen_list, color = 'k')
		ax.set_title(scipy.stats.pearsonr(sex_cohen_list, diet_cohen_list))
		ax.set_xlabel('sex')
		ax.set_ylabel('diet')
		ax.set_xlim(-2,2)
		ax.set_ylim(-2,2)
		plt.savefig(folder + 'figures/suppFig4b_complex_abundance_comparing_diet_sex.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def main_figure4b_subunit_scatter_abundances(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder, 'data/fig5d_gygi3_subunit_stoichiometry_difference.tsv.gz')
		gender_stoch_list = df['gender']
		diet_stoch_list = df['diet']
		colors = df['color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,7))
		ax = fig.add_subplot(111)
		ax.scatter(np.array(gender_stoch_list), np.array(diet_stoch_list),
				   color = colors, edgecolor = 'black', alpha = 0.7, s = 30)
		plottingFacade.add_trendline(ax,np.array(gender_stoch_list),
									 np.array(diet_stoch_list), color = 'k')
		ax.set_xlim(-1,1)
		ax.set_ylim(-1,1)
		ax.set_title(scipy.stats.pearsonr(np.array(gender_stoch_list), np.array(diet_stoch_list)))
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('logFC (high-fat/chow)')
		plt.savefig(folder + 'figures/suppFig4b_logFC_complexSubunits_scatter_stoichiometry_gender_diet.pdf',
					bbox_inches	= 'tight', dpi = 400)

class supp_figure4c:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE4C: main_supp_figure4c_volcanoPlots')
		supp_figure4c.main_supp_figure4c_volcanoPlots(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure4c_volcanoPlots(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE3C: suppFigure4c_volcanoPlot_cop1_hsComparison')
		supp_figure4c.suppFigure4c_volcanoPlot_cop1_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_volcanoPlot_cop2_hsComparison')
		supp_figure4c.suppFigure4c_volcanoPlot_cop2_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_volcanoPlot_cohesin_hsComparison')
		supp_figure4c.suppFigure4c_volcanoPlot_cohesin_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_volcanoPlot_retromer_hsComparison')
		supp_figure4c.suppFigure4c_volcanoPlot_retromer_hsComparison(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure4c_boxPlots(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE3C: suppFigure4c_cop1_boxplot_logFC_hsComparison')
		supp_figure4c.suppFigure4c_cop1_boxplot_logFC_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_cop2_boxplot_logFC_hsComparison')
		supp_figure4c.suppFigure4c_cop2_boxplot_logFC_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_retromer_boxplot_logFC_hsComparison')
		supp_figure4c.suppFigure4c_retromer_boxplot_logFC_hsComparison(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE3C: suppFigure4c_cohesin_boxplot_logFC_hsComparison')
		supp_figure4c.suppFigure4c_cohesin_boxplot_logFC_hsComparison(folder = folder, output_folder = output_folder)

	@staticmethod
	def suppFigure4c_volcanoPlot_cop1_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_highfat-chow']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search=='HC2402:COPI']

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])

		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
			edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 10)
		plt.savefig(folder + 'figures/suppFig4c_cop1_volcano_hs_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure4c_volcanoPlot_retromer_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_highfat-chow']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('retromer')]

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])

		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (highfat/chow)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 10)
		plt.savefig(folder + 'figures/suppFig4c_retromer_volcano_hs_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure4c_volcanoPlot_cohesin_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_highfat-chow']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('Cohesin')]

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])

		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (highfat/chow)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01,10)
		plt.savefig(folder + 'figures/suppFig4c_cohesin_volcano_hs_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure4c_volcanoPlot_cop2_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_highfat-chow']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search=='HC663:COPII']

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])

		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (highfat/chow)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 10)
		plt.savefig(folder + 'figures/suppFig4c_cop2_volcano_hs_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def suppFigure4c_cop1_boxplot_logFC_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search=='HC2402:COPI']
		male_data = complex_data[complex_data.analysis_type=='highfat']
		female_data = complex_data[complex_data.analysis_type=='chow']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_highfat-chow']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = wp.stoch_gygi3.copy()
		wpc_data = wpc_data[wpc_data.complex_search=='HC2402:COPI']
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'H')
		female_list = utilsFacade.filtering(quant_list, 'S')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=0.01:
				data_list.append(utilsFacade.finite(list(male_data[protein])))
				data_list.append(utilsFacade.finite(list(female_data[protein])))
				color_list.append('purple')
				color_list.append('green')
				sig_protein_list.append(protein)
				sig_protein_list.append(protein)
				positions.append(positions[-1]+0.4)
				positions.append(positions[-1]+0.6)

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		sig_protein_list.append('COPI-highfat')
		sig_protein_list.append('COPI-chow')
		positions.append(positions[-1]+0.5)


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPI complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/suppFig4c_cop1_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def suppFigure4c_cop2_boxplot_logFC_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search=='HC663:COPII']
		male_data = complex_data[complex_data.analysis_type=='highfat']
		female_data = complex_data[complex_data.analysis_type=='chow']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_highfat-chow']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = wp.stoch_gygi3.copy()
		wpc_data = wpc_data[wpc_data.complex_search=='HC663:COPII']
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'H')
		female_list = utilsFacade.filtering(quant_list, 'S')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=0.01:
				data_list.append(utilsFacade.finite(list(male_data[protein])))
				data_list.append(utilsFacade.finite(list(female_data[protein])))
				color_list.append('purple')
				color_list.append('green')
				sig_protein_list.append(protein)
				sig_protein_list.append(protein)
				positions.append(positions[-1]+0.4)
				positions.append(positions[-1]+0.6)

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		sig_protein_list.append('COPII-highfat')
		sig_protein_list.append('COPII-chow')
		positions.append(positions[-1]+0.5)


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPII complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/suppFigure4c_cop2_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure4c_retromer_boxplot_logFC_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('retromer')]
		male_data = complex_data[complex_data.analysis_type=='highfat']
		female_data = complex_data[complex_data.analysis_type=='chow']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_highfat-chow']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = wp.stoch_gygi3.copy()
		wpc_data = wpc_data[wpc_data.complex_search.str.contains('MAPK')]
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'H')
		female_list = utilsFacade.filtering(quant_list, 'S')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=1:
				data_list.append(utilsFacade.finite(list(male_data[protein])))
				data_list.append(utilsFacade.finite(list(female_data[protein])))
				color_list.append('purple')
				color_list.append('green')
				sig_protein_list.append(protein)
				sig_protein_list.append(protein)
				positions.append(positions[-1]+0.4)
				positions.append(positions[-1]+0.6)

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		sig_protein_list.append('MAPK-highfat')
		sig_protein_list.append('MAPK-chow')
		positions.append(positions[-1]+0.5)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('MAPK complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/suppFigure4c_retromer_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure4c_cohesin_boxplot_logFC_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('Cohesin')]
		male_data = complex_data[complex_data.analysis_type=='highfat']
		female_data = complex_data[complex_data.analysis_type=='chow']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_highfat-chow']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = wp.stoch_gygi3.copy()
		wpc_data = wpc_data[wpc_data.complex_search.str.contains('Cohesin')]
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'H')
		female_list = utilsFacade.filtering(quant_list, 'S')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=1:
				data_list.append(utilsFacade.finite(list(male_data[protein])))
				data_list.append(utilsFacade.finite(list(female_data[protein])))
				color_list.append('purple')
				color_list.append('green')
				sig_protein_list.append(protein)
				sig_protein_list.append(protein)
				positions.append(positions[-1]+0.4)
				positions.append(positions[-1]+0.6)

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		sig_protein_list.append('Cohesin-highfat')
		sig_protein_list.append('Cohesin-chow')
		positions.append(positions[-1]+0.5)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('Cohesin complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/suppFig4c_cohesin_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)

class supp_figure5a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE4A: main_suppFig5a_aebersold_liu_comparison')
		supp_figure5a.main_suppFig4a_aebersold_liu_comparison(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_suppFig4a_aebersold_liu_comparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('load_aebersold_liu_data')
		data = supp_figure5a.load_aebersold_liu_data(folder = folder, output_folder = output_folder)
		print('comparison_aebersold_liu_data')
		supp_figure5a.comparison_aebersold_liu_data(data, folder = folder, output_folder = output_folder)

	@staticmethod
	def load_aebersold_liu_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/aebersold_liu_twin_data.txt'
		data = DataFrameAnalyzer.getFile(folder, filename)
		data.index = data['Protein Symbol']
		return data

	@staticmethod
	def comparison_aebersold_liu_data(aedata, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fname = 'data/protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		data.index = [su.upper() for su in map(str,list(data['subunit']))]
		sex_all_quant = data[data.covariates=='sex']		
		diet_all_quant = data[data.covariates=='diet']

		overlapping_proteins = list(set(aedata.index).intersection(set(sex_all_quant.index)))
		aedata = aedata.T[overlapping_proteins].T
		print(aedata.shape)
		heritability_dict = aedata['Heritability'].to_dict()
		environment_dict = aedata['Individual environment'].to_dict()

		sub_sex_data = sex_all_quant.loc[overlapping_proteins]
		sex_dict = sub_sex_data['r2.all.module'].to_dict()
		sub_diet_data = diet_all_quant.loc[overlapping_proteins]
		diet_dict = sub_diet_data['r2.all.module'].to_dict()

		ae_heritability_list = list()
		ae_environment_list = list()
		ou_gender_list = list()
		ou_diet_list = list()
		genetic_protein_list = list()
		env_protein_list = list()
		for protein in overlapping_proteins:
			if heritability_dict[protein]>0 and environment_dict[protein]>0:
				ou_gender_list.append(sex_dict[protein])
				ae_heritability_list.append(heritability_dict[protein])
				genetic_protein_list.append(protein)
				ou_diet_list.append(diet_dict[protein])
				ae_environment_list.append(environment_dict[protein])
				env_protein_list.append(protein)
		
		#51 proteins compared
		genetic_df = pd.DataFrame({'gender':ou_gender_list,
						   		   'heritability':ae_heritability_list})
		env_df = pd.DataFrame({'environment':ae_environment_list,'diet':ou_diet_list})
		genetic_df.index = genetic_protein_list
		env_df.index = env_protein_list

		df = pd.DataFrame({'gender':ou_gender_list,
						   'heritability':ae_heritability_list,
						   'environment':ae_environment_list,
						   'diet':ou_diet_list})
		df.index = env_protein_list

		rand_corrs = list()
		for i in np.arange(1,100):
			table_list = list()
			for i,row in df.iterrows():
				temp = list(row)
				np.random.shuffle(temp)
				table_list.append(temp)
			rand_data = pd.DataFrame(table_list)
			rand_data = rand_data.T
			table_list = list()
			for i,row in rand_data.iterrows():
				temp = list(row)
				np.random.shuffle(temp)
				table_list.append(temp)
			rand_data = pd.DataFrame(table_list)
			rand_data = rand_data.T
			rand_corrs.append(utilsFacade.get_correlation_values(rand_data.corr(method = 'spearman')))
		rand_corrs = utilsFacade.flatten(rand_corrs)
		rand_corrs = np.array(rand_corrs)

		corrData = df.corr(method = 'spearman')
		corrData = corrData[['gender','diet']]
		corrData = corrData.T[['heritability','environment']].T

		pvalues = list()
		for i,row in corrData.iterrows():
			c1,c2 = list(row)
			zscore1 = utilsFacade.getSampleZScore(rand_corrs, c1)
			zscore2 = utilsFacade.getSampleZScore(rand_corrs, c2)
			p1,p2 = utilsFacade.zscore_to_pvalue(zscore1)
			temp = [p1]
			p1,p2 = utilsFacade.zscore_to_pvalue(zscore2)
			temp.append(p1)
			pvalues.append(temp)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (1,1))
		ax = fig.add_subplot(111)
		sns.heatmap(corrData, cmap = plt.cm.RdBu, linewidth = 0.5, cbar = False, annot = True)
		plt.savefig(folder + 'figures/suppFig5a_comparison_aebersold.pdf', bbox_inches = 'tight', dpi = 400)

		#make a heatmap with ranking for supp
		rank_list = list()
		for col in list(df.columns):
			rank_list.append(rankdata(list(df[col])))
		ranked_df = pd.DataFrame(rank_list)
		ranked_df = ranked_df.T
		ranked_df.index =  df.index
		ranked_df.columns = df.columns
		ranked_df, pro = utilsFacade.recluster_matrix_only_rows(ranked_df)
		ranked_df = ranked_df.T

		plt.clf()
		fig = plt.figure(figsize = (20,3))
		ax = fig.add_subplot(111)
		sns.heatmap(ranked_df, cmap = plt.cm.coolwarm, linewidth = 1.0)
		plt.savefig(folder + 'figures/suppFig5a_comparison_aebersold_heatmap_rankedDF.pdf',
					bbox_inches = 'tight', dpi = 400)

class supp_figure5b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE5B: main_supp_figure5b_volcanoPlots')
		supp_figure5b.main_supp_figure5b_volcanoPlots(folder = folder, output_folder = output_folder)
		print('SUPP-FIGURE5B: main_supp_figure5b_boxPlots')
		supp_figure5b.main_supp_figure5b_boxPlots(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure4b_volcanoPlots(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE5B: suppFigure5b_volcanoPlot_pyruvateDH_hsComparison')
		supp_figure5b.suppFigure5b_volcanoPlot_pyruvateDH_hsComparison(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_supp_figure4b_boxPlots(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('SUPP-FIGURE5B: suppFigure5b_pyruvateDH_boxplot_logFC_hsComparison')
		supp_figure5b.suppFigure5b_pyruvateDH_boxplot_logFC_hsComparison(folder = folder, output_folder = output_folder)

	@staticmethod
	def suppFigure5b_volcanoPlot_pyruvateDH_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_highfat-chow']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('pyruvate')]

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])

		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
			edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (highfat/chow)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 14)
		plt.savefig(folder + 'figures/suppFig5b_pyruvateDH_volcano_hs_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def suppFigure5b_pyruvateDH_boxplot_logFC_hsComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains('pyruvate')]
		male_data = complex_data[complex_data.analysis_type=='highfat']
		female_data = complex_data[complex_data.analysis_type=='chow']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_highfat-chow']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = DataFrameAnalyzer.getFile(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		wpc_data = wpc_data[wpc_data.complex_search.str.contains('pyruvate')]
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'H')
		female_list = utilsFacade.filtering(quant_list, 'S')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=0.01:
				data_list.append(utilsFacade.finite(list(male_data[protein])))
				data_list.append(utilsFacade.finite(list(female_data[protein])))
				color_list.append('purple')
				color_list.append('green')
				sig_protein_list.append(protein)
				sig_protein_list.append(protein)
				positions.append(positions[-1]+0.4)
				positions.append(positions[-1]+0.6)

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		sig_protein_list.append('pyruvate-highfat')
		sig_protein_list.append('pyruvate-chow')
		positions.append(positions[-1]+0.5)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('pyruvate complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/suppFig5b_pyruvateDH_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)

		com_data = DataFrameAnalyzer.getFile(folder, 'data/complex_filtered_gygi3.tsv.gz')
		com_data = com_data[com_data.complex_search.str.contains('pyruvate')]
		quant_list = utilsFacade.filtering(list(com_data.columns), 'quant_')
		hlist = utilsFacade.filtering(quant_list, 'H')
		clist = utilsFacade.filtering(quant_list, 'S')
		hdata = com_data[hlist]
		cdata = com_data[clist]
		h_median = utilsFacade.finite(list(hdata.median()))
		c_median = utilsFacade.finite(list(cdata.median()))
		data_list = [h_median, c_median]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(i+1, 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('pyruvate complex')
		ax.set_ylabel('complex median abundances')
		plt.savefig(folder + 'figures/suppFig5b_Abundances_pyruvateDH_boxplot_highfat_chow.pdf',
					bbox_inches = 'tight', dpi = 400)

class supp_figures(object):
	def __init__(self, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		'''
		print('*****************************')
		print('SUPP-FIGURE1')
		print('*****************************')
		sfig1 = supp_figure1a.execute(folder = folder, output_folder = output_folder)
		'''
		print('*****************************')
		print('SUPP-FIGURE2A')
		print('*****************************')
		sfig2a = supp_figure2a.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('SUPP-FIGURE2B')
		print('*****************************')
		sfig2b = supp_figure2b.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('SUPP-FIGURE3')
		print('*****************************')
		sfig3 = supp_figure3.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('SUPP-FIGURE4A')
		print('*****************************')
		sfig4a = supp_figure4a.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('SUPP-FIGURE4B')
		print('*****************************')
		sfig4b = supp_figure4b.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('SUPP-FIGURE4C')
		print('*****************************')
		sfig4c = supp_figure4c.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('SUPP-FIGURE5A')
		print('*****************************')
		sfig5a = supp_figure5a.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('SUPP-FIGURE5B')
		print('*****************************')
		sfig5b = supp_figure5b.execute(folder = folder, output_folder = output_folder)

supp_fig = supp_figures()


