class figure4a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE4A: main_figure4a_complex_intern_landscapes')
		figure4a.main_figure4a_complex_intern_landscapes(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure4a_complex_intern_landscapes(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		do_ranking = kwargs.get('do_ranking', False)

		print('get_data')
		data_dict = figure4a.get_data(folder = output_folder)
		name_list = ['gygi3','gygi1','battle_protein','mann_all_log2', 'tcgaBreast',
					 'tcgaOvarian', 'tcgaColoCancer']
		data_list = [data_dict['gygi3'], data_dict['gygi1'], data_dict['battle_protein'],
					data_dict['mann'], data_dict['tcgaBreast'], data_dict['tcgaOvarian'],
					data_dict['tcgaColoCancer']]

		print('iteration_complexes')
		figure4a.iteration_complexes(data_list, name_list, output_folder = output_folder, do_ranking = do_ranking)

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		stoch_gygi3 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		stoch_gygi1 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi1.tsv.gz')
		stoch_mann = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_mann.tsv.gz')
		stoch_battle_protein = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_battle_protein.tsv.gz')
		stoch_tcgaBreast = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaBreast.tsv.gz')
		stoch_tcgaOvarian = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaOvarian.tsv.gz')
		stoch_tcgaColorCancer = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaColoCancer.tsv.gz')
		return {'gygi3':stoch_gygi3, 'gygi1':stoch_gygi1, 'mann':stoch_mann,
				'battle_protein':stoch_battle_protein, 'tcgaBreast':stoch_tcgaBreast,
				'tcgaOvarian':stoch_tcgaOvarian, 'tcgaColoCancer':stoch_tcgaColorCancer}

	@staticmethod
	def load_relevant_complexes(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		com_data = DataFrameAnalyzer.getFile(folder, 'data/figure4a_relevant_complexes.tsv')
		relevant_complexes = list(com_data.index)
		return relevant_complexes

	@staticmethod
	def prepare_variance_dataframe(name_list, data_list, complex_df, proteins):
		df_list = list()
		for protein in proteins:
			temp = list()
			for d,n in zip(data_list, name_list):
				sub = complex_df[complex_df.fileName==n]
				if protein in list(sub.index):
					if type(sub.loc[protein]) == pd.DataFrame:
						quant_cols = utilsFacade.filtering(d, 'quant_', condition = 'startswith')
						s = sub.loc[protein][quant_cols].T
						coverage_list = list()
						for c,col in enumerate(list(s.columns)):
							temp_finite = utilsFacade.finite(list(np.array(s)[:,c]))
							tempSmall = list(np.array(s)[:,c])
							coverage_list.append(float(len(temp_finite))/float(len(tempSmall))*100)
						s = sub.loc[protein]
						s['coverage'] = pd.Series(coverage_list, index = s.index)
						s = s.sort_values('coverage', ascending = False)
						var_value = s.iloc[0]['relative_variance']
						temp.append(var_value)
					else:
						var_value = sub.loc[protein]['relative_variance']
						temp.append(var_value)
				else:
					temp.append(np.nan)
			df_list.append(temp)
		df = pd.DataFrame(df_list)
		df.index = proteins
		df.columns = name_list
		df = df.dropna(thresh = int(len(df.columns)/2.0))
		df = df.T
		return df

	@staticmethod
	def rank_dataset(df, proteins, name_list):
		df_lst = list()
		dfList = map(list,df.values)
		for f in dfList:
			rank_list = rankdata(f)
			all_ranks = rankdata(utilsFacade.finite(f))
			if len(all_ranks)>0:
				minimum_rank = all_ranks.min() 
			temp_list = list()
			for fitem, rank in zip(f, rank_list):
				if str(fitem) != 'nan':
					temp_list.append(rank)
				else:
					temp_list.append(np.nan)
			df_lst.append(temp_list)
		df = pd.DataFrame(df_lst)
		df.columns = proteins
		df.index = name_list
		return df

	@staticmethod
	def get_zscores(df):
		myArray = np.array(df)
		normalizedArray = []
		for row in range(0, len(myArray)):
			list_values = []
			Min =  min(utilsFacade.finite(list(myArray[row])))
			Max = max(utilsFacade.finite(list(myArray[row])))
			mean = np.mean(utilsFacade.finite(list(myArray[row])))
			std = np.std(utilsFacade.finite(list(myArray[row])))
			for element in myArray[row]:
				list_values.append((element - mean)/std)
			normalizedArray.append(list_values)

		newArray = []
		for row in range(0, len(normalizedArray)):
			list_values = normalizedArray[row]
			newArray.append(list_values)

		new_df = pd.DataFrame(newArray)
		new_df.columns = list(df.columns)
		new_df.index = df.index
		df = new_df.copy()
		df = df.iloc[::-1]
		dfList = map(list,df.values)
		return df, dfList

	@staticmethod
	def manage_proteasome_data(df):
		try:
			df = df.drop(['PSD3','C9','C2','C6','RPN1'], axis = 1)
		except:
			remove_list = ['PSD3','C9','C2','C6','RPN1']
			remove_list1 = [item[0] + item[1:].lower() for item in remove_list]
			remove_list = remove_list + remove_list1
			df = df[~df.index.isin(remove_list)]
		return df		

	@staticmethod
	def plot_heatmap(df, complex_id, altName, **kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		sc, color_list = colorFacade.get_specific_color_gradient(plt.cm.RdBu_r,
																 np.array(utilsFacade.finite(utilsFacade.flatten(map(list, df.values)))),
																 vmin = -2, vmax = 2)
		
		plt.clf()
		if complex_id.find('26S')!=-1 or complex_id.find('NPC')!=-1:
			fig = plt.figure(figsize = (10,3))
		else:
			fig = plt.figure(figsize = (5,3))
		ax = fig.add_subplot(111)
		mask = df.isnull()
		mask1 = mask.copy()
		mask1 = mask1.replace(True,'bla')
		mask1 = mask1.replace(False,'20')
		mask1 = mask1.replace('bla',1)
		mask1 = mask1.replace('20',0)
		sns.heatmap(mask1, cmap = ['grey'], linewidth = 0.2, cbar = False,
					xticklabels = [], yticklabels = [])
		sns.heatmap(df, cmap = plt.cm.RdBu_r, linewidth = 0.2, mask = mask,
					vmin = -2, vmax = 2)
		plt.savefig(output_folder + 'figures/fig4a_' + altName + '_heatmap_subunits.pdf', 
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def get_alternative_name(complex_id):
		altName = complex_id
		altName = '_'.join(altName.split(' '))
		altName = altName.replace('/','')
		altName = altName.replace(':','')
		return altName

	@staticmethod
	def iteration_complexes(data_list, name_list, **kwargs):
		do_ranking = kwargs.get('do_ranking', False)
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		relevant_complexes = figure4a.load_relevant_complexes()
		concatanated_complex_dfs = list()
		for complex_id in relevant_complexes:
			print('*************************')
			print(complex_id)
			altName = figure4a.get_alternative_name(complex_id)

			concat_list = list()
			for d,n in zip(data_list, name_list):
				sub = d[d.ComplexName == complex_id]
				if len(sub)>0:
					sub['fileName'] = pd.Series([n]*len(sub), index = sub.index)
					concat_list.append(sub)
			complex_df = pd.concat(concat_list)
			complex_df.index = [item.upper() for item in list(complex_df.index)]
			proteins = list(set(complex_df.index))			

			df = figure4a.prepare_variance_dataframe(name_list, data_list, complex_df, proteins)
			if do_ranking == True:
				df = figure4a.rank_dataset(df, proteins, name_list)
			df, dfList = figure4a.get_zscores(df)
			if complex_id.find('26S')!=-1:
				df = figure4a.manage_proteasome_data(df)

			protein_list = list(df.columns)
			label_list = list()
			median_list = list()
			for c,col in enumerate(df.columns):
				median_list.append(np.mean(utilsFacade.finite(list(np.array(df)[:,c]))))
				label_list.append(col)
			median_list, label_list = zip(*sorted(zip(median_list, label_list), reverse = False))
			df = df[list(label_list)]
			if complex_id.find('Kornberg')!=-1:
				complex_id = 'Kornbergs mediator (SRB) complex'

			df1 = df.T.copy()
			df1['complex_id'] = pd.Series([complex_id]*len(df1), index = df1.index)
			concatanated_complex_dfs.append(df1)
			print('plot_heatmap')
			print('*************************')

			figure4a.plot_heatmap(df, complex_id, altName, output_folder = output_folder)

		complex_data = pd.concat(concatanated_complex_dfs)
		complex_data.to_csv(output_folder + 'data/fig4a_underlyingData_complexes_RNA_newData_' + time.strftime('%Y%m%d') + '.tsv',
							sep = '\t')
		return complex_data

	@staticmethod
	def read_data():
		complex_data = DataFrameAnalyzer.getFile('/g/scb2/bork/romanov/wpc/',
					   'wpc_figure2B_underlyingData_complexes_RNA_newData.tsv')
		quant_data = complex_data.drop(['complex_id','mean','p.value','label'],1)
		mean_vector = quant_data.T.mean()
		median_vector = quant_data.T.median()
		pvalues = list()
		label_list = list()
		for zscore in median_vector:
			pval = utilsFacade.zscore_to_pvalue(zscore)[1]
			pvalues.append(pval)
			if pval<0.1 and zscore>0:
				label_list.append('variable')
			elif pval<0.1 and zscore<0:
				label_list.append('stable')
			else:
				label_list.append('')
		complex_data['median'] = pd.Series(list(median_vector), index = complex_data.index)
		complex_data['p.value'] = pd.Series(pvalues, index = complex_data.index)
		complex_data['label'] = pd.Series(label_list, index = complex_data.index)
		complex_data.to_csv('/g/scb2/bork/romanov/wpc/' + 
							'wpc_figure2B_underlyingData_complexes_RNA_newData.tsv',
							sep = '\t')

class figure4b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE4B: main_figure4b_26SProteasome_analysis')
		figure4b.main_figure4b_26SProteasome_analysis()

	@staticmethod
	def main_figure4b_26SProteasome_analysis(**kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print("load_data")
		dat_gygi3_26S, dat_battle_protein_26S = figure4b.load_data(folder = output_folder)
		print("map_variance")
		var_dict_gygi3, var_dict_bp = figure4b.map_variance(dat_gygi3_26S,
									  dat_battle_protein_26S, scatter_ranked = scatter_ranked)
		print("prepare_plotting_data")
		mutual_proteins, varList1, varList2 = figure4b.prepare_plotting_data(var_dict_gygi3,
											  var_dict_bp, scatter_ranked = scatter_ranked)
		print('make_scatter_plot')
		figure4b.make_scatter_plot(mutual_proteins, varList1, varList2,
								   scatter_ranked = scatter_ranked,
								   output_folder = output_folder)
		print("load_boxplot_data")
		sig_data_gygi3, sig_data_bp = figure4b.load_boxplot_data()
		print('plot_boxplot_complexVariance')
		figure4b.plot_boxplot_complexVariance(sig_data_gygi3,
											  output_folder =  output_folder)

	@staticmethod
	def load_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		dat_gygi3 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_gygi3.tsv.gz')
		dat_battle_protein = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_protein.tsv.gz')

		quant_cols_gygi3 = utilsFacade.get_quantCols(dat_gygi3)
		quant_cols_bp = utilsFacade.get_quantCols(dat_battle_protein)

		dat_gygi3_26S = dat_gygi3[dat_gygi3.ComplexName=="26S Proteasome"][quant_cols_gygi3].T
		dat_battle_protein_26S = dat_battle_protein[dat_battle_protein.ComplexName=="26S Proteasome"][quant_cols_bp].T
		return dat_gygi3_26S, dat_battle_protein_26S

	@staticmethod
	def map_variance(dat_gygi3_26S, dat_battle_protein_26S, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")

		varList_gygi3 = list()
		for c,col in enumerate(list(dat_gygi3_26S.columns)):
			varList_gygi3.append(np.var(utilsFacade.finite(list(dat_gygi3_26S.iloc[:,c]))))
		varList_bp = list()
		for c,col in enumerate(list(dat_battle_protein_26S.columns)):
			varList_bp.append(np.var(utilsFacade.finite(list(dat_battle_protein_26S.iloc[:,c]))))

		dat_gygi3_26S = dat_gygi3_26S.T
		dat_battle_protein_26S = dat_battle_protein_26S.T
		dat_gygi3_26S["variance"] = pd.Series(varList_gygi3, index=dat_gygi3_26S.index)
		dat_battle_protein_26S["variance"] = pd.Series(varList_bp, index=dat_battle_protein_26S.index)
		dat_gygi3_26S["ranked_variance"] = pd.Series(rankdata(varList_gygi3), index=dat_gygi3_26S.index)
		dat_battle_protein_26S["ranked_variance"] = pd.Series(rankdata(varList_bp), index=dat_battle_protein_26S.index)

		if scatter_ranked == "no":
			var_dict_gygi3 = dat_gygi3_26S.to_dict()["variance"]
			var_dict_bp = dat_battle_protein_26S.to_dict()["variance"]
		else:
			var_dict_gygi3 = dat_gygi3_26S.to_dict()["ranked_variance"]
			var_dict_bp = dat_battle_protein_26S.to_dict()["ranked_variance"]
		return var_dict_gygi3,var_dict_bp
	
	@staticmethod
	def prepare_plotting_data(var_dict_gygi3, var_dict_bp, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")

		mutual_proteins = list(set([item.upper() for item in list(var_dict_gygi3.keys())]).intersection(set(var_dict_bp.keys())))
		varList1 = list()
		varList2 = list()
		for protein in mutual_proteins:
			if scatter_ranked == "no":
				varList1.append(np.log10(var_dict_gygi3[protein[0] + protein[1:].lower()]))
				varList2.append(np.log10(var_dict_bp[protein]))
			else:
				varList1.append(var_dict_gygi3[protein[0] + protein[1:].lower()])
				varList2.append(var_dict_bp[protein])
		return mutual_proteins,varList1,varList2

	@staticmethod
	def make_scatter_plot(mutual_proteins, varList1, varList2, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		immuno_proteasome=["PSMB5","PSMB6","PSMB7","PSMB8","PSMB9","PSMB10"]
		#immuno_proteasome=["ATPIF1"]
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(4,3))
		ax = fig.add_subplot(111)
		count = 0
		for var1,var2 in zip(varList1,varList2):
			if mutual_proteins[count]!="PSMA8":#not enough data points
				if mutual_proteins[count] in immuno_proteasome:
					color = "orange"
				else:
					if mutual_proteins[count].find("A")!=-1 or mutual_proteins[count].find("B")!=-1:
						color = "#95DCEC"
					else:
						color = "grey"
				ax.scatter(var1,var2,alpha=0.8,color=color,edgecolor="black",s=100,linewidth=1)
				if scatter_ranked=="no":
					if var1>=0.5 or var2>=-1.5:
						if mutual_proteins[count]=="PSMB8":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2-0.25),fontsize=12)
						elif mutual_proteins[count]=="PSMB10":
							ax.annotate(mutual_proteins[count],xy=(var1-0.25,var2-0.3),fontsize=12)
						elif mutual_proteins[count]=="PSMB6":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2+0.05),fontsize=12)
						elif mutual_proteins[count]=="PSMB5":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2-0.1),fontsize=12)
						elif mutual_proteins[count]=="PSMB7":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMD9":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMB4":
							ax.annotate(mutual_proteins[count],xy=(var1-0.05,var2+0.15),fontsize=12)
						else:
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2),fontsize=12)
				else:
					if var1>=20 or var2>=20:
						if mutual_proteins[count]=="PSMB8":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2-0.25),fontsize=12)
						elif mutual_proteins[count]=="PSMB10":
							ax.annotate(mutual_proteins[count],xy=(var1-0.25,var2-0.3),fontsize=12)
						elif mutual_proteins[count]=="PSMB6":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2+0.05),fontsize=12)
						elif mutual_proteins[count]=="PSMB5":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2-0.1),fontsize=12)
						elif mutual_proteins[count]=="PSMB7":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMD9":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMB4":
							ax.annotate(mutual_proteins[count],xy=(var1-0.05,var2+0.15),fontsize=12)
						else:
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2),fontsize=12)
			count+=1
		lt = ["immunoproteasome","20S sub-complex","19S sub-complex"]
		lh = [plt.Rectangle((0,0),1,1,fc="orange"),plt.Rectangle((0,0),1,1,fc="#95DCEC"),plt.Rectangle((0,0),1,1,fc="grey")]
		if scatter_ranked=="no":
			ax.set_xlabel("DO mouse strains(P)\n[variance (-log10)]",fontsize=12)
			ax.set_ylabel("Human Individuals(P)\n[variance (-log10)]",fontsize=12)
		else:
			ax.set_xlabel("DO mouse strains(P)\n[ranked variance]",fontsize=12)
			ax.set_ylabel("Human Individuals(P)\n[ranked_variance]",fontsize=12)
		ax.legend(lh,lt,bbox_to_anchor=(0.75,-0.3),fontsize=10,frameon=True)
		if scatter_ranked=="no":
			plt.savefig(output_folder + "figures/fig4bX_variance_comparison_26SProteasome.pdf",
						bbox_inches="tight",dpi=600)
		else:
			plt.savefig(output_folder + "figures/fig4bX_variance_comparison_26SProteasome_rankedVector.pdf",
						bbox_inches="tight",dpi=600)

	@staticmethod
	def load_boxplot_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		sig_data_gygi3 = data[data.ComplexID=="26S Proteasome"]
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_battle_protein.tsv.gz')
		sig_data_bp = data[data.ComplexID=="26S Proteasome"]
		return sig_data_gygi3,sig_data_bp

	@staticmethod
	def plot_boxplot_complexVariance(sig_data, **kwargs):
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		dataset = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		dataset = dataset[dataset.ComplexID=="26S Proteasome"]
		quantCols = utilsFacade.get_quantCols(sig_data)
		dataset = dataset[quantCols]
		proteinList = list(dataset.index)
		dataset.index = proteinList
		dataset = dataset.drop_duplicates()

		complexID = "26S Proteasome"
		sub = sig_data[sig_data.ComplexID==complexID]
		sub = sub.sort_values("relative_variance")
		pvalueDict = sub.to_dict()["levene_pval.adj"]
		varianceDict = sub.to_dict()["relative_variance"]
		mean_variance = np.mean(utilsFacade.finite(sub["relative_variance"]))
		stateDict = sub.to_dict()["state"]
		quant_sub = sub[quantCols].T
		dsub = dataset.T[quant_sub.columns]
		dsub = dsub.T.drop_duplicates()
		dsub = dsub.T

		original_dataList = list()
		dataList1 = list()
		proteins1 = list()
		dataList2 = list()
		proteins = list()
		proteins2 = ["Psmb7","Psmd10","Psmb6","Psmb5","Psmb8","Psmd9","Psmb9","Psmb10"]
		for c,col in enumerate(dsub.columns):
			if col!="Psma8" and col!="Psmd4":#not enough datapoints; mention in Materials&Methods
				if col in proteins2:
					dataList2.append(utilsFacade.finite(dsub.iloc[:,c]))
					proteins.append(col)
				else:
					dataList1.append(utilsFacade.finite(dsub.iloc[:,c]))
					proteins1.append(col)
		proteins2 = proteins

		immuno_proteasome = ["Psmb5","Psmb6","Psmb7","Psmb8","Psmb9","Psmb10"]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(15,6))
		gs = gridspec.GridSpec(3,3)
		ax = plt.subplot(gs[0:,0:2])
		ax.axhline(1, color = 'k', linestyle = '--')
		ax.axhline(0.5, color = 'k', linestyle = '--')
		ax.axhline(-1, color = 'k', linestyle = '--')
		ax.axhline(0, color = 'k', linestyle = '--')
		ax.axhline(-0.5, color = 'k', linestyle = '--')
		bp=ax.boxplot(dataList1,notch=0,sym="",vert=1,patch_artist=True,widths=[0.8]*len(dataList1))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black")
		plt.setp(bp['whiskers'], color="black")
		for i,patch in enumerate(bp['boxes']):
			protein=proteins1[i]
			x = numpy.random.normal(i+1, 0.04, size=len(dataList1[i]))
			if protein in immuno_proteasome:
				patch.set_facecolor("orange")	
				patch.set_edgecolor("orange")
			else:
				patch.set_facecolor("grey")	
				patch.set_edgecolor("grey")
			patch.set_alpha(0.8)
		plt.xticks(list(xrange(len(proteins1)+1)))
		ax.set_xlim(0,len(proteins1)+1)
		ax.set_xticklabels([""]+proteins1,fontsize=13,rotation=90)
		ax.set_ylabel("Relative Abundance",fontsize=13)
		plt.tick_params(axis="y",which="both",bottom="off",top="off",labelsize=12)
		complex_name = complexID.replace("/","_")
		complex_name = complex_name.replace(":","_")
		ax.set_ylim(-1.5,1.5)

		ax = plt.subplot(gs[0:,2:])
		ax.axhline(1, color = 'k', linestyle = '--')
		ax.axhline(0.5, color = 'k', linestyle = '--')
		ax.axhline(-1, color = 'k', linestyle = '--')
		ax.axhline(-0.5, color = 'k', linestyle = '--')
		ax.axhline(0, color = 'k', linestyle = '--')
		bp = ax.boxplot(dataList2,notch=0,sym="",vert=1,patch_artist=True,widths=[0.8]*len(dataList2))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black")
		plt.setp(bp['whiskers'], color="black")
		for i,patch in enumerate(bp['boxes']):
			protein = proteins2[i]
			x = numpy.random.normal(i+1, 0.04, size=len(dataList2[i]))
			if protein in immuno_proteasome:
				patch.set_facecolor("orange")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="brown",s=20)		
			elif protein.find("a")!=-1 or protein.find("b")!=-1:
				patch.set_facecolor("#95DCEC")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="darkblue",s=20)		
			elif protein.find("c")!=-1 or protein.find("d")!=-1:
				patch.set_facecolor("grey")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="black",s=20)		
			patch.set_edgecolor("black")
			patch.set_alpha(0.8)
		plt.xticks(list(xrange(len(proteins2)+1)))
		ax.set_xlim(0,len(proteins2)+1)
		ax.set_xticklabels([""]+[item.upper() for item in proteins2],fontsize=13,rotation=90)
		ax.set_yticklabels([])
		plt.tick_params(axis="y",which="both",bottom="off",top="off",labelsize=12)
		complex_name = complexID.replace("/","_")
		complex_name = complex_name.replace(":","_")
		ax.set_ylim(-1.5,1.5)
		plt.savefig(output_folder + "figures/fig4b_stochiometry_data_complexes_26SProteasome_gygi3.pdf",
					bbox_inches="tight",dpi=600)