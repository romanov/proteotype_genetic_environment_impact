class figure2a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE2A: main_figure2a_rocMatrix')
		figure2a.main_figure2a_rocMatrix(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure2a_rocMatrix(**kwargs):	
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('get_auc_matrix')
		auc_matrix = figure2a.get_auc_matrix(folder = folder)
		print("plot_auc_matrix")
		figure2a.plot_auc_matrix(auc_matrix, output_folder = output_folder)
		print('export_underlying_data')
		figure2a.export_underlying_data(auc_matrix, folder = folder, output_folder = output_folder)

	@staticmethod
	def get_auc_matrix(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		auc_matrix = DataFrameAnalyzer.open_in_chunks(folder, 'data/fig2a_auc_matrix.tsv.gz', sep = '\t')
		return auc_matrix

	@staticmethod
	def get_specific_color_gradient(colormap,inputList, **kwargs):
		vmin = kwargs.get("vmin", False)
		vmax = kwargs.get("vmax", False)
		cm = plt.get_cmap(colormap)
		if type(inputList)==list:
			if vmin == False and vmax == False:
				cNorm = mpl.colors.Normalize(vmin=min(inputList), vmax=max(inputList))
			else:
				cNorm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
		else:
			if vmin == False and vmax == False:
				cNorm = mpl.colors.Normalize(vmin=inputList.min(), vmax=inputList.max())
			else:
				cNorm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
		scalarMap = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)
		scalarMap.set_array(inputList)
		colorList = scalarMap.to_rgba(inputList)
		return scalarMap,colorList

	@staticmethod
	def plot_auc_matrix(auc_df, output_folder):
		datasets = ["tcga_ovarian","battle_protein",'colo_cancer',"gygi3",'tcga_breast',"gygi1",
					"mann_all_log2","primatePRO","wu","battle_ribo","battle_rna",
					"gygi2",'bxd_protein',"primateRNA","tiannan"]
		categories = ["chromosome","housekeeping","essential","pathway",
					  "compartment","string_700","complex"]
		data = auc_df.T
		data = data[categories]
		data = data.T[datasets].T

		data.index = ["TCGA Ovarian Cancer(P)",'Human Individuals(Battle,P)',
					  'TCGA Colorectal Cancer(P)','DO mouse strains(P)',"TCGA Breast Cancer(P)",
					  'Founder mouse strains (P)',
					  'Human cell lines(P)','Primate cells(P)','Human Individuals(Wu,P)',
					  'Human Individuals(RP)','Human Individuals(RS)',
					  'DO mouse strains(RS)','BXD80 mouse strains(P)',
					  'Primate cells(RS)','Kidney cancer cells(P)']
		data.columns = ['chromosome','housekeeping','essential',
						'pathway','compartment','STRING','complex']


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		x_mean_list = list()
		for col in data.columns:
			x_mean_list.append(np.mean(utilsFacade.finite(list(data[col])))-0.5)
		y_mean_list = list()
		for col in data.T.columns:
			y_mean_list.append(max(utilsFacade.finite(list(data.T[col])))-0.5)

		plt.clf()
		fig = plt.figure(figsize = (5,8))
		gs = gridspec.GridSpec(16,11)
		ax1_density = plt.subplot(gs[0:2,0:8])
		ax1_density.set_ylim(0.5,0.8)
		ax1_density.axhline(0.55, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.6, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.65, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.7, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.75, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		scalarmap_x, colorList_x = figure2a.get_specific_color_gradient(plt.cm.Greys,
															   			np.array(xrange(len(x_mean_list))))
		ax1_density.bar(np.arange(len(x_mean_list)), 
			x_mean_list, 0.95, color = colorList_x, bottom = 0.5,
			edgecolor = "white", linewidth = 2, zorder = 3)
		plt.xticks(list(xrange(len(data.columns))))
		ax1_density.set_xticklabels([])
		ax1_density.set_xlim(0,len(data.columns))
		ax = plt.subplot(gs[2:10,0:8])
		scalarmap, colorList = figure2a.get_specific_color_gradient(plt.cm.RdBu,
														   			np.array(data), vmin = 0.4, vmax = 0.7)
		sns.heatmap(data, cmap = plt.cm.RdBu, vmin = 0.4,vmax = 0.7,
			linecolor = "white", linewidth = 2, cbar = False)
		y_mean_list = y_mean_list[::-1]
		ax2_density = plt.subplot(gs[2:10,8:10])
		plt.yticks(list(xrange(len(data.index))))
		ax2_density.set_ylim(0,len(data.index))
		ax2_density.set_xlim(0.5,0.85)
		scalarmap_y, colorList_y = figure2a.get_specific_color_gradient(plt.cm.Greys,
															   			np.array(xrange(len(y_mean_list))))
		plt.setp(ax2_density.get_xticklabels(), rotation = 90)
		ax2_density.set_yticklabels([])
		ax2_density.axvline(0.7, color = "red", linestyle = "--", linewidth = 0.5)
		ax2_density.axvline(0.55, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.6, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.65, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.75, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.8, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.barh(np.arange(len(y_mean_list)), y_mean_list,
			0.95, color = colorList_y, left = 0.5,
			edgecolor = "white", linewidth = 2, zorder = 3)
		ax_category = plt.subplot(gs[2:10,10:11])
		df = pd.DataFrame({"color":["green"]*7+["lightgreen"]+["magenta"]*2+["green","magenta","green"]})
		df = pd.DataFrame({'color':14*[1]})
		sns.heatmap(df, cbar = False, linewidth = 2, linecolor = "white")
		#ax_category.scatter([1]*len(df),xrange(len(df)), color = df["color"], marker = "s", s=200)
		ax_category.axis("off")
		ax_cbar = plt.subplot(gs[13:14,0:8])
		cbar = fig.colorbar(scalarmap, cax = ax_cbar, 
			   orientation = "horizontal")
		cbar.set_label("Area under curve (AUC)")
		plt.savefig(output_folder + "figures/fig2a_auc_matrix.pdf",
					bbox_inches = "tight", dpi=600)

	@staticmethod
	def export_underlying_data(auc_matrix, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		folder = "/g/scb2/bork/romanov/wpc/data/figure1_data/"#LOCAL DIRECTORY TO BE CHANGED

		category_list = ['chromosome', 'compartment', 'complex',
						 'housekeeping', 'jiyoye_essentiality', 'k562_essentiality',
						 'kbm7_essentiality', 'pathway', 'raji_essentiality',
						 'string_700']
		name_list = ["battle_protein","tiannan","battle_ribo", "battle_rna",
					 "primateRNA", "primatePRO", "wu","mann_all_log2","yibo",
					 "gygi1","gygi2","gygi3","tcga_ovarian",'tcga_breast',
					 'bxd_protein','colo_cancer']		

		concat_list = list()
		names_columns = list()
		category_columns = list()
		for name in name_list:
			for category in category_list:
				print(name,category)
				file_name = name + '_figure1_data_' + category.upper() + '.tsv.gz'
				data = DataFrameAnalyzer.open_in_chunks(folder, file_name)
				concat_list.append(data)
				for i in xrange(len(data)):
					names_columns.append(name)
					category_columns.append(category)
		data = pd.concat(concat_list)
		data['name'] = pd.Series(names_columns, index = data.index)
		data['category'] = pd.Series(category_columns, index = data.index)
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/'#LOCAL DIRECTORY TO BE CHANGED
		data.to_csv(output_folder + 'data/suppTable2_fig2a_underlying_data_ROCanalysis_' + time.strftime('%Y%m%d') +'.tsv',
					sep = '\t')

class figure2b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE2B: main_figure2b_vignette_complexEffect')
		figure2b.main_figure2b_vignette_complexEffect(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure2b_vignette_complexEffect(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('plot_complex_effect:tcga_ovarian')
		figure2b.plot_complex_effect('tcga_ovarian')
		print('plot_complex_effect:battle_protein')
		figure2b.plot_complex_effect('battle_protein')
		print('plot_complex_effect:gygi1')
		figure2b.plot_complex_effect('gygi1')
		print('plot_complex_effect:gygi3')
		figure2b.plot_complex_effect('gygi3')
		print('plot_complex_effect:tcga_breast')
		figure2b.plot_complex_effect('tcga_breast')
		print('plot_complex_effect:colo_cancer')
		figure2b.plot_complex_effect('colo_cancer')

	@staticmethod
	def get_data(name, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/' + name + '_figure1_data_COMPLEX.tsv.gz')
		return complex_data

	@staticmethod
	def plot_complex_effect(name, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_data = figure2b.get_data(name, folder = folder)
		other_correlations = utilsFacade.finite(list(complex_data[complex_data.label==False]["correlations"]))
		complex_correlations = utilsFacade.finite(list(complex_data[complex_data.label==True]["correlations"]))

		pval_list1 = list()
		for i in xrange(1,1000):
			corr1 = random.sample(complex_correlations,100)
			corr2 = random.sample(other_correlations,100)
			odds1, pval1 = scipy.stats.ranksums(corr1, corr2)
			pval_list1.append(pval1)
		print(np.mean(pval_list1))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		ax.set_ylabel('Density', fontsize=12)
		ax.set_xlabel('correlation coefficient (pearson)', fontsize=12)
		ax.hist(other_correlations, bins = 50, color='grey', alpha =0.3, normed = 1)
		plottingFacade.func_plotDensities_border(ax, other_correlations, 
												 linewidth = 2, alpha = 1, facecolor = 'grey')
		ax.hist(complex_correlations, bins = 50, color='#AF2D2D', alpha =0.3, normed =1)
		plottingFacade.func_plotDensities_border(ax, complex_correlations, 
												 linewidth = 2, alpha = 1, facecolor = '#AF2D2D')
		plottingFacade.make_full_legend(ax,['n(other)='+str(len(other_correlations)),
			'n(complex)='+str(len(complex_correlations)),
			'pvalComplex(Wilcoxon)='+str(np.mean(pval_list1))],['grey']*3, loc = 'upper left')
		ax.set_xlim(-1,1)
		plt.savefig(output_folder + 'figures/fig2b_' + name + '_complex_density_effect.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure2c_addendum:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('main_figure2c_atp_synthase')
		figure2c_addendum.main_figure2c_atp_synthase(output_folder = output_folder)
		print('additional_figure2c_all_complexes')
		figure2c_addendum.additional_figure2c_all_complexes(output_folder = output_folder)

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_rna.tsv.gz')
		ribo_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_ribo.tsv.gz')
		pro_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_protein.tsv.gz')
		return rna_data, ribo_data, pro_data

	@staticmethod
	def get_correlation_values(sub):
		quant_cols = utilsFacade.filtering(list(sub.columns),'quant_')
		quant_sub = sub[quant_cols]
		corr_data = quant_sub.T.corr()
		corr_values = utilsFacade.get_correlation_values(corr_data)
		return corr_values

	@staticmethod
	def get_complex_internal_corr_values_per_control_level(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data, ribo_data, pro_data = figure2c_addendum.get_data(folder = output_folder)
		complexes = list(set(pro_data.complex_search))
		pro_corr_values = list()
		rna_corr_values = list()
		ribo_corr_values = list()
		for complexID in complexes:
			pro_sub = pro_data[pro_data.complex_search==complexID]
			pro_corrs = figure2c_addendum.get_correlation_values(pro_sub)
			pro_corr_values.append(pro_corrs)
		for complexID in complexes:
			rna_sub = rna_data[rna_data.complex_search==complexID]
			rna_corrs = figure2c_addendum.get_correlation_values(rna_sub)
			rna_corr_values.append(rna_corrs)
		for complexID in complexes:
			ribo_sub = ribo_data[ribo_data.complex_search==complexID]
			ribo_corrs = figure2c_addendum.get_correlation_values(ribo_sub)
			ribo_corr_values.append(ribo_corrs)
		
		pro_corr_values = utilsFacade.flatten(pro_corr_values)
		rna_corr_values = utilsFacade.flatten(rna_corr_values)
		ribo_corr_values = utilsFacade.flatten(ribo_corr_values)
		return rna_corr_values, ribo_corr_values, pro_corr_values

	@staticmethod
	def additional_figure2c_all_complexes(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		corrs = figure2c_addendum.get_complex_internal_corr_values_per_control_level(output_folder = output_folder)
		rna_corr_values, ribo_corr_values, pro_corr_values = corrs
		
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True
		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		plottingFacade.func_plotDensities_border(ax, rna_corr_values, facecolor = 'purple', linewidth = 1)
		plottingFacade.func_plotDensities_border(ax, ribo_corr_values, facecolor = 'lightgreen', linewidth = 1)
		plottingFacade.func_plotDensities_border(ax, pro_corr_values, facecolor = 'green', linewidth = 1)
		ax.set_xlim(-1,1)
		ax.set_xlabel('Pearson correlation coefficient')
		ax.set_ylabel('Density')
		plt.savefig(output_folder + 'figures/fig2c_additional_buffering_allComplexes.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def main_figure2c_atp_synthase(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data, ribo_data, pro_data = figure2c_addendum.get_data(folder = output_folder)
		
		remove_proteins = ['RASSF1']
		rna_data = rna_data[rna_data.ComplexName.str.contains('F0/F1 ATP')]
		ribo_data = ribo_data[ribo_data.ComplexName.str.contains('F0/F1 ATP')]
		pro_data = pro_data[pro_data.ComplexName.str.contains('F0/F1 ATP')]
		rna_data = rna_data.drop(remove_proteins, axis = 0)
		ribo_data = ribo_data.drop(remove_proteins, axis = 0)

		rna_cols = utilsFacade.filtering(list(rna_data.columns), 'quant', condition = 'startswith')
		ribo_cols = utilsFacade.filtering(list(ribo_data.columns), 'quant', condition = 'startswith')
		pro_cols = filter(lambda a:str(a)!='quant_GM18871',utilsFacade.filtering(list(pro_data.columns),
				   'quant', condition = 'startswith'))

		rna_data = rna_data[rna_cols]
		ribo_data = ribo_data[ribo_cols]
		pro_data = pro_data[pro_cols]

		rna_corr = rna_data.T.corr()
		ribo_corr = ribo_data.T.corr()
		pro_corr = pro_data.T.corr()


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		sc_pro, colorList_pro = colorFacade.get_specific_color_gradient(plt.cm.Greens,
								np.array(list(xrange(len(pro_data.index)))), vmin = -1, vmax = 1)
		sc_rna, colorList_rna = colorFacade.get_specific_color_gradient(plt.cm.Purples,
								np.array(list(xrange(len(rna_data.index)))), vmin = -1, vmax = 1)
		sc_ribo, colorList_ribo = colorFacade.get_specific_color_gradient(plt.cm.YlGn,
								  np.array(list(xrange(len(ribo_data.index)))), vmin = -1, vmax = 1)

		rna_df = sns.clustermap(rna_corr).data2d
		pro_df = sns.clustermap(pro_corr).data2d
		ribo_df = sns.clustermap(ribo_corr).data2d

		plt.clf() 
		fig = plt.figure(figsize = (10,12)) 
		ax1 = fig.add_subplot(321)

		for p,protein in enumerate(list(rna_data.index)):
			temp = list(rna_data.loc[protein])
			ax1.plot(list(xrange(len(temp))), temp, color = sc_rna.to_rgba(p), linewidth = 0.2)
		ax1.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(rna_corr.mean())))]
		ax1.legend(lh, lt , loc = 'upper left')
		
		ax2 = fig.add_subplot(322)
		sns.heatmap(rna_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax2.set_xticklabels([])
		ax2.set_yticklabels([])

		ax3= fig.add_subplot(323)
		for p,protein in enumerate(list(ribo_data.index)):
			temp = list(ribo_data.loc[protein])
			ax3.plot(list(xrange(len(temp))), temp, color = sc_ribo.to_rgba(p), linewidth = 0.2)
		ax3.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(ribo_corr.mean())))]
		ax3.legend(lh, lt , loc = 'upper left')

		ax4 = fig.add_subplot(324)
		sns.heatmap(ribo_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax4.set_xticklabels([])
		ax4.set_yticklabels([])

		ax5 = fig.add_subplot(325)
		for p,protein in enumerate(list(pro_data.index)):
			temp = list(pro_data.loc[protein])
			ax5.plot(list(xrange(len(temp))), temp, color = sc_pro.to_rgba(p), linewidth = 0.2)
		ax5.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(pro_corr.mean())))]
		ax5.legend(lh, lt , loc = 'upper left')

		ax6 = fig.add_subplot(326)
		sns.heatmap(pro_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax6.set_xticklabels([])
		ax6.set_yticklabels([])

		plt.savefig(output_folder + 'figures/fig2c_buffering_atpSynthase.pdf',bbox_inches = 'tight', dpi = 400)
