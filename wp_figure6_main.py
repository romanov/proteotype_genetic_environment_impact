class figure6a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE6A: get_considered_pathways')
		considered_pathways = figure6a.get_considered_pathways(folder = folder)
		print('FIGURE6A: plot')
		figure6a.plot(considered_pathways, folder = folder)

	@staticmethod
	def load_multivariate_module_specific_data(dataset, module_type, datatype, covariate, **kwargs):
		n_subunit = kwargs.get('n_subunit',0)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_' + dataset + '/' + module_type + '/'
		joint_name = '_'.join([dataset, module_type, datatype,covariate])
		filename = joint_name + '_data_all_multi_covariate_moduleSpecific_empiricalFDR.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data[data['n.subunits']>=n_subunit]
		return data	

	@staticmethod
	def load_multivariate_combined_module_specific_data(dataset, module_type, datatype, **kwargs):
		n_subunit = kwargs.get('n_subunit',0)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_' + dataset + '/' + module_type + '/'
		joint_name = '_'.join([dataset, module_type, datatype])
		filename = joint_name + '_data_all_multi_covariate_moduleSpecific_COMBINEDEFFECT.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data[data['n.subunits']>=n_subunit]
		return data

	@staticmethod
	def load_combined_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		comb_all_quant = data[data.covariates=='sex_diet']

		comb_mquant_complex = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							  'complex','quant', n_subunit = su)
		comb_mquant_corum = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							'corum','quant', n_subunit = su)
		comb_mstoch_complex = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							  'complex','stoichiometry', n_subunit = su)
		comb_mstoch_corum = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							'corum','stoichiometry', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mquant_pathway = comb_mquant_pathway[comb_mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mstoch_pathway = comb_mstoch_pathway[comb_mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_loc = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mquant_loc = comb_mquant_loc[comb_mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mstoch_loc = comb_mstoch_loc[comb_mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_string = DataFrameAnalyzer.open_in_chunks(folder,filename)

		comb_dict = {'complex':{'quant':comb_mquant_complex,'stoichiometry':comb_mstoch_complex},
					 'corum':{'quant':comb_mquant_corum,'stoichiometry':comb_mstoch_corum},
					 'pathway':{'quant':comb_mquant_pathway,'stoichiometry':comb_mstoch_pathway},
					 'loc':{'quant':comb_mquant_loc,'stoichiometry':comb_mstoch_loc},
					 'string':{'quant':comb_mquant_string},
					 'all':{'quant':comb_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(comb_dict, output_folder + 'combined_dictionary.pkl')

		return {'complex':{'quant':comb_mquant_complex,'stoichiometry':comb_mstoch_complex},
				'corum':{'quant':comb_mquant_corum,'stoichiometry':comb_mstoch_corum},
				'pathway':{'quant':comb_mquant_pathway,'stoichiometry':comb_mstoch_pathway},
				'loc':{'quant':comb_mquant_loc,'stoichiometry':comb_mstoch_loc},
				'string':{'quant':comb_mquant_string},
				'all':{'quant':comb_all_quant}}

	@staticmethod
	def load_sex_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		sex_all_quant = data[data.covariates=='sex']

		mquant_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','quant','sex', n_subunit = su)
		mstoch_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','stoichiometry','sex', n_subunit = su)
		mquant_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','quant','sex', n_subunit = su)
		mstoch_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','stoichiometry','sex', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_pathway = mquant_pathway[mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_pathway = mstoch_pathway[mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_loc = mquant_loc[mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_loc = mstoch_loc[mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_string = DataFrameAnalyzer.open_in_chunks(folder, filename)

		sex_dict = {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
					'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
					'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
					'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
					'string':{'quant':mquant_string},
					'all':{'quant':sex_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(sex_dict, output_folder + 'sex_effect_dictionary.pkl')


		return {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
				'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
				'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
				'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
				'string':{'quant':mquant_string},
				'all':{'quant':sex_all_quant}}

	@staticmethod
	def load_diet_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		diet_all_quant = data[data.covariates=='diet']

		mquant_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','quant','diet', n_subunit = su)
		mstoch_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','stoichiometry','diet', n_subunit = su)
		mquant_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','quant','diet', n_subunit = su)
		mstoch_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','stoichiometry','diet', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_pathway = mquant_pathway[mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_pathway = mstoch_pathway[mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_loc = mquant_loc[mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_loc = mstoch_loc[mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_string = DataFrameAnalyzer.open_in_chunks(folder, filename)

		diet_dict = {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
					 'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
					 'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
					 'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
					 'string':{'quant':mquant_string},
					 'all':{'quant':diet_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(diet_dict, output_folder + 'diet_effect_dictionary.pkl')

		return {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
				'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
				'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
				'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
				'string':{'quant':mquant_string},
				'all':{'quant':diet_all_quant}}

	@staticmethod
	def export_considered_pathways():
		#pathways are considered when not different from random, but different from complex (<0.5)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		df = DataFrameAnalyzer.getFile(folder,'corr_classification_pathways.tsv')
		sub = df[df.pval1>0.1]
		sub = sub[sub.pval1_complex<0.1]
		considered_pathways = list(sub.index)

		df.to_csv('/g/scb2/bork/romanov/wpc/wpc_package/data/corr_classification_pathways.tsv.gz', sep = '\t', compression = 'gzip')
		return considered_pathways

	@staticmethod
	def get_considered_pathways(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		df = DataFrameAnalyzer.getFile(folder,'data/corr_classification_pathways.tsv.gz')
		sub = df[df.pval1>0.1]
		sub = sub[sub.pval1_complex<0.1]
		considered_pathways = list(sub.index)		
		return considered_pathways

	@staticmethod
	def plot(considered_pathways, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		comb_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/combined_dictionary.pkl')
		sex_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/sex_effect_dictionary.pkl')
		diet_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/diet_effect_dictionary.pkl')

		#comb_dict = figure6a.load_combined_data(5)
		#sex_dict = figure6a.load_sex_data(5)
		#diet_dict = figure6a.load_diet_data(5)

		for key in ['all','complex','pathway','loc','string']:
			if key == 'all':
				quant_sex_all = list(set(sex_dict[key]['quant']['r2.all.module']))
				quant_diet_all = list(set(diet_dict[key]['quant']['r2.all.module']))
				quant_comb_all = list(set(comb_dict[key]['quant']['r2.all.module']))
			elif key == 'complex':
				quant_sex_complex = list(set(sex_dict[key]['quant']['r2.all.module']))
				stoch_sex_complex = list(set(sex_dict[key]['stoichiometry']['r2.all.module']))
				quant_diet_complex = list(set(diet_dict[key]['quant']['r2.all.module']))
				stoch_diet_complex = list(set(diet_dict[key]['stoichiometry']['r2.all.module']))
				quant_comb_complex = list(set(comb_dict[key]['quant']['r2.all.module']))
				stoch_comb_complex = list(set(comb_dict[key]['stoichiometry']['r2.all.module']))

				quant_sex_corum = list(set(sex_dict['corum']['quant']['r2.all.module']))
				stoch_sex_corum = list(set(sex_dict['corum']['stoichiometry']['r2.all.module']))
				quant_diet_corum = list(set(diet_dict['corum']['quant']['r2.all.module']))
				stoch_diet_corum = list(set(diet_dict['corum']['stoichiometry']['r2.all.module']))
				quant_comb_corum = list(set(comb_dict['corum']['quant']['r2.all.module']))
				stoch_comb_corum = list(set(comb_dict['corum']['stoichiometry']['r2.all.module']))
			elif key == 'pathway':
				mquant_sex = sex_dict[key]['quant']
				mquant_sex = mquant_sex[~mquant_sex['complex.name'].isin(considered_pathways)]
				quant_sex_pathway = list(set(mquant_sex['r2.all.module']))
				mstoch_sex = sex_dict[key]['stoichiometry']
				mstoch_sex = mstoch_sex[~mstoch_sex['complex.name'].isin(considered_pathways)]
				stoch_sex_pathway = list(set(mstoch_sex['r2.all.module']))
				mquant_diet = diet_dict[key]['quant']
				mquant_diet = mquant_diet[~mquant_diet['complex.name'].isin(considered_pathways)]
				quant_diet_pathway = list(set(mquant_diet['r2.all.module']))
				mstoch_diet = diet_dict[key]['stoichiometry']
				mstoch_diet = mstoch_diet[~mstoch_diet['complex.name'].isin(considered_pathways)]
				stoch_diet_pathway = list(set(mstoch_diet['r2.all.module']))
				mquant_comb = comb_dict[key]['quant']
				mquant_comb = mquant_comb[~mquant_comb['complex.name'].isin(considered_pathways)]
				quant_comb_pathway = list(set(mquant_comb['r2.all.module']))
				mstoch_comb = comb_dict[key]['stoichiometry']
				mstoch_comb = mstoch_comb[~mstoch_comb['complex.name'].isin(considered_pathways)]
				stoch_comb_pathway = list(set(mstoch_comb['r2.all.module']))
			elif key == 'loc':
				quant_sex_loc = list(set(sex_dict[key]['quant']['r2.all.module']))
				stoch_sex_loc = list(set(sex_dict[key]['stoichiometry']['r2.all.module']))
				quant_diet_loc = list(set(diet_dict[key]['quant']['r2.all.module']))
				stoch_diet_loc = list(set(diet_dict[key]['stoichiometry']['r2.all.module']))
				quant_comb_loc = list(set(comb_dict[key]['quant']['r2.all.module']))
				stoch_comb_loc = list(set(comb_dict[key]['stoichiometry']['r2.all.module']))
			elif key == 'string':
				quant_sex_string = list(set(sex_dict[key]['quant']['r2.all.module']))
				quant_diet_string = list(set(diet_dict[key]['quant']['r2.all.module']))
				quant_comb_string = list(set(comb_dict[key]['quant']['r2.all.module']))

		r2_quant_sex_complex = quant_sex_complex# + quant_sex_corum
		r2_stoch_sex_complex = stoch_sex_complex# + stoch_sex_corum
		r2_quant_diet_complex = quant_diet_complex# + quant_diet_corum
		r2_stoch_diet_complex = stoch_diet_complex# + stoch_diet_corum
		r2_quant_comb_complex = quant_comb_complex# + quant_comb_corum
		r2_stoch_comb_complex = stoch_comb_complex# + stoch_comb_corum
		r2_quant_sex_complex = quant_sex_complex + quant_sex_pathway
		r2_stoch_sex_complex = stoch_sex_complex + stoch_sex_pathway
		r2_quant_diet_complex = quant_diet_complex + quant_diet_pathway
		r2_stoch_diet_complex = stoch_diet_complex + stoch_diet_pathway
		r2_quant_comb_complex = quant_comb_complex + quant_comb_pathway
		r2_stoch_comb_complex = stoch_comb_complex + stoch_comb_pathway

		r2_complex_list = [r2_quant_sex_complex, r2_stoch_sex_complex,
						   r2_quant_diet_complex, r2_stoch_diet_complex,
						   r2_quant_comb_complex, r2_stoch_comb_complex]
		r2_pathway_list = [quant_sex_pathway, stoch_sex_pathway,
						   quant_diet_pathway, stoch_diet_pathway,
						   quant_comb_pathway, stoch_comb_pathway]
		r2_string_list = [quant_sex_string, quant_diet_string, quant_comb_string]
		r2_all_list = [quant_sex_all, quant_diet_all, quant_comb_all]

		r2_list = [r2_stoch_comb_complex, r2_quant_comb_complex, quant_comb_all,
				   r2_stoch_diet_complex, r2_quant_diet_complex, quant_diet_all,
				   r2_stoch_sex_complex, r2_quant_sex_complex, quant_sex_all]

		label_all_list = ['qsex_all','qdiet_all','qcomb_all']
		label_complex_list = ['qsex_complex','ssex_complex', 
							  'qdiet_complex', 'sdiet_complex',
					  		  'qcomb_complex','scomb_complex']
		label_pathway_list = ['qsex_pathway','ssex_pathway',
							  'qdiet_pathway','sdiet_pathway',
							  'qcomb_pathway','scomb_pathway']
		label_string_list = ['qsex_string','qdiet_string','qcomb_string']
		label_list = ['scomb_complex','qcomb_complex','qcomb_all',
					  'sdiet_complex','qdiet_complex','qdiet_all',
					  'ssex_complex','qsex_complex','qsex_all']

		color_list = ['lightblue','blue','lightgreen','green','grey','black']
		big_color_list = ['black','grey','grey','green','lightgreen','lightgreen','blue','lightblue','lightblue']
		small_color_list = ['lightblue','lightgreen','grey']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (10,10))
		#gs = gridspec.GridSpec(15,15)
		gs = gridspec.GridSpec(9,9)
		
		'''
		ax = plt.subplot(gs[0:2,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_all_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_all_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(small_color_list[i])
		plt.yticks(list(xrange(len(label_all_list))))
		ax.set_yticklabels(label_all_list)
		'''

		ax = plt.subplot(gs[0:6,0:])
		ax.set_xlim(-0.01,0.4)
		bp = ax.boxplot(r2_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.8]*len(r2_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(big_color_list[i])
		plt.yticks(list(xrange(len(label_list))))
		ax.set_yticklabels(label_list)
		print(scipy.stats.f_oneway(r2_list[0]+r2_list[1],r2_list[2])[1])
		print(scipy.stats.f_oneway(r2_list[3]+r2_list[4],r2_list[5])[1])
		print(scipy.stats.f_oneway(r2_list[6]+r2_list[7],r2_list[8])[1])
		ax.axvline(0.1, color = 'black', linestyle = '--')
		ax.axvline(0.2, color = 'black', linestyle = '--')
		ax.axvline(0.3, color = 'black', linestyle = '--')
		'''
		ax = plt.subplot(gs[6:10,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_pathway_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_pathway_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
		plt.yticks(list(xrange(len(label_pathway_list))))
		ax.set_yticklabels(label_pathway_list)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[10:12,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_string_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_string_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(small_color_list[i])
		plt.yticks(list(xrange(len(label_string_list))))
		ax.set_yticklabels(label_string_list)
		ax.set_xticklabels([])
		'''

		ax = plt.subplot(gs[6:9,0:])
		ax.axis('off')
		plt.savefig(folder + 'figures/fig6a_combinedEffect_analysis.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure6c:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE6C: plot_effectSize_complexDistribution')
		figure6c.plot_effectSize_complexDistribution(folder = folder)

	@staticmethod
	def export_data():
		folder = '/g/scb2/bork/romanov/wpc/'
		mouse_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv')
		mouse_pval_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv')
		human_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv')
		human_pval_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv')


		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/'
		mouse_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		mouse_pval_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		human_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		human_pval_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv.gz', sep = '\t', compression = 'gzip')

		return {'mouse': (mouse_df, mouse_pval_df),
				'human': (human_df, human_pval_df)}

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		mouse_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv.gz')
		mouse_pval_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv.gz')
		human_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv.gz')
		human_pval_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv.gz')
		return {'mouse': (mouse_df, mouse_pval_df),
				'human': (human_df, human_pval_df)}		

	@staticmethod
	def plot_effectSize_complexDistribution(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure6c.get_data()
		mouse_df, mouse_pval_df = data_dict['mouse']
		human_df, human_pval_df = data_dict['human']

		mouse_df.columns = ['mouse_'+item for item in list(mouse_df.columns)]
		mouse_pval_df.columns = ['mouse_'+item for item in list(mouse_pval_df.columns)]
		human_df.columns = ['human_'+item for item in list(human_df.columns)]
		human_pval_df.columns = ['human_'+item for item in list(human_pval_df.columns)]

		human_dict_quant = human_df['human_sex_quant'].to_dict()
		human_dict_stoch = human_df['human_sex_stoch'].to_dict()
		human_dict_pval_quant = human_pval_df['human_sex_quant'].to_dict()
		human_dict_pval_stoch = human_pval_df['human_sex_stoch'].to_dict()

		human_quant_list = list()
		human_stoch_list = list()
		human_quant_pvallist = list()
		human_stoch_pvallist = list()
		for key in list(mouse_df.index):
			try:
				human_quant_effectSize = human_dict_quant[key]
				human_quant_pval = human_dict_pval_quant[key]
			except:
				human_quant_effectSize = np.nan
				human_quant_pval = np.nan
			try:
				human_stoch_effectSize = human_dict_stoch[key]
				human_stoch_pval = human_dict_pval_stoch[key]
			except:
				human_stoch_effectSize = np.nan
				human_stoch_pval = np.nan
			human_quant_list.append(human_quant_effectSize)
			human_stoch_list.append(human_stoch_effectSize)
			human_quant_pvallist.append(human_quant_pval)
			human_stoch_pvallist.append(human_stoch_pval)
		'''
		mouse_df['human_sex_quant'] = pd.Series(human_quant_list, index = mouse_df.index)
		mouse_df['human_sex_stoch'] = pd.Series(human_stoch_list, index = mouse_df.index)
		mouse_pval_df['human_sex_quant'] = pd.Series(human_quant_pvallist, index = mouse_df.index)
		mouse_pval_df['human_sex_stoch'] = pd.Series(human_stoch_pvallist, index = mouse_df.index)
		'''

		mouse_df = mouse_df.replace(np.nan,-100)
		mdf, proteinList = utilsFacade.recluster_matrix_only_rows(mouse_df)
		mdf = mdf.replace(-100, np.nan)
		mdf = mdf.T

		ranked_sorted_list = list()
		for i,row in mdf.iterrows():
			temp = list()
			for item in list(row):
				if str(item)!='nan':
					temp.append(item)
			ranked_temp = rankdata(temp)
			rank_dict = dict()
			for t,r in zip(temp, ranked_temp):
				rank_dict[t] = r
			final_temp = list()
			for t in list(row):
				if str(t)!='nan':
					final_temp.append(rank_dict[t])
				else:
					final_temp.append(np.nan)
			ranked_sorted_list.append(final_temp)
		ranked_df = pd.DataFrame(ranked_sorted_list)
		ranked_df.index = mdf.index
		ranked_df.columns = mdf.columns

		sex_quant_list = [item*100 for item in list(mdf.T['mouse_sex_quant'])]
		sex_stoch_list = [item*100 for item in list(mdf.T['mouse_sex_stoch'])]
		sex_sum_list =  np.array(sex_quant_list) + np.array(sex_stoch_list)
		diet_quant_list = [item*100 for item in list(mdf.T['mouse_diet_quant'])]
		diet_stoch_list = [item*100 for item in list(mdf.T['mouse_diet_stoch'])]
		key_list = list(mdf.columns)
		lists = [sex_sum_list, sex_quant_list, sex_stoch_list, diet_quant_list, diet_stoch_list, key_list]
		sorted_lists = utilsFacade.sort_multiple_lists(lists, reverse = True)
		sex_sum_list, sex_quant_list, sex_stoch_list, diet_quant_list, diet_stoch_list, key_list = sorted_lists
		ranked_df = ranked_df[key_list]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (17,10))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:4,0:])
		ax.axhline(10,color = 'k', linestyle = '--')
		ax.axhline(20,color = 'k', linestyle = '--')
		ax.axhline(30,color = 'k', linestyle = '--')
		ax.axhline(40,color = 'k', linestyle = '--')
		ax.axhline(50,color = 'k', linestyle = '--')
		ind = list(xrange(len(sex_quant_list)))
		width = 1
		rects = ax.bar(ind, sex_quant_list, width, color='lightblue',
			edgecolor = 'white')
		rects = ax.bar(ind, sex_stoch_list, width, color='darkblue',
			edgecolor = 'white', bottom = np.array(sex_quant_list))

		ax.set_xlim(-0.5,len(sex_quant_list)+0.5)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[4:8,0:])
		ax.set_ylim(-60,0)
		ax.axhline(-10,color = 'k', linestyle = '--')
		ax.axhline(-20,color = 'k', linestyle = '--')
		ax.axhline(-30,color = 'k', linestyle = '--')
		ax.axhline(-40,color = 'k', linestyle = '--')
		ax.axhline(-50,color = 'k', linestyle = '--')		
		ind = list(xrange(len(sex_quant_list)))
		width = 1
		rects = ax.bar(ind, (-1)*np.array(diet_quant_list), width, color='lightgreen',
			edgecolor = 'white')
		rects = ax.bar(ind, (-1)*np.array(diet_stoch_list), width, color='darkgreen',
			edgecolor = 'white', bottom = (-1)*np.array(diet_quant_list))
		ax.set_xlim(-0.5,len(diet_quant_list)+0.5)
		plt.xticks(list(utilsFacade.frange(0.5,len(ranked_df.columns)+0.5,1)))
		ax.set_xticklabels([':'.join(item.split(':')[1:]) for item in list(ranked_df.columns)],
						   rotation = 90, fontsize = 5)
		plt.savefig(folder + 'figures/fig6b_complex_effectSize_Distribution.pdf',
					bbox_inches = 'tight', dpi = 400)
