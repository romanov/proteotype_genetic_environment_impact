class figure5a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5A: figure5a_complex_abundanceChange_demonstration')
		figure5a.figure5a_complex_abundanceChange_demonstration()
		print('FIGURE5A: figure5a_overview_stoichiometry')
		figure5a.figure5a_overview_stoichiometry()

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = DataFrameAnalyzer.read_pickle(folder + 'data/figure5a_data_dictionary.pkl')
		relevant_complexes = ['HC663:COPII','HC2402:COPI','HC1479:retromer complex',
					  		  'SMC_AM000047:Cohesin complex']
		#tRNA splicing endonuclease:tRNA splicing endonuclease''
		return data_dict, relevant_complexes

	@staticmethod
	def figure5a_complex_abundanceChange_demonstration(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict, relevant_complexes = figure5a.get_data(folder = folder)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		positions = [1,1.5]
		plt.clf()
		fig = plt.figure(figsize = (10,5))
		ax = fig.add_subplot(141)
		dataList = [data_dict[relevant_complexes[0]][0],data_dict[relevant_complexes[0]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(142)
		dataList = [data_dict[relevant_complexes[1]][0],data_dict[relevant_complexes[1]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(143)
		dataList = [data_dict[relevant_complexes[2]][0],data_dict[relevant_complexes[2]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(144)
		dataList = [data_dict[relevant_complexes[3]][0],data_dict[relevant_complexes[3]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		plt.savefig(folder + 'figures/fig5a_complex_abundances.pdf',
					bbox_inches = 'tight',dpi = 400)

	@staticmethod
	def get_limma_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz',
				    							sep = '\t', compression = 'gzip')
		data = data.drop_duplicates()
		quant_data = data[data.analysis_type=='quant_male-female']		
		stoch_data = data[data.analysis_type=='stoch_male-female']

		'''
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz',
				    							sep = '\t', compression = 'gzip')
		data = data.drop_duplicates()
		quant_data = data[data.analysis_type=='quant_highfat-chow']		
		stoch_data = data[data.analysis_type=='stoch_highfat-chow']
		'''
		return data, quant_data, stoch_data

	@staticmethod
	def prepare_stochiometry_hit_dictionary(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data = figure5a.get_limma_data(folder = folder)
		complex_set = list(set(data.complex_search))
		cov_sig_dict = dict()
		num_dict = dict()
		for complexID in complex_set:
			quant_sub = quant_data[quant_data.complex_search==complexID]
			stoch_sub = stoch_data[stoch_data.complex_search==complexID]
			if len(stoch_sub)>=4:
				num_dict[complexID] = len(stoch_sub)
				stoch_sig_sub = stoch_sub[stoch_sub['pval.adj']<=0.01]
				stoch_non_sub = stoch_sub[stoch_sub['pval.adj']>0.01]
				coverage = float(len(stoch_sig_sub))/float(len(stoch_sub))*100
				cov_sig_dict[complexID] = coverage
		DataFrameAnalyzer.to_pickle(cov_sig_dict, folder + 'data/figure5a_stoichiometric_hits_per_complex_dict.pkl')
		DataFrameAnalyzer.to_pickle(num_dict, folder + 'data/figure5a_stoichiometric_hitsNUM_per_complex_dict.pkl')
		#DataFrameAnalyzer.to_pickle(cov_sig_dict, folder + 'data/figure5a_stoichiometric_diet_hits_per_complex_dict.pkl')
		#DataFrameAnalyzer.to_pickle(num_dict, folder + 'data/figure5a_stoichiometric_diet_hitsNUM_per_complex_dict.pkl')
		return cov_sig_dict, num_dict

	@staticmethod
	def get_stoichiometric_hits_per_complex_dict(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		cov_sig_dict = DataFrameAnalyzer.read_pickle(folder + 'data/figure5a_stoichiometric_hits_per_complex_dict.pkl')

		'''
		for key in cov_sig_dict:
			if key.find('mitochondrial outer')!=-1:
				raise Exception()
		'''
		return cov_sig_dict

	@staticmethod
	def prepare_complex_abundance_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		ffolder = '/g/scb2/bork/romanov/wpc/'
		abu_data = DataFrameAnalyzer.getFile(ffolder,'gygi3_complex_total_abundance_difference.tsv')
		abu_data.to_csv(folder + 'data/figure5a_gygi3_complex_total_abundance_difference.tsv.gz',
						sep = '\t', compression = 'gzip')

		diet_abu_data = DataFrameAnalyzer.getFile(ffolder,'gygi3_complex_diet_total_abundance_difference.tsv')
		diet_abu_data.to_csv(folder + 'data/figure5a_gygi3_complex_diet_total_abundance_difference.tsv.gz',
							 sep = '\t', compression = 'gzip')				

	@staticmethod
	def get_complex_dictionary(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complexDict = DataFrameAnalyzer.read_pickle(folder + 'data/complex_dictionary.pkl')
		return complexDict

	@staticmethod
	def get_complex_abundance_data(cov_sig_dict, num_dict, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		abu_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/figure5a_gygi3_complex_diet_total_abundance_difference.tsv.gz')
		key_list = list(set(abu_data.index).intersection(set(cov_sig_dict.keys())))

		cohen_list = list()
		tt_pval_list  = list()
		complex_list = list()
		stable_fractions = list()
		variable_fractions = list()
		for key in key_list:
			num = num_dict[key]
			complexID = key.split(':')[0]
			gold = complexDict[complexID]['goldComplex'][0]
			if gold=='yes' and num>=4:
				coverage = cov_sig_dict[key]
				cohen_list.append(abu_data.loc[key]['cohen_distance'])
				tt_pval_list.append(abu_data.loc[key]['ttest_pval'])
				complex_list.append(':'.join(key.split(':')[1:]))
				stable_fractions.append(100-coverage)
				variable_fractions.append(coverage)
			if complexID.find('tRNA splicing')!=-1:
				coverage = cov_sig_dict[key]
				cohen_list.append(abu_data.loc[key]['cohen_distance'])
				tt_pval_list.append(abu_data.loc[key]['ttest_pval'])
				complex_list.append(':'.join(key.split(':')[1:]))
				stable_fractions.append(100-coverage)
				variable_fractions.append(coverage)

		cohen_list,complex_list,tt_pval_list,stable_fractions, variable_fractions = utilsFacade.sort_multiple_lists([cohen_list, complex_list,tt_pval_list, stable_fractions, variable_fractions], reverse = True)
		tt_pvalCorrs = statsFacade.correct_pvalues(tt_pval_list)

		#gather numbers
		print(scipy.stats.spearmanr(cohen_list,variable_fractions))

		tt1 = 'bla'
		for idx,tt in enumerate(tt_pvalCorrs):
			if tt>=0.01 and tt1=='bla':
				tt1 = cohen_list[idx]
			if tt1!='bla' and tt<=0.01:
				tt2 = cohen_list[idx]
				break

		df = {'complex':complex_list, 'cohen':cohen_list, 'tt_pval':tt_pval_list,
			  'stable':stable_fractions, 'variable':variable_fractions,
			  'tt_pvalCorr':tt_pvalCorrs, 'tt1':[tt1]*len(complex_list),
			  'tt2':[tt2]*len(complex_list)}
		df = pd.DataFrame(df)
		df.to_csv(folder + 'data/figure5a_bigPicture_underlying_data.tsv.gz',
				  sep = '\t', compression = 'gzip')
		'''
		df.to_csv(folder + 'data/figure5a_bigPicture_diet_underlying_data.tsv.gz',
				  sep = '\t', compression = 'gzip')

		df = DataFrameAnalyzer.open_in_chunks(folder, 'data/figure5a_bigPicture_diet_underlying_data.tsv.gz')
		'''
		return df

	@staticmethod
	def prepare_data_for_second_graph(**kwargs):
		data, quant_data, stoch_data = figure5a.get_limma_data(folder = folder)
		cov_sig_dict, num_dict = figure5a.get_stoichiometric_hits_per_complex_dict(folder = folder)
		complexDict = figure5a.get_complex_dictionary(folder = folder)
		df = figure5a.get_complex_abundance_data(cov_sig_dict, num_dict, folder = folder)

	@staticmethod
	def figure5a_overview_stoichiometry(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder,'data/figure5a_bigPicture_underlying_data.tsv.gz')
		complex_list = list(df['complex'])
		cohen_list = list(df['cohen'])
		tt_pval_list = list(df['tt_pval'])
		stable_fractions = list(df['stable'])
		variable_fractions = list(df['variable'])
		tt_pvalCorrs = list(df['tt_pvalCorr'])
		tt1 = list(df.tt1)[0]
		tt2 = list(df.tt2)[0]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:5,0:])
		ind = np.arange(len(complex_list))
		width = 0.85
		ax.axhline(0, color = 'k')
		ax.axhline(0.5,linestyle = '--', color = 'k')
		ax.axhline(-0.5,linestyle = '--', color = 'k')
		ax.axhline(1.0,linestyle = '--', color = 'k')
		ax.axhline(-1.0,linestyle = '--', color = 'k')
		ax.axhline(1.5,linestyle = '--', color = 'k')
		ax.axhline(-1.5,linestyle = '--', color = 'k')
		ax.axhline(tt1, color = 'red')
		ax.axhline(tt2, color = 'red')
		colors = list()
		for p,pval in enumerate(tt_pval_list):
			if str(pval)=='nan':
				colors.append('grey')
			else:
				if cohen_list[p]>=tt1:
					colors.append('purple')
				elif cohen_list[p]<=tt2:
					colors.append('lightgreen')
				else:
					colors.append('grey')
		rects = ax.bar(ind, cohen_list, width, color=colors,
			edgecolor = 'white')
		ax.set_ylim(-2,2)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[5:7,0:])
		rects1 = ax.bar(ind, stable_fractions, width, color='darkblue',
						edgecolor = 'white')
		rects2 = ax.bar(ind, variable_fractions, width, color='red',
						bottom =np.array(stable_fractions),
						edgecolor = 'white')
		ax.set_ylim(0,100)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		plt.xticks(list(utilsFacade.frange(0.5,len(complex_list)+0.5,1)))
		ax.set_xticklabels(complex_list,rotation = 90, fontsize = 7)
		plt.savefig(folder + 'figures/fig5a_abundance_stoichiometry_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5a_overview_stoichiometry_diet(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder,'data/figure5a_bigPicture_diet_underlying_data.tsv.gz')
		complex_list = list(df['complex'])
		cohen_list = list(df['cohen'])
		tt_pval_list = list(df['tt_pval'])
		stable_fractions = list(df['stable'])
		variable_fractions = list(df['variable'])
		tt_pvalCorrs = list(df['tt_pvalCorr'])
		tt1 = list(df.tt1)[0]
		tt2 = list(df.tt2)[0]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:5,0:])
		ind = np.arange(len(complex_list))
		width = 0.85
		ax.axhline(0, color = 'k')
		ax.axhline(0.5,linestyle = '--', color = 'k')
		ax.axhline(-0.5,linestyle = '--', color = 'k')
		ax.axhline(1.0,linestyle = '--', color = 'k')
		ax.axhline(-1.0,linestyle = '--', color = 'k')
		ax.axhline(1.5,linestyle = '--', color = 'k')
		ax.axhline(-1.5,linestyle = '--', color = 'k')
		ax.axhline(tt1, color = 'red')
		ax.axhline(tt2, color = 'red')
		colors = list()
		for p,pval in enumerate(tt_pval_list):
			if str(pval)=='nan':
				colors.append('grey')
			else:
				if cohen_list[p]>=tt1:
					colors.append('purple')
				elif cohen_list[p]<=tt2:
					colors.append('lightgreen')
				else:
					colors.append('grey')
		rects = ax.bar(ind, cohen_list, width, color=colors,
			edgecolor = 'white')
		ax.set_ylim(-2,2)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[5:7,0:])
		rects1 = ax.bar(ind, stable_fractions, width, color='darkblue',
						edgecolor = 'white')
		rects2 = ax.bar(ind, variable_fractions, width, color='red',
						bottom =np.array(stable_fractions),
						edgecolor = 'white')
		ax.set_ylim(0,100)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		plt.xticks(list(utilsFacade.frange(0.5,len(complex_list)+0.5,1)))
		ax.set_xticklabels(complex_list,rotation = 90, fontsize = 7)
		plt.savefig(folder + 'figures/fig5a_abundance_diet_stoichiometry_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure5b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5B: main_figure5b_volcanoPlots')
		figure5b.main_figure5b_volcanoPlots()
		print('FIGURE5B: main_figure5b_boxPlots')
		figure5b.main_figure5b_boxPlots()

	@staticmethod
	def get_data(name,**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_male-female']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains(name)]

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])
		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]
		return {'pval':pval_list, 'fc': fc_list, 'complex_pval': complex_pval_list,
				'complex_fc':complex_fc_list, 'complex_label': complex_label_list,
				'complex_color':complex_colors}

	@staticmethod
	def figure5b_volcanoPlot_retromer_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('retromer')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_retromer_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cop1_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('HC2402:COPI')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cop1_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_ste5MAPK_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('MAPK')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_mapkComplex_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cohesin_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('Cohesin')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cohesin_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cop2_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('HC663:COPII')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cop2_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def main_figure5b_volcanoPlots(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('volcano_plot_stoichiometry:COPI')
		figure5b.figure5b_volcanoPlot_cop1_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:COPII')
		figure5b.figure5b_volcanoPlot_cop2_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:Cohesin')
		figure5b.figure5b_volcanoPlot_cohesin_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:retromer complex')
		figure5b.figure5b_volcanoPlot_retromer_mfComparison(folder = output_folder)

	@staticmethod
	def main_figure5b_boxPlots(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('boxplot:COPI')
		figure5b.figure5b_cop1_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:COPII')
		figure5b.figure5b_cop2_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:retromer complex')
		figure5b.figure5b_retromer_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:Cohesin')
		figure5b.figure5b_cohesin_boxplot_logFC_male_female(folder = output_folder)

	@staticmethod
	def get_data_boxplot(name,**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains(name)]
		male_data = complex_data[complex_data.analysis_type=='male']
		female_data = complex_data[complex_data.analysis_type=='female']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_male-female']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		wpc_data = wpc_data[wpc_data.complex_search.str.contains(name)]
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'M')
		female_list = utilsFacade.filtering(quant_list, 'F')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=0.01:
				try:
					data_list.append(utilsFacade.finite(list(male_data[protein])))
					data_list.append(utilsFacade.finite(list(female_data[protein])))
					color_list.append('purple')
					color_list.append('green')
					sig_protein_list.append(protein)
					sig_protein_list.append(protein)
					positions.append(positions[-1]+0.4)
					positions.append(positions[-1]+0.6)
				except:
					data_list.append(utilsFacade.finite(list(male_data[protein].T.iloc[0].T)))
					data_list.append(utilsFacade.finite(list(female_data[protein].T.iloc[0].T)))
					color_list.append('purple')
					color_list.append('green')
					sig_protein_list.append(protein)
					sig_protein_list.append(protein)
					positions.append(positions[-1]+0.4)
					positions.append(positions[-1]+0.6)					

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		if name=='HC2402:COPI':
			sig_protein_list.append('COPI-male')
			sig_protein_list.append('COPI-female')
		elif name == 'HC663:COPII':
			sig_protein_list.append('COPII-male')
			sig_protein_list.append('COPII-female')
		elif name=='MAPK':
			sig_protein_list.append('MAPK-male')
			sig_protein_list.append('MAPK-female')
		elif name == 'Cohesin':
			sig_protein_list.append('Cohesin-male')
			sig_protein_list.append('Cohesin-female')			
		positions.append(positions[-1]+0.5)
		return {'data':data_list, 'color': color_list,
				'sig_protein':sig_protein_list, 'positions':positions}

	@staticmethod
	def figure5b_retromer_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('retromer')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
			 			widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPI complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_retromer_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def figure5b_cop1_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('HC2402:COPI')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
			 			widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPI complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cop1_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def figure5b_cop2_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('HC663:COPII')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPII complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cop2_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_ste5MAPK_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('MAPK')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('MAPK complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_mapk_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_cohesin_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('Cohesin')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('Cohesin complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cohesin_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure5c_addendum:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5C: plot_BP_go_for_main_figure')
		figure5c_addendum.plot_BP_go_for_main_figure()
		print('FIGURE5C: plot_CC_go_for_main_figure')
		figure5c_addendum.plot_CC_go_for_main_figure()

	@staticmethod
	def prepare_input():
		folder = '/g/scb2/bork/romanov/wpc/'
		fileName = 'gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		data = data.drop_duplicates()
		data.index = data.complex_search + '::' +data.index

		data = data[data['pval.adj']<=0.1]
		quant_data = data[data.analysis_type=='quant_male-female']
		stoch_data = data[data.analysis_type=='stoch_male-female']

		quant_indices = list(set(quant_data.index).difference(set(stoch_data.index)))
		stoch_indices = list(set(stoch_data.index).difference(set(quant_data.index)))

		quant_genes = list(set([item.split('::')[1] for item in quant_indices]))
		stoch_genes = list(set([item.split('::')[1] for item in stoch_indices]))
		quant_entrezgenes = Mapper(quant_genes, input = 'symbol', output = 'entrezgene')
		stoch_entrezgenes = Mapper(stoch_genes, input = 'symbol', output = 'entrezgene')
		quant_entrezgenes = map(int,utilsFacade.finite(list(quant_entrezgenes.trans_df['entrezgene'])))
		stoch_entrezgenes = map(int,utilsFacade.finite(list(stoch_entrezgenes.trans_df['entrezgene'])))
		o = open(folder + 'gender_quant_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,quant_entrezgenes))
		o.write(exportText)
		o.close()
		o = open(folder + 'gender_stoch_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,stoch_entrezgenes))
		o.write(exportText)
		o.close()

		fileName = 'gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		data = data.drop_duplicates()
		data.index = data.complex_search + '::' +data.index
		data = data[data['pval.adj']<=0.1]
		quant_data = data[data.analysis_type=='quant_highfat-chow']
		stoch_data = data[data.analysis_type=='stoch_highfat-chow']

		quant_indices = list(set(quant_data.index).difference(set(stoch_data.index)))
		stoch_indices = list(set(stoch_data.index).difference(set(quant_data.index)))

		quant_genes = list(set([item.split('::')[1] for item in quant_indices]))
		stoch_genes = list(set([item.split('::')[1] for item in stoch_indices]))
		quant_entrezgenes = Mapper(quant_genes, input = 'symbol', output = 'entrezgene')
		stoch_entrezgenes = Mapper(stoch_genes, input = 'symbol', output = 'entrezgene')
		quant_entrezgenes = map(int,utilsFacade.finite(list(quant_entrezgenes.trans_df['entrezgene'])))
		stoch_entrezgenes = map(int,utilsFacade.finite(list(stoch_entrezgenes.trans_df['entrezgene'])))
		o = open(folder + 'diet_quant_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,quant_entrezgenes))
		o.write(exportText)
		o.close()
		o = open(folder + 'diet_stoch_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,stoch_entrezgenes))
		o.write(exportText)
		o.close()

	@staticmethod
	def get_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		mf_quant_go_data = DataFrameAnalyzer.getFile(folder,'gender_quant_go_output.txt')
		mf_stoch_go_data = DataFrameAnalyzer.getFile(folder,'gender_stoch_go_output.txt')
		hs_quant_go_data = DataFrameAnalyzer.getFile(folder,'diet_quant_go_output.txt')
		hs_stoch_go_data = DataFrameAnalyzer.getFile(folder,'diet_stoch_go_output.txt')

		mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.FDR<=0.01]
		hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.FDR<=0.01]
		mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.FDR<=0.01]
		hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.FDR<=0.01]

		categories = ['GOTERM_BP_FAT','GOTERM_CC_FAT','GOTERM_MF_FAT']
		mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.isin(categories)]
		hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.isin(categories)]
		mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.isin(categories)]
		hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.isin(categories)]

		bp_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('BP')]
		bp_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('BP')]
		bp_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('BP')]
		bp_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('BP')]
		mf_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('MF')]
		mf_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('MF')]
		mf_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('MF')]
		mf_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('MF')]
		cc_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('CC')]
		cc_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('CC')]
		cc_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('CC')]
		cc_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('CC')]

		bp_mf_quant_go_data.index = pd.Series(list(bp_mf_quant_go_data.Term), index = bp_mf_quant_go_data.index)
		bp_hs_quant_go_data.index = pd.Series(list(bp_hs_quant_go_data.Term), index = bp_hs_quant_go_data.index)
		bp_mf_stoch_go_data.index = pd.Series(list(bp_mf_stoch_go_data.Term), index = bp_mf_stoch_go_data.index)
		bp_hs_stoch_go_data.index = pd.Series(list(bp_hs_stoch_go_data.Term), index = bp_hs_stoch_go_data.index)
		mf_mf_quant_go_data.index = pd.Series(list(mf_mf_quant_go_data.Term), index = mf_mf_quant_go_data.index)
		mf_hs_quant_go_data.index = pd.Series(list(mf_hs_quant_go_data.Term), index = mf_hs_quant_go_data.index)
		mf_mf_stoch_go_data.index = pd.Series(list(mf_mf_stoch_go_data.Term), index = mf_mf_stoch_go_data.index)
		mf_hs_stoch_go_data.index = pd.Series(list(mf_hs_stoch_go_data.Term), index = mf_hs_stoch_go_data.index)
		cc_mf_quant_go_data.index = pd.Series(list(cc_mf_quant_go_data.Term), index = cc_mf_quant_go_data.index)
		cc_hs_quant_go_data.index = pd.Series(list(cc_hs_quant_go_data.Term), index = cc_hs_quant_go_data.index)
		cc_mf_stoch_go_data.index = pd.Series(list(cc_mf_stoch_go_data.Term), index = cc_mf_stoch_go_data.index)
		cc_hs_stoch_go_data.index = pd.Series(list(cc_hs_stoch_go_data.Term), index = cc_hs_stoch_go_data.index)

		bp_mf_quant_fdr_dict = bp_mf_quant_go_data['FDR'].to_dict()
		bp_hs_quant_fdr_dict = bp_hs_quant_go_data['FDR'].to_dict()
		bp_mf_stoch_fdr_dict = bp_mf_stoch_go_data['FDR'].to_dict()
		bp_hs_stoch_fdr_dict = bp_hs_stoch_go_data['FDR'].to_dict()
		mf_mf_quant_fdr_dict = mf_mf_quant_go_data['FDR'].to_dict()
		mf_hs_quant_fdr_dict = mf_hs_quant_go_data['FDR'].to_dict()
		mf_mf_stoch_fdr_dict = mf_mf_stoch_go_data['FDR'].to_dict()
		mf_hs_stoch_fdr_dict = mf_hs_stoch_go_data['FDR'].to_dict()
		cc_mf_quant_fdr_dict = cc_mf_quant_go_data['FDR'].to_dict()
		cc_hs_quant_fdr_dict = cc_hs_quant_go_data['FDR'].to_dict()
		cc_mf_stoch_fdr_dict = cc_mf_stoch_go_data['FDR'].to_dict()
		cc_hs_stoch_fdr_dict = cc_hs_stoch_go_data['FDR'].to_dict()

		bp_mf_quant_size_dict = bp_mf_quant_go_data['Count'].to_dict()
		bp_hs_quant_size_dict = bp_hs_quant_go_data['Count'].to_dict()
		bp_mf_stoch_size_dict = bp_mf_stoch_go_data['Count'].to_dict()
		bp_hs_stoch_size_dict = bp_hs_stoch_go_data['Count'].to_dict()
		mf_mf_quant_size_dict = mf_mf_quant_go_data['Count'].to_dict()
		mf_hs_quant_size_dict = mf_hs_quant_go_data['Count'].to_dict()
		mf_mf_stoch_size_dict = mf_mf_stoch_go_data['Count'].to_dict()
		mf_hs_stoch_size_dict = mf_hs_stoch_go_data['Count'].to_dict()
		cc_mf_quant_size_dict = cc_mf_quant_go_data['Count'].to_dict()
		cc_hs_quant_size_dict = cc_hs_quant_go_data['Count'].to_dict()
		cc_mf_stoch_size_dict = cc_mf_stoch_go_data['Count'].to_dict()
		cc_hs_stoch_size_dict = cc_hs_stoch_go_data['Count'].to_dict()

		bp_mf_quant_fe_dict = bp_mf_quant_go_data['Fold Enrichment'].to_dict()
		bp_hs_quant_fe_dict = bp_hs_quant_go_data['Fold Enrichment'].to_dict()
		bp_mf_stoch_fe_dict = bp_mf_stoch_go_data['Fold Enrichment'].to_dict()
		bp_hs_stoch_fe_dict = bp_hs_stoch_go_data['Fold Enrichment'].to_dict()
		mf_mf_quant_fe_dict = mf_mf_quant_go_data['Fold Enrichment'].to_dict()
		mf_hs_quant_fe_dict = mf_hs_quant_go_data['Fold Enrichment'].to_dict()
		mf_mf_stoch_fe_dict = mf_mf_stoch_go_data['Fold Enrichment'].to_dict()
		mf_hs_stoch_fe_dict = mf_hs_stoch_go_data['Fold Enrichment'].to_dict()
		cc_mf_quant_fe_dict = cc_mf_quant_go_data['Fold Enrichment'].to_dict()
		cc_hs_quant_fe_dict = cc_hs_quant_go_data['Fold Enrichment'].to_dict()
		cc_mf_stoch_fe_dict = cc_mf_stoch_go_data['Fold Enrichment'].to_dict()
		cc_hs_stoch_fe_dict = cc_hs_stoch_go_data['Fold Enrichment'].to_dict()

		bp_key_list = [set(bp_mf_quant_fdr_dict.keys()), set(bp_hs_quant_fdr_dict.keys()),
					   set(bp_mf_stoch_fdr_dict.keys()), set(bp_hs_stoch_fdr_dict.keys())]
		bp_key_list = set.union(*bp_key_list)
		mf_key_list = [set(mf_mf_quant_fdr_dict.keys()), set(mf_hs_quant_fdr_dict.keys()),
					   set(mf_mf_stoch_fdr_dict.keys()), set(mf_hs_stoch_fdr_dict.keys())]
		mf_key_list = set.union(*mf_key_list)
		cc_key_list = [set(cc_mf_quant_fdr_dict.keys()), set(cc_hs_quant_fdr_dict.keys()),
					   set(cc_mf_stoch_fdr_dict.keys()), set(cc_hs_stoch_fdr_dict.keys())]
		cc_key_list = set.union(*cc_key_list)

		fe_dict = dict()
		fdr_dict = dict()
		size_dict = dict()
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_fdr_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_fdr_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_fdr_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_fdr_dict.get(key,np.nan)
			fdr_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fdr_dict[key]['gender_quant'] = get_male_quant
			fdr_dict[key]['diet_quant'] = get_female_quant
			fdr_dict[key]['gender_stoch'] = get_male_stoch
			fdr_dict[key]['diet_stoch'] = get_female_stoch
		bp_fdr_data = pd.DataFrame(fdr_dict)
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_fe_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_fe_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_fe_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_fe_dict.get(key,np.nan)
			fe_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fe_dict[key]['gender_quant'] = get_male_quant
			fe_dict[key]['diet_quant'] = get_female_quant
			fe_dict[key]['gender_stoch'] = get_male_stoch
			fe_dict[key]['diet_stoch'] = get_female_stoch
		bp_fe_data = pd.DataFrame(fe_dict)
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_size_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_size_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_size_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_size_dict.get(key,np.nan)
			size_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			size_dict[key]['gender_quant'] = get_male_quant
			size_dict[key]['diet_quant'] = get_female_quant
			size_dict[key]['gender_stoch'] = get_male_stoch
			size_dict[key]['diet_stoch'] = get_female_stoch
		bp_size_data = pd.DataFrame(size_dict)

		fe_dict = dict()
		fdr_dict = dict()
		size_dict = dict()
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_fdr_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_fdr_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_fdr_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_fdr_dict.get(key,np.nan)
			fdr_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fdr_dict[key]['gender_quant'] = get_male_quant
			fdr_dict[key]['diet_quant'] = get_female_quant
			fdr_dict[key]['gender_stoch'] = get_male_stoch
			fdr_dict[key]['diet_stoch'] = get_female_stoch
		cc_fdr_data = pd.DataFrame(fdr_dict)
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_fe_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_fe_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_fe_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_fe_dict.get(key,np.nan)
			fe_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fe_dict[key]['gender_quant'] = get_male_quant
			fe_dict[key]['diet_quant'] = get_female_quant
			fe_dict[key]['gender_stoch'] = get_male_stoch
			fe_dict[key]['diet_stoch'] = get_female_stoch
		cc_fe_data = pd.DataFrame(fe_dict)
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_size_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_size_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_size_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_size_dict.get(key,np.nan)
			size_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			size_dict[key]['gender_quant'] = get_male_quant
			size_dict[key]['diet_quant'] = get_female_quant
			size_dict[key]['gender_stoch'] = get_male_stoch
			size_dict[key]['diet_stoch'] = get_female_stoch
		cc_size_data = pd.DataFrame(size_dict)

		bp_fdr_data.to_csv(folder + 'BP_go_output_fdrData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		bp_fe_data.to_csv(folder + 'BP_go_output_feData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		bp_size_data.to_csv(folder + 'BP_go_output_sizeData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_fdr_data.to_csv(folder + 'CC_go_output_fdrData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_fe_data.to_csv(folder + 'CC_go_output_feData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_size_data.to_csv(folder + 'CC_go_output_sizeData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')

	@staticmethod
	def plot_BP_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['gender_quant','diet_quant']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['gender_stoch','diet_stoch']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['gender_quant','diet_quant']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['gender_stoch','diet_stoch']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['gender_quant','diet_quant']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['gender_stoch','diet_stoch']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('gender_quant', ascending = False)
		q1 = quant_fe_data[quant_fe_data['gender_quant']>0]
		q2 = quant_fe_data[quant_fe_data['gender_quant']<=0]
		q2 = q2.sort_values('diet_quant')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('gender_stoch', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data['gender_stoch']>0]
		q2 = stoch_fe_data[stoch_fe_data['gender_stoch']<=0]
		q2 = q2.sort_values('diet_stoch')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq)
		female_my_cmap = colorFacade.make_colormap(female_seq)

		male_quant_fe_list = list(quant_fe_data['gender_quant'])
		female_quant_fe_list = list(quant_fe_data['diet_quant'])
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data['gender_quant']))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data['diet_quant']))
		male_stoch_fe_list = list(stoch_fe_data['gender_stoch'])
		female_stoch_fe_list = list(stoch_fe_data['diet_stoch'])
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data['gender_stoch']))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data['diet_stoch']))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'BP_abundances_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(folder + 'BP_stoichiometry_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)
	
	@staticmethod
	def plot_CC_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'CC_go_output_sizeData_gender_diet_difference.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['gender_quant','diet_quant']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['gender_stoch','diet_stoch']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['gender_quant','diet_quant']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['gender_stoch','diet_stoch']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['gender_quant','diet_quant']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['gender_stoch','diet_stoch']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('gender_quant', ascending = False)
		q1 = quant_fe_data[quant_fe_data['gender_quant']>0]
		q2 = quant_fe_data[quant_fe_data['gender_quant']<=0]
		q2 = q2.sort_values('diet_quant')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('gender_stoch', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data['gender_stoch']>0]
		q2 = stoch_fe_data[stoch_fe_data['gender_stoch']<=0]
		q2 = q2.sort_values('diet_stoch')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq)
		female_my_cmap = colorFacade.make_colormap(female_seq)

		male_quant_fe_list = list(quant_fe_data['gender_quant'])
		female_quant_fe_list = list(quant_fe_data['diet_quant'])
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data['gender_quant']))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data['diet_quant']))
		male_stoch_fe_list = list(stoch_fe_data['gender_stoch'])
		female_stoch_fe_list = list(stoch_fe_data['diet_stoch'])
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data['gender_stoch']))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data['diet_stoch']))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'CC_abundances_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(folder + 'CC_stoichiometry_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

	@staticmethod
	def export_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		ffolder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				   'BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  'BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  	'BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data.to_csv(folder + 'data/fig5c_BP_go_output_fdrData_gender_diet_difference.tsv.gz',
						sep = '\t', compression = 'gzip')
		fe_data.to_csv(folder + 'data/fig5c_BP_go_output_feData_gender_diet_difference.tsv.gz',
					   sep = '\t', compression = 'gzip')
		size_data.to_csv(folder + 'data/fig5c_BP_go_output_sizeData_gender_diet_difference.tsv.gz',
						 sep = '\t', compression = 'gzip')


		ffolder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				   'CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  'CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  	'CC_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data.to_csv(folder + 'data/fig5c_CC_go_output_fdrData_gender_diet_difference.tsv.gz',
						sep = '\t', compression = 'gzip')
		fe_data.to_csv(folder + 'data/fig5c_CC_go_output_feData_gender_diet_difference.tsv.gz',
					   sep = '\t', compression = 'gzip')
		size_data.to_csv(folder + 'data/fig5c_CC_go_output_sizeData_gender_diet_difference.tsv.gz',
						 sep = '\t', compression = 'gzip')

	@staticmethod
	def plot_BP_go_for_main_figure(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/fig5c_BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/fig5c_BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/fig5c_BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		size_data = size_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.dropna(thresh=1)
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		fe_data = fe_data.dropna(thresh=1)

		fe_data = fe_data.replace(np.nan, 0)
		size_data = size_data.replace(np.nan,0)

		fe_data, protList = utilsFacade.recluster_matrix_only_rows(fe_data, method = 'complete')
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.T[list(fe_data.index)].T
		sc_gender,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greens, np.arange(0,20,0.001))
		sc_diet,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys, np.arange(0,20,0.001))


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,10)

		ax = plt.subplot(gs[0:8,0:8])
		count=0
		for col in ['gender_quant','diet_quant','gender_stoch','diet_stoch']:
			values = np.array(fe_data[col])
			sizes = np.array(size_data[col])*30
			if col.startswith('gender')==True:
				colors = [sc_gender.to_rgba(item) for item in values]
			else:
				colors = [sc_diet.to_rgba(item) for item in values]
			ax.scatter(list(xrange(len(values))),[count]*len(values), s = sizes, 
				edgecolor = 'black', color = colors, alpha = 1)
			count+=1
		plt.yticks(list(xrange(len(size_data.columns))))
		ax.set_yticklabels(list(size_data.columns))
		plt.xticks(list(xrange(len(size_data))))
		ax.set_xticklabels([item.split('~')[1] for item in list(size_data.index)], rotation = 90)
		ax.set_xlim(-1, len(size_data))

		ax = plt.subplot(gs[0:8,8:9])
		ax.axis('off')
		cbar_gender = fig.colorbar(sc_gender)
		ax = plt.subplot(gs[0:8,9:])
		ax.axis('off')
		cbar_diet = fig.colorbar(sc_diet)

		ax = plt.subplot(gs[8:,8:])
		ax.axis('off')
		lh, lt = plottingFacade.get_legendHandles([0,5,10,20],30, edgecolor = 'k', color = 'white')
		ax.legend(lh,lt, loc = 'center',ncol=4, fontsize = 12)
		plt.savefig(output_folder + 'figures/fig5c_go_data.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def plot_CC_go_for_main_figure(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/fig5c_CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/fig5c_CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/fig5c_CC_go_output_sizeData_gender_diet_difference.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		size_data = size_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.dropna(thresh=1)
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		fe_data = fe_data.dropna(thresh=1)

		fe_data = fe_data.replace(np.nan, 0)
		size_data = size_data.replace(np.nan,0)

		fe_data, protList = utilsFacade.recluster_matrix_only_rows(fe_data, method = 'complete')
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.T[list(fe_data.index)].T
		sc_gender,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greens, np.arange(0,20,0.001))
		sc_diet,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys, np.arange(0,20,0.001))



		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,10)

		ax = plt.subplot(gs[0:8,0:8])
		count=0
		for col in ['gender_quant','diet_quant','gender_stoch','diet_stoch']:
			values = np.array(fe_data[col])
			sizes = np.array(size_data[col])*30
			if col.startswith('gender')==True:
				colors = [sc_gender.to_rgba(item) for item in values]
			else:
				colors = [sc_diet.to_rgba(item) for item in values]
			ax.scatter(list(xrange(len(values))),[count]*len(values), s = sizes, 
				edgecolor = 'black', color = colors, alpha = 1)
			count+=1
		plt.yticks(list(xrange(len(size_data.columns))))
		ax.set_yticklabels(list(size_data.columns))
		plt.xticks(list(xrange(len(size_data))))
		ax.set_xticklabels([item.split('~')[1] for item in list(size_data.index)], rotation = 90)
		ax.set_xlim(-1, len(size_data))

		ax = plt.subplot(gs[0:8,8:9])
		ax.axis('off')
		cbar_gender = fig.colorbar(sc_gender)
		ax = plt.subplot(gs[0:8,9:])
		ax.axis('off')
		cbar_diet = fig.colorbar(sc_diet)

		ax = plt.subplot(gs[8:,8:])
		ax.axis('off')
		lh, lt = plottingFacade.get_legendHandles([0,5,10,20],30, edgecolor = 'k', color = 'white')
		ax.legend(lh,lt, loc = 'center',ncol=4, fontsize = 12)
		plt.savefig(output_folder + 'figures/fig5c_CC_supp_go_data.pdf',
					bbox_inches = 'tight', dpi = 400)