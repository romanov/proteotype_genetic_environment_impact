class figure3:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		recluster_unbiasedly = kwargs.get("recluster_unbiasedly","")
		name = kwargs.get("name","protein_main")
		cmap = kwargs.get("cmap",plt.cm.RdBu)
		cmap_colorbar = kwargs.get("cmap_r",plt.cm.RdBu_r)

		print('FIGURE3: main_figure3_landscape')
		figure3.main_figure3_landscape(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure3_landscape(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		recluster_unbiasedly = kwargs.get("recluster_unbiasedly","")
		name = kwargs.get("name","protein_main")
		cmap = kwargs.get("cmap",plt.cm.RdBu)
		cmap_colorbar = kwargs.get("cmap_r",plt.cm.RdBu_r)

		'''
		print("get_gold_complexes")
		gold_complexes = figure3.get_gold_complexes()
		print("load_data")
		data_dict = figure3.load_data(gold_complexes)
		print("get_complex_info_dict")
		complex_info_dict = figure3.get_complex_info_dict(data_dict, gold_complexes)
		'''
		print('get_figure_data')
		complex_info_dict = figure3.get_figure_data()

		if name=="protein_main":
			print("prepare_df")
			df, dfList, real_df = figure3.prepare_df_protein(complex_info_dict, recluster_unbiasedly)
		elif name=="rna_supp":
			print("prepare_df")
			df, dfList = figure3.prepare_df_rna(complex_info_dict)

		print("plot_landscape")
		figure3.plot_landscape(df, dfList, real_df, name, cmap, output_folder)

	@staticmethod
	def get_median_correlations(dat, complexID):
		sub = dat[dat.ComplexName==complexID]
		quantCols = figure3.get_quantCols(sub)
		if complexID=="26S Proteasome":
			if "C2" in list(sub.index):
				sub=sub.drop(["C2"],0)
			if "C9" in list(sub.index):
				sub=sub.drop(["C9"],0)
		med_corr = -2
		if len(sub)>=5:
			corrData = sub[quantCols].T.corr()
			corrValues = utilsFacade.get_correlation_values(corrData)
			med_corr = np.median(utilsFacade.finite(corrValues))
		return med_corr,len(sub)

	@staticmethod
	def get_quantCols(dataset):
		quantCols = list()
		for col in list(dataset.columns):
			if col.startswith("quant_")==True and col!="quant_GM18871":
				quantCols.append(col)
		if len(quantCols) == 0:
			for col in list(dataset.columns):
				if col.find("Ratio")!=-1:
					quantCols.append(col)
		return quantCols

	@staticmethod
	def get_figure_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_info_dict = DataFrameAnalyzer.read_pickle(folder + 'data/fig3_complex_info_dictionary.pkl')
		return complex_info_dict

	@staticmethod
	def get_complex_info_dict(data_dict, gold_complexes, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		complex_info_dict = dict()

		for complexID in gold_complexes:
			med_corr_tcgaBreast,length_tcgaBreast = figure3.get_median_correlations(data_dict['tcga_breast'], complexID)
			med_corr_tcgaOvarian,length_tcgaOvarian = figure3.get_median_correlations(data_dict['tcga_ovarian'], complexID)
			med_corr_mann,length_mann = figure3.get_median_correlations(data_dict['mann'], complexID)
			med_corr_gygi3,length_gygi3 = figure3.get_median_correlations(data_dict['gygi3'], complexID)
			med_corr_gygi1,length_gygi1 = figure3.get_median_correlations(data_dict['gygi1'], complexID)
			med_corr_bp,length_bp = figure3.get_median_correlations(data_dict['battle_protein'], complexID)
			med_corr_gygi2,length_gygi2 = figure3.get_median_correlations(data_dict['gygi2'], complexID)
			med_corr_battle_rna,length_battle_rna = figure3.get_median_correlations(data_dict['battle_rna'], complexID)
			med_corr_battle_ribo,length_battle_ribo = figure3.get_median_correlations(data_dict['battle_ribo'], complexID)
			med_corr_tcgaColo,length_tcgaColo = figure3.get_median_correlations(data_dict['tcga_colo'], complexID)
			med_corrs = [med_corr_gygi3 ,med_corr_gygi1, med_corr_bp,
						 med_corr_tcgaBreast, med_corr_tcgaOvarian,
						 med_corr_mann, med_corr_tcgaColo]
			filtered_meds = filter(lambda a:a>-2, med_corrs)

			if len(filtered_meds)>=4:
				complex_name = complexID
				complex_info_dict.setdefault(complex_name,[]).append({
															  "bp": (med_corr_bp,length_bp),
															  "gygi1": (med_corr_gygi1,length_gygi1),
															  "gygi3": (med_corr_gygi3,length_gygi3),
															  "gygi2": (med_corr_gygi2,length_gygi2),
															  "brna": (med_corr_battle_rna,length_battle_rna),
															  "bribo": (med_corr_battle_ribo,length_battle_ribo),
															  "tcga_breast": (med_corr_tcgaBreast,length_tcgaBreast),
															  "tcga_ovarian": (med_corr_tcgaOvarian,length_tcgaOvarian),
															  "mann": (med_corr_mann,length_mann),
															  'tcga_colo':(med_corr_tcgaColo, length_tcgaColo)})
		DataFrameAnalyzer.to_pickle(complex_info_dict, folder + 'data/fig3_complex_info_dictionary.pkl')
		return complex_info_dict

	@staticmethod
	def prepare_df_protein(complex_info_dict, recluster_unbiasedly):
		gygi3_list = list()
		gygi1_list = list()
		gygi2_list = list()

		bp_list = list()
		brna_list = list()
		bribo_list = list()

		mann_list = list()
		tcga_breast_list = list()
		tcga_ovarian_list = list()
		tcga_colo_list = list()

		labelList = list()
		for complexID in complex_info_dict:
			gygi3_list.append(complex_info_dict[complexID][0]["gygi3"][0])
			gygi1_list.append(complex_info_dict[complexID][0]["gygi1"][0])
			#gygi2_list.append(complex_info_dict[complexID][0]["gygi2"][0])
			bp_list.append(complex_info_dict[complexID][0]["bp"][0])
			mann_list.append(complex_info_dict[complexID][0]["mann"][0])
			tcga_breast_list.append(complex_info_dict[complexID][0]["tcga_breast"][0])
			tcga_ovarian_list.append(complex_info_dict[complexID][0]["tcga_ovarian"][0])
			tcga_colo_list.append(complex_info_dict[complexID][0]["tcga_colo"][0])
			#brna_list.append(complex_info_dict[complexID][0]["brna"][0])
			#bribo_list.append(complex_info_dict[complexID][0]["bribo"][0])
			labelList.append(complexID.split("(")[0])

		df = pd.DataFrame([gygi3_list,gygi1_list,bp_list])#,gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi3","gygi1","battle-protein"]#,"gygi2","battle-ribo","battle-rna"]

		df = pd.DataFrame([gygi3_list,gygi1_list,
						   bp_list, mann_list,
						   tcga_breast_list,tcga_ovarian_list,
						   tcga_colo_list])#,gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi3","gygi1","battle-protein",
					"mann","tcga_breast","tcga_ovarian",
					'tcga_colo']#,"gygi2","battle-ribo","battle-rna"]

		if recluster_unbiasedly == True:
			df = utilsFacade.recluster_matrix(df)
		else:
			medians = [np.median(df[colname]) for colname in df.columns]
			colnames = list(df.columns)
			medians,colnames = zip(*sorted(zip(medians,colnames),reverse=True))
			df = df[list(colnames)]
		labelList = df.index
		columnList = df.columns
		dfList = map(list,df.values)

		subunit_length_list_gygi3 = dict()
		subunit_length_list_gygi1 = dict()
		subunit_length_list_bp = dict()
		subunit_length_list_mann = dict()
		subunit_length_list_tcgaBreast = dict()
		subunit_length_list_tcgaOvarian = dict()
		subunit_length_list_tcgaColo = dict()
		for complexID in complex_info_dict.keys():
			key = complexID.split("(")[0]
			subunit_length_list_gygi3.setdefault(key,[]).append(complex_info_dict[complexID][0]["gygi3"][1])
			subunit_length_list_gygi1.setdefault(key,[]).append(complex_info_dict[complexID][0]["gygi1"][1])
			subunit_length_list_bp.setdefault(key,[]).append(complex_info_dict[complexID][0]["bp"][1])
			subunit_length_list_mann.setdefault(key,[]).append(complex_info_dict[complexID][0]["mann"][1])
			subunit_length_list_tcgaBreast.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_breast"][1])
			subunit_length_list_tcgaOvarian.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_ovarian"][1])
			subunit_length_list_tcgaColo.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_colo"][1])
		
		su_length_list_gygi3 = list()
		su_length_list_gygi1 = list()
		su_length_list_bp = list()
		su_length_list_mann = list()
		su_length_list_tcgaBreast = list()
		su_length_list_tcgaOvarian = list()
		su_length_list_tcgaColo = list()
		for complexID in columnList:
			su_length_list_bp.append(subunit_length_list_bp[complexID][0])
			su_length_list_gygi1.append(subunit_length_list_gygi1[complexID][0])
			su_length_list_gygi3.append(subunit_length_list_gygi3[complexID][0])
			su_length_list_mann.append(subunit_length_list_mann[complexID][0])
			su_length_list_tcgaBreast.append(subunit_length_list_tcgaBreast[complexID][0])
			su_length_list_tcgaOvarian.append(subunit_length_list_tcgaOvarian[complexID][0])
			su_length_list_tcgaColo.append(subunit_length_list_tcgaColo[complexID][0])

		df_lst = list()
		for df in dfList:
			rank_list = rankdata(df)
			minimum_rank = rank_list.min() 
			rank_list = [np.nan if item==minimum_rank else item for item in rank_list]
			df_lst.append(rank_list)
		df = pd.DataFrame(df_lst)
		df.index = labelList
		df.columns = columnList

		real_df = pd.DataFrame(dfList)
		real_df.index = labelList
		real_df.columns = columnList

		myArray = np.array(df)
		normalizedArray = []

		for row in range(0, len(myArray)):
			list_values = []
			Min =  min(utilsFacade.finite(list(myArray[row])))
			Max = max(utilsFacade.finite(list(myArray[row])))
			mean = np.mean(utilsFacade.finite(list(myArray[row])))
			std = np.std(utilsFacade.finite(list(myArray[row])))

			for element in myArray[row]:
				list_values.append((element - mean)/std)
			normalizedArray.append(list_values)

		newArray = []
		for row in range(0, len(normalizedArray)):
			list_values = normalizedArray[row]
			newArray.append(list_values)
		new_df = pd.DataFrame(newArray)
		new_df.columns = list(df.columns)
		new_df.index = df.index
		df = new_df.copy()
		dfList=map(list,df.values)

		"""
		df=self.recluster_matrix(df)
		"""
		return df,dfList, real_df

	@staticmethod
	def prepare_df_rna(complex_info_dict):
		gygi2_list = list()
		brna_list = list()
		bribo_list = list()
		labelList = list()
		for complexID in complex_info_dict:
			gygi2_list.append(complex_info_dict[complexID][0]["gygi2"][0])
			brna_list.append(complex_info_dict[complexID][0]["brna"][0])
			bribo_list.append(complex_info_dict[complexID][0]["bribo"][0])
			labelList.append(complexID.split("(")[0])
		df = pd.DataFrame([gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi2","battle-ribo","battle-rna"]
		df = utilsFacade.recluster_matrix(df)
		labelList = df.index
		columnList = df.columns
		dfList = map(list,df.values)

		df_lst = list()
		for df in dfList:
			df_lst.append(rankdata(df))
		df = pd.DataFrame(df_lst)
		df.index = labelList
		df.columns = columnList
		df = utilsFacade.recluster_matrix(df)
		return df,dfList

	@staticmethod
	def get_scalarmap(cmap, dfList):
		scalarmap1,colorList1 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[0])))
		scalarmap2,colorList2 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[1])))
		scalarmap3,colorList3 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[2])))
		scalarmap4,colorList4 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[3])))
		scalarmap5,colorList5 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[4])))
		scalarmap6,colorList6 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[5])))
		scalarmap7,colorList7 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[6])))
		all_values = utilsFacade.flatten(dfList)
		scalarmap,colorList = colorFacade.get_specific_color_gradient(cmap,
							  np.array(utilsFacade.finite(all_values)), vmin = -2, vmax = 2)
		scalarmap_dict = {'1':scalarmap1,'2':scalarmap2, '3':scalarmap3,
						  '4': scalarmap4, '5': scalarmap5, '6':scalarmap6,'7': scalarmap7,
						  'all':scalarmap}
		return scalarmap_dict,all_values

	@staticmethod
	def plot_landscape(df, dfList, real_df, name, cmap, output_folder, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_list = list(real_df.columns)
		real_df = real_df.replace(-2, np.nan)
		corr_median_list = list(real_df.median())
		corr_median_list, complex_list = zip(*sorted(zip(corr_median_list, complex_list), reverse = True))
		df = df[list(complex_list)]
		scalarmap_dict, all_values = figure3.get_scalarmap(cmap, dfList)


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig=plt.figure(figsize=(10,4))
		gs = gridspec.GridSpec(10,10)
		ax = plt.subplot(gs[0:3,0:])
		ind = np.arange(len(corr_median_list))
		width = 0.85
		sc, color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys_r,
						 np.array(list(xrange(len(corr_median_list)))))
		rects_all = ax.bar(ind, corr_median_list, width, color=color_list, edgecolor = 'white')		

		ax.set_xlim(0,len(corr_median_list)+0.5)
		ax.set_ylim(min(corr_median_list), max(corr_median_list))
		perc25, perc75 = np.percentile(corr_median_list,[25,75])
		ax.set_xticklabels([])
		ax.axhline(perc25, color = 'red', linewidth = 0.5, linestyle = '--')
		ax.axhline(perc75, color = 'red', linewidth = 0.5, linestyle = '--')

		ax = plt.subplot(gs[3:8,0:])
		plt.rcParams["axes.grid"] = False
		dfList = map(list,df.values)
		count = 0
		for item in dfList:
			colors = list()
			for i in item:
				if str(i) == 'nan':
					color = np.array(colorFacade.hex_to_rgb('#808080'))/256.0
					color = list(color)
					color.append(1)
					colors.append(color)
				else:
					'''
					if count==0:
						colors.append(scalarmap_dict['1'].to_rgba(i))
					elif count==1:
						colors.append(scalarmap_dict['2'].to_rgba(i))
					elif count==2:
						colors.append(scalarmap_dict['3'].to_rgba(i))
					elif count==3:
						colors.append(scalarmap_dict['4'].to_rgba(i))
					elif count==4:
						colors.append(scalarmap_dict['5'].to_rgba(i))
					elif count==5:
						colors.append(scalarmap_dict['6'].to_rgba(i))
					elif count==6:
						colors.append(scalarmap_dict['7'].to_rgba(i))
					'''
					colors.append(scalarmap_dict['all'].to_rgba(i))
			ax.scatter(xrange(len(item)),[count]*len(item),
					   color=colors,s=200,marker="s",edgecolor="white")
			ax.scatter(len(item),[count],color="white",
					   s=200,marker="s",edgecolor="white")
			count+=1
		plt.yticks(list(xrange(len(dfList))))
		if name=="protein_main":
			ylabelList = ["DO mouse strains(P)", "Founder mouse strains(P)",
						  "Human Individuals(P)", "Human cell types(P)",
						  "TCGA Breast Cancer (P)", "TCGA Ovarian Cancer(P)",
						  'TCGA Colorectal Cancer(P)']
		else:
			ylabelList = ["DO mouse strains(RS)","Human Individuals(RP)","Human Individuals(RS)"]
		ax.set_yticklabels(list(ylabelList))
		plt.xticks(list(xrange(len(df.columns))))
		ax.set_xticklabels(list(df.columns), rotation=45, fontsize=8, ha="right")
		ax.set_xlim(-1,len(df.columns)-0.5)
		plt.savefig(output_folder + "figures/fig3_landscape_variability_complexes_" + name + ".pdf",
					bbox_inches="tight", dpi=400)


		all_values = utilsFacade.flatten(dfList)
		scalarmap,colorList = colorFacade.get_specific_color_gradient(cmap,
							  np.array(utilsFacade.finite(all_values)), vmin = -2, vmax = 2)

		plt.rcParams["axes.grid"] = True
		plt.clf()
		fig = plt.figure(figsize=(10,1))
		ax = fig.add_subplot(111)
		ax.set_xticklabels([])
		ax.set_yticklabels([])
		cbar = fig.colorbar(scalarmap,orientation="horizontal")
		plt.savefig(output_folder + "figures/fig3_landscape_variability_complexes_" + name + "_LEGEND.pdf",
					bbox_inches="tight", dpi=400)
