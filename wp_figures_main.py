import sys
sys.path.append('/g/bork1/romanov/scripts/')
from wp_import_modules import *
from utils import *

import sklearn
import matplotlib as mpl
mpl.rcParams["axes.grid"]=True;mpl.rcParams["axes.facecolor"]="white"
mpl.rcParams["grid.color"]="#E7EBEF";mpl.rcParams["grid.linestyle"]="solid";mpl.rcParams["grid.alpha"]=0.7
plt.rcParams['svg.fonttype'] = 'none'

class figure1:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE1:main_figure1_elements')
		figure1.main_figure1_elements(folder = folder, output_folder = output_folder)
		print('FIGURE1:main_figure1_rocCurves')
		figure1.main_figure1_rocCurves(folder = folder, output_folder = output_folder)		

	@staticmethod
	def main_figure1_elements(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('get_data')
		data = figure1.get_data(folder = folder)
		print('sub_figure_abundanceProfiles')
		figure1.sub_figure_abundanceProfiles(data, output_folder = output_folder)
		print('sub_figure_coexpressionMatrix')
		figure1.sub_figure_coexpressionMatrix(data, output_folder = output_folder)

	@staticmethod
	def main_figure1_rocCurves(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		string_fileFolder = folder + 'data/string_roc_data/'
		pat_fileFolder = folder + 'data/pathway_roc_data/'
		com_fileFolder = folder + 'data/complex_roc_data/'
		loc_fileFolder = folder + 'data/location_roc_data/'
		folders = [string_fileFolder,pat_fileFolder,com_fileFolder,loc_fileFolder,output_folder]

		print("return_areaDict")
		areaDict1, xList1 = figure1.return_areaDict_track1(folders)
		areaDict2, xList2 = figure1.return_areaDict_track2(string_fileFolder)
		print("sub_figure_rocCurve")
		figure1.sub_figure_rocCurve_track1(areaDict1, output_folder)
		figure1.sub_figure_rocCurve_track2(areaDict2, output_folder)

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_protein.tsv.gz')
		return data

	@staticmethod
	def sub_figure_coexpressionMatrix(data, output_folder):
		quantCols = utilsFacade.get_quantCols(data)

		sub = data[data.ComplexID.isin(["HC2308","Nucleopore Complex","HC2224","HC2191"])]
		quantData = sub[quantCols][0:600].dropna()

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize=(5,5))
		ax = fig.add_subplot(111)
		altProteinList = list(quantData.index)
		plottingFacade.func_plotCorrelationMatrix(ax, quantData, altProteinList)
		plt.savefig(output_folder + "figures/fig1a_coexpression_matrix.pdf",
					bbox_inches="tight",dpi=600)

	@staticmethod
	def sub_figure_abundanceProfiles(data, output_folder):
		quantCols = utilsFacade.get_quantCols(data)
		quantData = data[quantCols]
		quantData = quantData.dropna()

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.rcParams["axes.grid"] = True
		plt.clf()
		fig = plt.figure(figsize=(5,5))
		ax = fig.add_subplot(311)
		ax.plot(xrange(len(quantData.iloc[0])),list(quantData.iloc[0]),color="orange",linewidth=2)
		ax.set_yticklabels([])
		ax.set_xticklabels([])
		ax = fig.add_subplot(312)
		ax.plot(xrange(len(quantData.iloc[100])),list(quantData.iloc[100]),color="blue",linewidth=2)
		ax.set_ylabel("expression/abundance",fontsize=20)
		#ax.set_xlabel("individuals",fontsize=20)
		ax.set_yticklabels([])
		ax.set_xticklabels([])
		ax = fig.add_subplot(313)
		ax.plot(xrange(len(quantData.iloc[200])),list(quantData.iloc[200]),color="green",linewidth=2)
		ax.set_yticklabels([])
		ax.set_xticklabels([])
		ax.set_xlabel("individuals",fontsize=20)
		plt.savefig(output_folder + "figures/fig1a_protein_profile_examples.pdf",
					bbox_inches="tight",dpi=600)

	@staticmethod
	def return_areaDict_track1(folders):
		keyname = "battle_protein"
		category = "combined_score"

		string_fileFolder,pat_fileFolder,com_fileFolder,loc_fileFolder,output_folder = folders
		areaDict = dict()
		areaDict.setdefault(keyname,{})
		areaDict[keyname].setdefault("STRING",{})

		fileName = "string_rocCurves_inputData_" + keyname + "_" + category + "_complexExcluded.txt"
		fileData = DataFrameAnalyzer.getFile(string_fileFolder, fileName, sep="\t")
		areaList = list()
		thresholdList = map(float,list(fileData["threshold"].iloc[0].split(",")))

		for score in list(xrange(100,1000,100)):
			area = list(fileData[fileData["stringScore"]==score]["area"])[0]
			areaList.append(area)
			tp = map(float,list(fileData[fileData["stringScore"]==score]["TP"])[0].split(","))
			fp = map(float,list(fileData[fileData["stringScore"]==score]["FP"])[0].split(","))
			areaDict[keyname]["STRING"].setdefault(score,[]).append((area,tp,fp))

		areaDict = figure1.get_compartment(areaDict, loc_fileFolder)
		areaDict = figure1.get_pathways(areaDict, pat_fileFolder)
		areaDict = figure1.get_complexes(areaDict, com_fileFolder)
		return areaDict,thresholdList

	@staticmethod
	def return_areaDict_track2(string_fileFolder):
		keyname = "battle_protein"
		category = "combined_score"

		keynames = ["battle_protein","battle_ribo","battle_rna","gygi1","gygi2"]
		areaDict = dict((e1,dict()) for e1 in keynames)
		for keyname in keynames:
			print(keyname)
			areaDict[keyname].setdefault("STRING",{})
			fileName = "string_rocCurves_inputData_" + keyname + "_" + category + "_complexExcluded.txt"
			fileData = DataFrameAnalyzer.getFile(string_fileFolder, fileName, sep="\t")
			areaList = list()
			thresholdList = map(float,list(fileData["threshold"].iloc[0].split(",")))

			for score in list(xrange(100,1000,100)):
				area = list(fileData[fileData["stringScore"]==score]["area"])[0]
				areaList.append(area)
				tp = map(float,list(fileData[fileData["stringScore"]==score]["TP"])[0].split(","))
				fp = map(float,list(fileData[fileData["stringScore"]==score]["FP"])[0].split(","))
				areaDict[keyname]["STRING"].setdefault(score,[]).append((area,tp,fp))
		return areaDict,thresholdList

	@staticmethod
	def get_pathways(areaDict, pat_fileFolder):
		keyname = "battle_protein"
		fileName = "pathway_rocCurves_inputData_" + keyname + "_diffMethod.txt"
		fileData = DataFrameAnalyzer.getFile(pat_fileFolder, fileName, sep="\t")
		area = list(fileData["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("pathway",[]).append((area,tp,fp))
		return areaDict

	@staticmethod
	def get_complexes(areaDict, com_fileFolder):
		keyname = "battle_protein"
		
		fileName = "complex_rocCurves_inputData_" + keyname + "_3members.txt"
		fileData = DataFrameAnalyzer.getFile(com_fileFolder, fileName, sep="\t")
		area = list(fileData["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("complex_3members",[]).append((area,tp,fp))
		
		fileName = "complex_rocCurves_inputData_" + keyname + "_5members.txt"
		fileData = DataFrameAnalyzer.getFile(com_fileFolder,fileName,sep="\t")
		area = list(fileData["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("complex_5members",[]).append((area,tp,fp))
		return areaDict

	@staticmethod
	def get_compartment(areaDict, loc_fileFolder):
		keyname = "battle_protein"
		fileName = "compartment_rocCurves_inputData_" + keyname + ".txt"
		fileData = DataFrameAnalyzer.getFile(loc_fileFolder, fileName, sep="\t")

		data = fileData[fileData["dataset_name"]=="localization"]
		area = list(data["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("compartment",[]).append((area,tp,fp))

		data = fileData[fileData["dataset_name"]=="Nucleus"]
		area = list(data["FP"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("nucleus",[]).append((area,tp,fp))

		fileName = "mitocarta_rocCurves_inputData_"+keyname+".txt"
		fileData = DataFrameAnalyzer.getFile(loc_fileFolder,fileName,sep="\t")
		data = fileData[fileData["category"]=="mitoDomain"]
		area = list(data["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("mitocarta_mitoDomain",[]).append((area,tp,fp))

		data = fileData[fileData["category"]=="sharedDomain"]
		area = list(data["area"])[0]
		tp = map(float,list(fileData["TP"])[0].split(","))
		fp = map(float,list(fileData["FP"])[0].split(","))
		areaDict[keyname].setdefault("mitocarta_sharedDomain",[]).append((area,tp,fp))
		return areaDict

	@staticmethod
	def sub_figure_rocCurve_track1(areaDict, output_folder):
		area_complex,tp_complex,fp_complex = areaDict["battle_protein"]["complex_5members"][0]
		area_pat,tp_pat,fp_pat = areaDict["battle_protein"]["pathway"][0]
		area_compa,tp_compa,fp_compa = areaDict["battle_protein"]["compartment"][0]
		area_mito,tp_mito,fp_mito = areaDict["battle_protein"]["mitocarta_mitoDomain"][0]
		area_string500,tp_string500,fp_string500 = areaDict["battle_protein"]["STRING"][500][0]
		area_string600,tp_string600,fp_string600 = areaDict["battle_protein"]["STRING"][600][0]
		area_string700,tp_string700,fp_string700 = areaDict["battle_protein"]["STRING"][700][0]
		area_string800,tp_string800,fp_string800 = areaDict["battle_protein"]["STRING"][800][0]
		area_string900,tp_string900,fp_string900 = areaDict["battle_protein"]["STRING"][900][0]

		cm = plt.get_cmap('ocean')
		cNorm = mpl.colors.Normalize(vmin=0, vmax=4)
		scalarMap = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)
		scalarMap.set_array(list(xrange(9)))
		colorList1 = scalarMap.to_rgba(list(xrange(9)))
		colorList1 = ["red","green","blue","darkblue"]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(5,5))
		ax = fig.add_subplot(111)
		ax.plot(list(utilsFacade.frange(0,1,0.1)),list(utilsFacade.frange(0,1,0.1)),
				color="grey", alpha=0.85, linewidth=2, linestyle="--")
		ax.plot(fp_complex,tp_complex, color=colorList1[0], alpha=0.85, linewidth=2)
		ax.plot(fp_pat,tp_pat, color=colorList1[1], alpha=0.85, linewidth=2)
		ax.plot(fp_compa,tp_compa, color=colorList1[2], alpha=0.85, linewidth=2)
		ax.plot(fp_string700,tp_string700, color=colorList1[3], alpha=0.85, linewidth=2)
		plottingFacade.make_full_legend(ax, ["complexes","pathways","compartments","STRING interactions"],
										colorList1, fontsize=12)
		ax.set_xlabel("False Positive Rate", fontsize=13)
		ax.set_ylabel("True Positive Rate", fontsize=13)
		plt.tick_params(axis="x",which="both",bottom="off",top="off",labelsize=12)
		plt.tick_params(axis="y",which="both",left="off",right="off",labelsize=12)
		ax.set_title("ROC curves for collecting AUC-values",fontsize=14)
		plt.savefig(output_folder + "figures/fig1a_roc_curve_examples_TRACK1.pdf",
					bbox_inches="tight", dpi=600)

	@staticmethod
	def sub_figure_rocCurve_track2(areaDict, output_folder):
		bp_area_string700, bp_tp_string700, bp_fp_string700 = areaDict["battle_protein"]["STRING"][700][0]
		bribo_area_string700, bribo_tp_string700, bribo_fp_string700 = areaDict["battle_ribo"]["STRING"][700][0]
		brna_area_string700, brna_tp_string700, brna_fp_string700 = areaDict["battle_rna"]["STRING"][700][0]
		gygi1_area_string700, gygi1_tp_string700, gygi1_fp_string700 = areaDict["gygi1"]["STRING"][700][0]
		gygi2_area_string700, gygi2_tp_string700, gygi2_fp_string700 = areaDict["gygi2"]["STRING"][700][0]

		cm = plt.get_cmap('ocean')
		cNorm = mpl.colors.Normalize(vmin=0, vmax=4)
		scalarMap = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)
		scalarMap.set_array(list(xrange(9)))
		colorList1 = scalarMap.to_rgba(list(xrange(9)))
		colorList1 = ["red","green","blue","darkblue","darkgreen","darkred"]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(5,5))
		ax = fig.add_subplot(111)
		ax.plot(list(utilsFacade.frange(0,1,0.1)),list(utilsFacade.frange(0,1,0.1)),
					 color="grey", alpha=0.85, linewidth=2, linestyle="--")

		ax.plot(bp_fp_string700,bp_tp_string700, color=colorList1[0], alpha=0.85, linewidth=2)
		ax.plot(bribo_fp_string700,bribo_tp_string700, color=colorList1[1], alpha=0.85, linewidth=2)
		ax.plot(brna_fp_string700,brna_tp_string700, color=colorList1[2], alpha=0.85, linewidth=2)
		ax.plot(gygi1_fp_string700,gygi1_tp_string700, color=colorList1[3], alpha=0.85, linewidth=2)
		ax.plot(gygi2_fp_string700,gygi2_tp_string700, color=colorList1[4], alpha=0.85, linewidth=2)

		plottingFacade.make_full_legend(ax, ["Dataset1","Dataset2","Dataset3","Dataset4","Dataset5"],
										colorList1, fontsize=12)
		ax.set_xlabel("False Positive Rate", fontsize=13)
		ax.set_ylabel("True Positive Rate", fontsize=13)
		plt.tick_params(axis="x",which="both",bottom="off",top="off",labelsize=12)
		plt.tick_params(axis="y",which="both",left="off",right="off",labelsize=12)
		ax.set_title("ROC curves for collecting AUC-values",fontsize=14)
		plt.savefig(output_folder + "figures/fig1a_roc_curve_examples_TRACK2.pdf",
					bbox_inches="tight",dpi=600)

class figure2a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE2A: main_figure2a_rocMatrix')
		figure2a.main_figure2a_rocMatrix(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure2a_rocMatrix(**kwargs):	
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('get_auc_matrix')
		auc_matrix = figure2a.get_auc_matrix(folder = folder)
		print("plot_auc_matrix")
		figure2a.plot_auc_matrix(auc_matrix, output_folder = output_folder)
		print('export_underlying_data')
		figure2a.export_underlying_data(auc_matrix, folder = folder, output_folder = output_folder)

	@staticmethod
	def get_auc_matrix(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		auc_matrix = DataFrameAnalyzer.open_in_chunks(folder, 'data/fig2a_auc_matrix.tsv.gz', sep = '\t')
		return auc_matrix

	@staticmethod
	def get_specific_color_gradient(colormap,inputList, **kwargs):
		vmin = kwargs.get("vmin", False)
		vmax = kwargs.get("vmax", False)
		cm = plt.get_cmap(colormap)
		if type(inputList)==list:
			if vmin == False and vmax == False:
				cNorm = mpl.colors.Normalize(vmin=min(inputList), vmax=max(inputList))
			else:
				cNorm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
		else:
			if vmin == False and vmax == False:
				cNorm = mpl.colors.Normalize(vmin=inputList.min(), vmax=inputList.max())
			else:
				cNorm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
		scalarMap = mpl.cm.ScalarMappable(norm=cNorm, cmap=cm)
		scalarMap.set_array(inputList)
		colorList = scalarMap.to_rgba(inputList)
		return scalarMap,colorList

	@staticmethod
	def plot_auc_matrix(auc_df, output_folder):
		datasets = ["tcga_ovarian","battle_protein",'colo_cancer',"gygi3",'tcga_breast',"gygi1",
					"mann_all_log2","primatePRO","wu","battle_ribo","battle_rna",
					"gygi2",'bxd_protein',"primateRNA","tiannan"]
		categories = ["chromosome","housekeeping","essential","pathway",
					  "compartment","string_700","complex"]
		data = auc_df.T
		data = data[categories]
		data = data.T[datasets].T

		data.index = ["TCGA Ovarian Cancer(P)",'Human Individuals(Battle,P)',
					  'TCGA Colorectal Cancer(P)','DO mouse strains(P)',"TCGA Breast Cancer(P)",
					  'Founder mouse strains (P)',
					  'Human cell lines(P)','Primate cells(P)','Human Individuals(Wu,P)',
					  'Human Individuals(RP)','Human Individuals(RS)',
					  'DO mouse strains(RS)','BXD80 mouse strains(P)',
					  'Primate cells(RS)','Kidney cancer cells(P)']
		data.columns = ['chromosome','housekeeping','essential',
						'pathway','compartment','STRING','complex']


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		x_mean_list = list()
		for col in data.columns:
			x_mean_list.append(np.mean(utilsFacade.finite(list(data[col])))-0.5)
		y_mean_list = list()
		for col in data.T.columns:
			y_mean_list.append(max(utilsFacade.finite(list(data.T[col])))-0.5)

		plt.clf()
		fig = plt.figure(figsize = (5,8))
		gs = gridspec.GridSpec(16,11)
		ax1_density = plt.subplot(gs[0:2,0:8])
		ax1_density.set_ylim(0.5,0.8)
		ax1_density.axhline(0.55, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.6, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.65, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.7, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax1_density.axhline(0.75, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		scalarmap_x, colorList_x = figure2a.get_specific_color_gradient(plt.cm.Greys,
															   			np.array(xrange(len(x_mean_list))))
		ax1_density.bar(np.arange(len(x_mean_list)), 
			x_mean_list, 0.95, color = colorList_x, bottom = 0.5,
			edgecolor = "white", linewidth = 2, zorder = 3)
		plt.xticks(list(xrange(len(data.columns))))
		ax1_density.set_xticklabels([])
		ax1_density.set_xlim(0,len(data.columns))
		ax = plt.subplot(gs[2:10,0:8])
		scalarmap, colorList = figure2a.get_specific_color_gradient(plt.cm.RdBu,
														   			np.array(data), vmin = 0.4, vmax = 0.7)
		sns.heatmap(data, cmap = plt.cm.RdBu, vmin = 0.4,vmax = 0.7,
			linecolor = "white", linewidth = 2, cbar = False)
		y_mean_list = y_mean_list[::-1]
		ax2_density = plt.subplot(gs[2:10,8:10])
		plt.yticks(list(xrange(len(data.index))))
		ax2_density.set_ylim(0,len(data.index))
		ax2_density.set_xlim(0.5,0.85)
		scalarmap_y, colorList_y = figure2a.get_specific_color_gradient(plt.cm.Greys,
															   			np.array(xrange(len(y_mean_list))))
		plt.setp(ax2_density.get_xticklabels(), rotation = 90)
		ax2_density.set_yticklabels([])
		ax2_density.axvline(0.7, color = "red", linestyle = "--", linewidth = 0.5)
		ax2_density.axvline(0.55, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.6, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.65, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.75, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.axvline(0.8, alpha = 0.6, color="grey", linestyle='--', linewidth = 0.2, zorder=1)
		ax2_density.barh(np.arange(len(y_mean_list)), y_mean_list,
			0.95, color = colorList_y, left = 0.5,
			edgecolor = "white", linewidth = 2, zorder = 3)
		ax_category = plt.subplot(gs[2:10,10:11])
		df = pd.DataFrame({"color":["green"]*7+["lightgreen"]+["magenta"]*2+["green","magenta","green"]})
		df = pd.DataFrame({'color':14*[1]})
		sns.heatmap(df, cbar = False, linewidth = 2, linecolor = "white")
		#ax_category.scatter([1]*len(df),xrange(len(df)), color = df["color"], marker = "s", s=200)
		ax_category.axis("off")
		ax_cbar = plt.subplot(gs[13:14,0:8])
		cbar = fig.colorbar(scalarmap, cax = ax_cbar, 
			   orientation = "horizontal")
		cbar.set_label("Area under curve (AUC)")
		plt.savefig(output_folder + "figures/fig2a_auc_matrix.pdf",
					bbox_inches = "tight", dpi=600)

	@staticmethod
	def export_underlying_data(auc_matrix, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/data/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		folder = "/g/scb2/bork/romanov/wpc/data/figure1_data/"#LOCAL DIRECTORY TO BE CHANGED

		category_list = ['chromosome', 'compartment', 'complex',
						 'housekeeping', 'jiyoye_essentiality', 'k562_essentiality',
						 'kbm7_essentiality', 'pathway', 'raji_essentiality',
						 'string_700']
		name_list = ["battle_protein","tiannan","battle_ribo", "battle_rna",
					 "primateRNA", "primatePRO", "wu","mann_all_log2","yibo",
					 "gygi1","gygi2","gygi3","tcga_ovarian",'tcga_breast',
					 'bxd_protein','colo_cancer']		

		concat_list = list()
		names_columns = list()
		category_columns = list()
		for name in name_list:
			for category in category_list:
				print(name,category)
				file_name = name + '_figure1_data_' + category.upper() + '.tsv.gz'
				data = DataFrameAnalyzer.open_in_chunks(folder, file_name)
				concat_list.append(data)
				for i in xrange(len(data)):
					names_columns.append(name)
					category_columns.append(category)
		data = pd.concat(concat_list)
		data['name'] = pd.Series(names_columns, index = data.index)
		data['category'] = pd.Series(category_columns, index = data.index)
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/'#LOCAL DIRECTORY TO BE CHANGED
		data.to_csv(output_folder + 'data/suppTable2_fig2a_underlying_data_ROCanalysis_' + time.strftime('%Y%m%d') +'.tsv',
					sep = '\t')

class figure2b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE2B: main_figure2b_vignette_complexEffect')
		figure2b.main_figure2b_vignette_complexEffect(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure2b_vignette_complexEffect(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('plot_complex_effect:tcga_ovarian')
		figure2b.plot_complex_effect('tcga_ovarian')
		print('plot_complex_effect:battle_protein')
		figure2b.plot_complex_effect('battle_protein')
		print('plot_complex_effect:gygi1')
		figure2b.plot_complex_effect('gygi1')
		print('plot_complex_effect:gygi3')
		figure2b.plot_complex_effect('gygi3')
		print('plot_complex_effect:tcga_breast')
		figure2b.plot_complex_effect('tcga_breast')
		print('plot_complex_effect:colo_cancer')
		figure2b.plot_complex_effect('colo_cancer')

	@staticmethod
	def get_data(name, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/' + name + '_figure1_data_COMPLEX.tsv.gz')
		return complex_data

	@staticmethod
	def plot_complex_effect(name, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_data = figure2b.get_data(name, folder = folder)
		other_correlations = utilsFacade.finite(list(complex_data[complex_data.label==False]["correlations"]))
		complex_correlations = utilsFacade.finite(list(complex_data[complex_data.label==True]["correlations"]))

		pval_list1 = list()
		for i in xrange(1,1000):
			corr1 = random.sample(complex_correlations,100)
			corr2 = random.sample(other_correlations,100)
			odds1, pval1 = scipy.stats.ranksums(corr1, corr2)
			pval_list1.append(pval1)
		print(np.mean(pval_list1))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		ax.set_ylabel('Density', fontsize=12)
		ax.set_xlabel('correlation coefficient (pearson)', fontsize=12)
		ax.hist(other_correlations, bins = 50, color='grey', alpha =0.3, normed = 1)
		plottingFacade.func_plotDensities_border(ax, other_correlations, 
												 linewidth = 2, alpha = 1, facecolor = 'grey')
		ax.hist(complex_correlations, bins = 50, color='#AF2D2D', alpha =0.3, normed =1)
		plottingFacade.func_plotDensities_border(ax, complex_correlations, 
												 linewidth = 2, alpha = 1, facecolor = '#AF2D2D')
		plottingFacade.make_full_legend(ax,['n(other)='+str(len(other_correlations)),
			'n(complex)='+str(len(complex_correlations)),
			'pvalComplex(Wilcoxon)='+str(np.mean(pval_list1))],['grey']*3, loc = 'upper left')
		ax.set_xlim(-1,1)
		plt.savefig(output_folder + 'figures/fig2b_' + name + '_complex_density_effect.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure2c_addendum:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('main_figure2c_atp_synthase')
		figure2c_addendum.main_figure2c_atp_synthase(output_folder = output_folder)
		print('additional_figure2c_all_complexes')
		figure2c_addendum.additional_figure2c_all_complexes(output_folder = output_folder)

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_rna.tsv.gz')
		ribo_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_ribo.tsv.gz')
		pro_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_protein.tsv.gz')
		return rna_data, ribo_data, pro_data

	@staticmethod
	def get_correlation_values(sub):
		quant_cols = utilsFacade.filtering(list(sub.columns),'quant_')
		quant_sub = sub[quant_cols]
		corr_data = quant_sub.T.corr()
		corr_values = utilsFacade.get_correlation_values(corr_data)
		return corr_values

	@staticmethod
	def get_complex_internal_corr_values_per_control_level(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data, ribo_data, pro_data = figure2c_addendum.get_data(folder = output_folder)
		complexes = list(set(pro_data.complex_search))
		pro_corr_values = list()
		rna_corr_values = list()
		ribo_corr_values = list()
		for complexID in complexes:
			pro_sub = pro_data[pro_data.complex_search==complexID]
			pro_corrs = figure2c_addendum.get_correlation_values(pro_sub)
			pro_corr_values.append(pro_corrs)
		for complexID in complexes:
			rna_sub = rna_data[rna_data.complex_search==complexID]
			rna_corrs = figure2c_addendum.get_correlation_values(rna_sub)
			rna_corr_values.append(rna_corrs)
		for complexID in complexes:
			ribo_sub = ribo_data[ribo_data.complex_search==complexID]
			ribo_corrs = figure2c_addendum.get_correlation_values(ribo_sub)
			ribo_corr_values.append(ribo_corrs)
		
		pro_corr_values = utilsFacade.flatten(pro_corr_values)
		rna_corr_values = utilsFacade.flatten(rna_corr_values)
		ribo_corr_values = utilsFacade.flatten(ribo_corr_values)
		return rna_corr_values, ribo_corr_values, pro_corr_values

	@staticmethod
	def additional_figure2c_all_complexes(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		corrs = figure2c_addendum.get_complex_internal_corr_values_per_control_level(output_folder = output_folder)
		rna_corr_values, ribo_corr_values, pro_corr_values = corrs
		
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True
		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		plottingFacade.func_plotDensities_border(ax, rna_corr_values, facecolor = 'purple', linewidth = 1)
		plottingFacade.func_plotDensities_border(ax, ribo_corr_values, facecolor = 'lightgreen', linewidth = 1)
		plottingFacade.func_plotDensities_border(ax, pro_corr_values, facecolor = 'green', linewidth = 1)
		ax.set_xlim(-1,1)
		ax.set_xlabel('Pearson correlation coefficient')
		ax.set_ylabel('Density')
		plt.savefig(output_folder + 'figures/fig2c_additional_buffering_allComplexes.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def main_figure2c_atp_synthase(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		rna_data, ribo_data, pro_data = figure2c_addendum.get_data(folder = output_folder)
		
		remove_proteins = ['RASSF1']
		rna_data = rna_data[rna_data.ComplexName.str.contains('F0/F1 ATP')]
		ribo_data = ribo_data[ribo_data.ComplexName.str.contains('F0/F1 ATP')]
		pro_data = pro_data[pro_data.ComplexName.str.contains('F0/F1 ATP')]
		rna_data = rna_data.drop(remove_proteins, axis = 0)
		ribo_data = ribo_data.drop(remove_proteins, axis = 0)

		rna_cols = utilsFacade.filtering(list(rna_data.columns), 'quant', condition = 'startswith')
		ribo_cols = utilsFacade.filtering(list(ribo_data.columns), 'quant', condition = 'startswith')
		pro_cols = filter(lambda a:str(a)!='quant_GM18871',utilsFacade.filtering(list(pro_data.columns),
				   'quant', condition = 'startswith'))

		rna_data = rna_data[rna_cols]
		ribo_data = ribo_data[ribo_cols]
		pro_data = pro_data[pro_cols]

		rna_corr = rna_data.T.corr()
		ribo_corr = ribo_data.T.corr()
		pro_corr = pro_data.T.corr()


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		sc_pro, colorList_pro = colorFacade.get_specific_color_gradient(plt.cm.Greens,
								np.array(list(xrange(len(pro_data.index)))), vmin = -1, vmax = 1)
		sc_rna, colorList_rna = colorFacade.get_specific_color_gradient(plt.cm.Purples,
								np.array(list(xrange(len(rna_data.index)))), vmin = -1, vmax = 1)
		sc_ribo, colorList_ribo = colorFacade.get_specific_color_gradient(plt.cm.YlGn,
								  np.array(list(xrange(len(ribo_data.index)))), vmin = -1, vmax = 1)

		rna_df = sns.clustermap(rna_corr).data2d
		pro_df = sns.clustermap(pro_corr).data2d
		ribo_df = sns.clustermap(ribo_corr).data2d

		plt.clf() 
		fig = plt.figure(figsize = (10,12)) 
		ax1 = fig.add_subplot(321)

		for p,protein in enumerate(list(rna_data.index)):
			temp = list(rna_data.loc[protein])
			ax1.plot(list(xrange(len(temp))), temp, color = sc_rna.to_rgba(p), linewidth = 0.2)
		ax1.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(rna_corr.mean())))]
		ax1.legend(lh, lt , loc = 'upper left')
		
		ax2 = fig.add_subplot(322)
		sns.heatmap(rna_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax2.set_xticklabels([])
		ax2.set_yticklabels([])

		ax3= fig.add_subplot(323)
		for p,protein in enumerate(list(ribo_data.index)):
			temp = list(ribo_data.loc[protein])
			ax3.plot(list(xrange(len(temp))), temp, color = sc_ribo.to_rgba(p), linewidth = 0.2)
		ax3.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(ribo_corr.mean())))]
		ax3.legend(lh, lt , loc = 'upper left')

		ax4 = fig.add_subplot(324)
		sns.heatmap(ribo_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax4.set_xticklabels([])
		ax4.set_yticklabels([])

		ax5 = fig.add_subplot(325)
		for p,protein in enumerate(list(pro_data.index)):
			temp = list(pro_data.loc[protein])
			ax5.plot(list(xrange(len(temp))), temp, color = sc_pro.to_rgba(p), linewidth = 0.2)
		ax5.set_xticklabels([])
		lh = [plt.Rectangle((0,0),1,1,fc = 'grey')]
		lt = ['av.R='+str(np.mean(list(pro_corr.mean())))]
		ax5.legend(lh, lt , loc = 'upper left')

		ax6 = fig.add_subplot(326)
		sns.heatmap(pro_df, cmap = plt.cm.RdBu, xticklabels = [], yticklabels = [], vmin = -1, vmax = 1,linewidth = 0.2)
		ax6.set_xticklabels([])
		ax6.set_yticklabels([])

		plt.savefig(output_folder + 'figures/fig2c_buffering_atpSynthase.pdf',bbox_inches = 'tight', dpi = 400)

class figure3:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		recluster_unbiasedly = kwargs.get("recluster_unbiasedly","")
		name = kwargs.get("name","protein_main")
		cmap = kwargs.get("cmap",plt.cm.RdBu)
		cmap_colorbar = kwargs.get("cmap_r",plt.cm.RdBu_r)

		print('FIGURE3: main_figure3_landscape')
		figure3.main_figure3_landscape(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure3_landscape(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		recluster_unbiasedly = kwargs.get("recluster_unbiasedly","")
		name = kwargs.get("name","protein_main")
		cmap = kwargs.get("cmap",plt.cm.RdBu)
		cmap_colorbar = kwargs.get("cmap_r",plt.cm.RdBu_r)

		'''
		print("get_gold_complexes")
		gold_complexes = figure3.get_gold_complexes()
		print("load_data")
		data_dict = figure3.load_data(gold_complexes)
		print("get_complex_info_dict")
		complex_info_dict = figure3.get_complex_info_dict(data_dict, gold_complexes)
		'''
		print('get_figure_data')
		complex_info_dict = figure3.get_figure_data()

		if name=="protein_main":
			print("prepare_df")
			df, dfList, real_df = figure3.prepare_df_protein(complex_info_dict, recluster_unbiasedly)
		elif name=="rna_supp":
			print("prepare_df")
			df, dfList = figure3.prepare_df_rna(complex_info_dict)

		print("plot_landscape")
		figure3.plot_landscape(df, dfList, real_df, name, cmap, output_folder)

	@staticmethod
	def get_median_correlations(dat, complexID):
		sub = dat[dat.ComplexName==complexID]
		quantCols = figure3.get_quantCols(sub)
		if complexID=="26S Proteasome":
			if "C2" in list(sub.index):
				sub=sub.drop(["C2"],0)
			if "C9" in list(sub.index):
				sub=sub.drop(["C9"],0)
		med_corr = -2
		if len(sub)>=5:
			corrData = sub[quantCols].T.corr()
			corrValues = utilsFacade.get_correlation_values(corrData)
			med_corr = np.median(utilsFacade.finite(corrValues))
		return med_corr,len(sub)

	@staticmethod
	def get_quantCols(dataset):
		quantCols = list()
		for col in list(dataset.columns):
			if col.startswith("quant_")==True and col!="quant_GM18871":
				quantCols.append(col)
		if len(quantCols) == 0:
			for col in list(dataset.columns):
				if col.find("Ratio")!=-1:
					quantCols.append(col)
		return quantCols

	@staticmethod
	def get_figure_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_info_dict = DataFrameAnalyzer.read_pickle(folder + 'data/fig3_complex_info_dictionary.pkl')
		return complex_info_dict

	@staticmethod
	def get_complex_info_dict(data_dict, gold_complexes, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		complex_info_dict = dict()

		for complexID in gold_complexes:
			med_corr_tcgaBreast,length_tcgaBreast = figure3.get_median_correlations(data_dict['tcga_breast'], complexID)
			med_corr_tcgaOvarian,length_tcgaOvarian = figure3.get_median_correlations(data_dict['tcga_ovarian'], complexID)
			med_corr_mann,length_mann = figure3.get_median_correlations(data_dict['mann'], complexID)
			med_corr_gygi3,length_gygi3 = figure3.get_median_correlations(data_dict['gygi3'], complexID)
			med_corr_gygi1,length_gygi1 = figure3.get_median_correlations(data_dict['gygi1'], complexID)
			med_corr_bp,length_bp = figure3.get_median_correlations(data_dict['battle_protein'], complexID)
			med_corr_gygi2,length_gygi2 = figure3.get_median_correlations(data_dict['gygi2'], complexID)
			med_corr_battle_rna,length_battle_rna = figure3.get_median_correlations(data_dict['battle_rna'], complexID)
			med_corr_battle_ribo,length_battle_ribo = figure3.get_median_correlations(data_dict['battle_ribo'], complexID)
			med_corr_tcgaColo,length_tcgaColo = figure3.get_median_correlations(data_dict['tcga_colo'], complexID)
			med_corrs = [med_corr_gygi3 ,med_corr_gygi1, med_corr_bp,
						 med_corr_tcgaBreast, med_corr_tcgaOvarian,
						 med_corr_mann, med_corr_tcgaColo]
			filtered_meds = filter(lambda a:a>-2, med_corrs)

			if len(filtered_meds)>=4:
				complex_name = complexID
				complex_info_dict.setdefault(complex_name,[]).append({
															  "bp": (med_corr_bp,length_bp),
															  "gygi1": (med_corr_gygi1,length_gygi1),
															  "gygi3": (med_corr_gygi3,length_gygi3),
															  "gygi2": (med_corr_gygi2,length_gygi2),
															  "brna": (med_corr_battle_rna,length_battle_rna),
															  "bribo": (med_corr_battle_ribo,length_battle_ribo),
															  "tcga_breast": (med_corr_tcgaBreast,length_tcgaBreast),
															  "tcga_ovarian": (med_corr_tcgaOvarian,length_tcgaOvarian),
															  "mann": (med_corr_mann,length_mann),
															  'tcga_colo':(med_corr_tcgaColo, length_tcgaColo)})
		DataFrameAnalyzer.to_pickle(complex_info_dict, folder + 'data/fig3_complex_info_dictionary.pkl')
		return complex_info_dict

	@staticmethod
	def prepare_df_protein(complex_info_dict, recluster_unbiasedly):
		gygi3_list = list()
		gygi1_list = list()
		gygi2_list = list()

		bp_list = list()
		brna_list = list()
		bribo_list = list()

		mann_list = list()
		tcga_breast_list = list()
		tcga_ovarian_list = list()
		tcga_colo_list = list()

		labelList = list()
		for complexID in complex_info_dict:
			gygi3_list.append(complex_info_dict[complexID][0]["gygi3"][0])
			gygi1_list.append(complex_info_dict[complexID][0]["gygi1"][0])
			#gygi2_list.append(complex_info_dict[complexID][0]["gygi2"][0])
			bp_list.append(complex_info_dict[complexID][0]["bp"][0])
			mann_list.append(complex_info_dict[complexID][0]["mann"][0])
			tcga_breast_list.append(complex_info_dict[complexID][0]["tcga_breast"][0])
			tcga_ovarian_list.append(complex_info_dict[complexID][0]["tcga_ovarian"][0])
			tcga_colo_list.append(complex_info_dict[complexID][0]["tcga_colo"][0])
			#brna_list.append(complex_info_dict[complexID][0]["brna"][0])
			#bribo_list.append(complex_info_dict[complexID][0]["bribo"][0])
			labelList.append(complexID.split("(")[0])

		df = pd.DataFrame([gygi3_list,gygi1_list,bp_list])#,gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi3","gygi1","battle-protein"]#,"gygi2","battle-ribo","battle-rna"]

		df = pd.DataFrame([gygi3_list,gygi1_list,
						   bp_list, mann_list,
						   tcga_breast_list,tcga_ovarian_list,
						   tcga_colo_list])#,gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi3","gygi1","battle-protein",
					"mann","tcga_breast","tcga_ovarian",
					'tcga_colo']#,"gygi2","battle-ribo","battle-rna"]

		if recluster_unbiasedly == True:
			df = utilsFacade.recluster_matrix(df)
		else:
			medians = [np.median(df[colname]) for colname in df.columns]
			colnames = list(df.columns)
			medians,colnames = zip(*sorted(zip(medians,colnames),reverse=True))
			df = df[list(colnames)]
		labelList = df.index
		columnList = df.columns
		dfList = map(list,df.values)

		subunit_length_list_gygi3 = dict()
		subunit_length_list_gygi1 = dict()
		subunit_length_list_bp = dict()
		subunit_length_list_mann = dict()
		subunit_length_list_tcgaBreast = dict()
		subunit_length_list_tcgaOvarian = dict()
		subunit_length_list_tcgaColo = dict()
		for complexID in complex_info_dict.keys():
			key = complexID.split("(")[0]
			subunit_length_list_gygi3.setdefault(key,[]).append(complex_info_dict[complexID][0]["gygi3"][1])
			subunit_length_list_gygi1.setdefault(key,[]).append(complex_info_dict[complexID][0]["gygi1"][1])
			subunit_length_list_bp.setdefault(key,[]).append(complex_info_dict[complexID][0]["bp"][1])
			subunit_length_list_mann.setdefault(key,[]).append(complex_info_dict[complexID][0]["mann"][1])
			subunit_length_list_tcgaBreast.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_breast"][1])
			subunit_length_list_tcgaOvarian.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_ovarian"][1])
			subunit_length_list_tcgaColo.setdefault(key,[]).append(complex_info_dict[complexID][0]["tcga_colo"][1])
		
		su_length_list_gygi3 = list()
		su_length_list_gygi1 = list()
		su_length_list_bp = list()
		su_length_list_mann = list()
		su_length_list_tcgaBreast = list()
		su_length_list_tcgaOvarian = list()
		su_length_list_tcgaColo = list()
		for complexID in columnList:
			su_length_list_bp.append(subunit_length_list_bp[complexID][0])
			su_length_list_gygi1.append(subunit_length_list_gygi1[complexID][0])
			su_length_list_gygi3.append(subunit_length_list_gygi3[complexID][0])
			su_length_list_mann.append(subunit_length_list_mann[complexID][0])
			su_length_list_tcgaBreast.append(subunit_length_list_tcgaBreast[complexID][0])
			su_length_list_tcgaOvarian.append(subunit_length_list_tcgaOvarian[complexID][0])
			su_length_list_tcgaColo.append(subunit_length_list_tcgaColo[complexID][0])

		df_lst = list()
		for df in dfList:
			rank_list = rankdata(df)
			minimum_rank = rank_list.min() 
			rank_list = [np.nan if item==minimum_rank else item for item in rank_list]
			df_lst.append(rank_list)
		df = pd.DataFrame(df_lst)
		df.index = labelList
		df.columns = columnList

		real_df = pd.DataFrame(dfList)
		real_df.index = labelList
		real_df.columns = columnList

		myArray = np.array(df)
		normalizedArray = []

		for row in range(0, len(myArray)):
			list_values = []
			Min =  min(utilsFacade.finite(list(myArray[row])))
			Max = max(utilsFacade.finite(list(myArray[row])))
			mean = np.mean(utilsFacade.finite(list(myArray[row])))
			std = np.std(utilsFacade.finite(list(myArray[row])))

			for element in myArray[row]:
				list_values.append((element - mean)/std)
			normalizedArray.append(list_values)

		newArray = []
		for row in range(0, len(normalizedArray)):
			list_values = normalizedArray[row]
			newArray.append(list_values)
		new_df = pd.DataFrame(newArray)
		new_df.columns = list(df.columns)
		new_df.index = df.index
		df = new_df.copy()
		dfList=map(list,df.values)

		"""
		df=self.recluster_matrix(df)
		"""
		return df,dfList, real_df

	@staticmethod
	def prepare_df_rna(complex_info_dict):
		gygi2_list = list()
		brna_list = list()
		bribo_list = list()
		labelList = list()
		for complexID in complex_info_dict:
			gygi2_list.append(complex_info_dict[complexID][0]["gygi2"][0])
			brna_list.append(complex_info_dict[complexID][0]["brna"][0])
			bribo_list.append(complex_info_dict[complexID][0]["bribo"][0])
			labelList.append(complexID.split("(")[0])
		df = pd.DataFrame([gygi2_list,bribo_list,brna_list])
		df.columns = labelList
		df.index = ["gygi2","battle-ribo","battle-rna"]
		df = utilsFacade.recluster_matrix(df)
		labelList = df.index
		columnList = df.columns
		dfList = map(list,df.values)

		df_lst = list()
		for df in dfList:
			df_lst.append(rankdata(df))
		df = pd.DataFrame(df_lst)
		df.index = labelList
		df.columns = columnList
		df = utilsFacade.recluster_matrix(df)
		return df,dfList

	@staticmethod
	def get_scalarmap(cmap, dfList):
		scalarmap1,colorList1 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[0])))
		scalarmap2,colorList2 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[1])))
		scalarmap3,colorList3 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[2])))
		scalarmap4,colorList4 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[3])))
		scalarmap5,colorList5 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[4])))
		scalarmap6,colorList6 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[5])))
		scalarmap7,colorList7 = colorFacade.get_specific_color_gradient(cmap,np.array(utilsFacade.finite(dfList[6])))
		all_values = utilsFacade.flatten(dfList)
		scalarmap,colorList = colorFacade.get_specific_color_gradient(cmap,
							  np.array(utilsFacade.finite(all_values)), vmin = -2, vmax = 2)
		scalarmap_dict = {'1':scalarmap1,'2':scalarmap2, '3':scalarmap3,
						  '4': scalarmap4, '5': scalarmap5, '6':scalarmap6,'7': scalarmap7,
						  'all':scalarmap}
		return scalarmap_dict,all_values

	@staticmethod
	def plot_landscape(df, dfList, real_df, name, cmap, output_folder, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complex_list = list(real_df.columns)
		real_df = real_df.replace(-2, np.nan)
		corr_median_list = list(real_df.median())
		corr_median_list, complex_list = zip(*sorted(zip(corr_median_list, complex_list), reverse = True))
		df = df[list(complex_list)]
		scalarmap_dict, all_values = figure3.get_scalarmap(cmap, dfList)


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig=plt.figure(figsize=(10,4))
		gs = gridspec.GridSpec(10,10)
		ax = plt.subplot(gs[0:3,0:])
		ind = np.arange(len(corr_median_list))
		width = 0.85
		sc, color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys_r,
						 np.array(list(xrange(len(corr_median_list)))))
		rects_all = ax.bar(ind, corr_median_list, width, color=color_list, edgecolor = 'white')		

		ax.set_xlim(0,len(corr_median_list)+0.5)
		ax.set_ylim(min(corr_median_list), max(corr_median_list))
		perc25, perc75 = np.percentile(corr_median_list,[25,75])
		ax.set_xticklabels([])
		ax.axhline(perc25, color = 'red', linewidth = 0.5, linestyle = '--')
		ax.axhline(perc75, color = 'red', linewidth = 0.5, linestyle = '--')

		ax = plt.subplot(gs[3:8,0:])
		plt.rcParams["axes.grid"] = False
		dfList = map(list,df.values)
		count = 0
		for item in dfList:
			colors = list()
			for i in item:
				if str(i) == 'nan':
					color = np.array(colorFacade.hex_to_rgb('#808080'))/256.0
					color = list(color)
					color.append(1)
					colors.append(color)
				else:
					'''
					if count==0:
						colors.append(scalarmap_dict['1'].to_rgba(i))
					elif count==1:
						colors.append(scalarmap_dict['2'].to_rgba(i))
					elif count==2:
						colors.append(scalarmap_dict['3'].to_rgba(i))
					elif count==3:
						colors.append(scalarmap_dict['4'].to_rgba(i))
					elif count==4:
						colors.append(scalarmap_dict['5'].to_rgba(i))
					elif count==5:
						colors.append(scalarmap_dict['6'].to_rgba(i))
					elif count==6:
						colors.append(scalarmap_dict['7'].to_rgba(i))
					'''
					colors.append(scalarmap_dict['all'].to_rgba(i))
			ax.scatter(xrange(len(item)),[count]*len(item),
					   color=colors,s=200,marker="s",edgecolor="white")
			ax.scatter(len(item),[count],color="white",
					   s=200,marker="s",edgecolor="white")
			count+=1
		plt.yticks(list(xrange(len(dfList))))
		if name=="protein_main":
			ylabelList = ["DO mouse strains(P)", "Founder mouse strains(P)",
						  "Human Individuals(P)", "Human cell types(P)",
						  "TCGA Breast Cancer (P)", "TCGA Ovarian Cancer(P)",
						  'TCGA Colorectal Cancer(P)']
		else:
			ylabelList = ["DO mouse strains(RS)","Human Individuals(RP)","Human Individuals(RS)"]
		ax.set_yticklabels(list(ylabelList))
		plt.xticks(list(xrange(len(df.columns))))
		ax.set_xticklabels(list(df.columns), rotation=45, fontsize=8, ha="right")
		ax.set_xlim(-1,len(df.columns)-0.5)
		plt.savefig(output_folder + "figures/fig3_landscape_variability_complexes_" + name + ".pdf",
					bbox_inches="tight", dpi=400)


		all_values = utilsFacade.flatten(dfList)
		scalarmap,colorList = colorFacade.get_specific_color_gradient(cmap,
							  np.array(utilsFacade.finite(all_values)), vmin = -2, vmax = 2)

		plt.rcParams["axes.grid"] = True
		plt.clf()
		fig = plt.figure(figsize=(10,1))
		ax = fig.add_subplot(111)
		ax.set_xticklabels([])
		ax.set_yticklabels([])
		cbar = fig.colorbar(scalarmap,orientation="horizontal")
		plt.savefig(output_folder + "figures/fig3_landscape_variability_complexes_" + name + "_LEGEND.pdf",
					bbox_inches="tight", dpi=400)

class figure4a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE4A: main_figure4a_complex_intern_landscapes')
		figure4a.main_figure4a_complex_intern_landscapes(folder = folder, output_folder = output_folder)

	@staticmethod
	def main_figure4a_complex_intern_landscapes(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		do_ranking = kwargs.get('do_ranking', False)

		print('get_data')
		data_dict = figure4a.get_data(folder = output_folder)
		name_list = ['gygi3','gygi1','battle_protein','mann_all_log2', 'tcgaBreast',
					 'tcgaOvarian', 'tcgaColoCancer']
		data_list = [data_dict['gygi3'], data_dict['gygi1'], data_dict['battle_protein'],
					data_dict['mann'], data_dict['tcgaBreast'], data_dict['tcgaOvarian'],
					data_dict['tcgaColoCancer']]

		print('iteration_complexes')
		figure4a.iteration_complexes(data_list, name_list, output_folder = output_folder, do_ranking = do_ranking)

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		stoch_gygi3 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		stoch_gygi1 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi1.tsv.gz')
		stoch_mann = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_mann.tsv.gz')
		stoch_battle_protein = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_battle_protein.tsv.gz')
		stoch_tcgaBreast = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaBreast.tsv.gz')
		stoch_tcgaOvarian = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaOvarian.tsv.gz')
		stoch_tcgaColorCancer = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_tcgaColoCancer.tsv.gz')
		return {'gygi3':stoch_gygi3, 'gygi1':stoch_gygi1, 'mann':stoch_mann,
				'battle_protein':stoch_battle_protein, 'tcgaBreast':stoch_tcgaBreast,
				'tcgaOvarian':stoch_tcgaOvarian, 'tcgaColoCancer':stoch_tcgaColorCancer}

	@staticmethod
	def load_relevant_complexes(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		com_data = DataFrameAnalyzer.getFile(folder, 'data/figure4a_relevant_complexes.tsv')
		relevant_complexes = list(com_data.index)
		return relevant_complexes

	@staticmethod
	def prepare_variance_dataframe(name_list, data_list, complex_df, proteins):
		df_list = list()
		for protein in proteins:
			temp = list()
			for d,n in zip(data_list, name_list):
				sub = complex_df[complex_df.fileName==n]
				if protein in list(sub.index):
					if type(sub.loc[protein]) == pd.DataFrame:
						quant_cols = utilsFacade.filtering(d, 'quant_', condition = 'startswith')
						s = sub.loc[protein][quant_cols].T
						coverage_list = list()
						for c,col in enumerate(list(s.columns)):
							temp_finite = utilsFacade.finite(list(np.array(s)[:,c]))
							tempSmall = list(np.array(s)[:,c])
							coverage_list.append(float(len(temp_finite))/float(len(tempSmall))*100)
						s = sub.loc[protein]
						s['coverage'] = pd.Series(coverage_list, index = s.index)
						s = s.sort_values('coverage', ascending = False)
						var_value = s.iloc[0]['relative_variance']
						temp.append(var_value)
					else:
						var_value = sub.loc[protein]['relative_variance']
						temp.append(var_value)
				else:
					temp.append(np.nan)
			df_list.append(temp)
		df = pd.DataFrame(df_list)
		df.index = proteins
		df.columns = name_list
		df = df.dropna(thresh = int(len(df.columns)/2.0))
		df = df.T
		return df

	@staticmethod
	def rank_dataset(df, proteins, name_list):
		df_lst = list()
		dfList = map(list,df.values)
		for f in dfList:
			rank_list = rankdata(f)
			all_ranks = rankdata(utilsFacade.finite(f))
			if len(all_ranks)>0:
				minimum_rank = all_ranks.min() 
			temp_list = list()
			for fitem, rank in zip(f, rank_list):
				if str(fitem) != 'nan':
					temp_list.append(rank)
				else:
					temp_list.append(np.nan)
			df_lst.append(temp_list)
		df = pd.DataFrame(df_lst)
		df.columns = proteins
		df.index = name_list
		return df

	@staticmethod
	def get_zscores(df):
		myArray = np.array(df)
		normalizedArray = []
		for row in range(0, len(myArray)):
			list_values = []
			Min =  min(utilsFacade.finite(list(myArray[row])))
			Max = max(utilsFacade.finite(list(myArray[row])))
			mean = np.mean(utilsFacade.finite(list(myArray[row])))
			std = np.std(utilsFacade.finite(list(myArray[row])))
			for element in myArray[row]:
				list_values.append((element - mean)/std)
			normalizedArray.append(list_values)

		newArray = []
		for row in range(0, len(normalizedArray)):
			list_values = normalizedArray[row]
			newArray.append(list_values)

		new_df = pd.DataFrame(newArray)
		new_df.columns = list(df.columns)
		new_df.index = df.index
		df = new_df.copy()
		df = df.iloc[::-1]
		dfList = map(list,df.values)
		return df, dfList

	@staticmethod
	def manage_proteasome_data(df):
		try:
			df = df.drop(['PSD3','C9','C2','C6','RPN1'], axis = 1)
		except:
			remove_list = ['PSD3','C9','C2','C6','RPN1']
			remove_list1 = [item[0] + item[1:].lower() for item in remove_list]
			remove_list = remove_list + remove_list1
			df = df[~df.index.isin(remove_list)]
		return df		

	@staticmethod
	def plot_heatmap(df, complex_id, altName, **kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		sc, color_list = colorFacade.get_specific_color_gradient(plt.cm.RdBu_r,
																 np.array(utilsFacade.finite(utilsFacade.flatten(map(list, df.values)))),
																 vmin = -2, vmax = 2)
		
		plt.clf()
		if complex_id.find('26S')!=-1 or complex_id.find('NPC')!=-1:
			fig = plt.figure(figsize = (10,3))
		else:
			fig = plt.figure(figsize = (5,3))
		ax = fig.add_subplot(111)
		mask = df.isnull()
		mask1 = mask.copy()
		mask1 = mask1.replace(True,'bla')
		mask1 = mask1.replace(False,'20')
		mask1 = mask1.replace('bla',1)
		mask1 = mask1.replace('20',0)
		sns.heatmap(mask1, cmap = ['grey'], linewidth = 0.2, cbar = False,
					xticklabels = [], yticklabels = [])
		sns.heatmap(df, cmap = plt.cm.RdBu_r, linewidth = 0.2, mask = mask,
					vmin = -2, vmax = 2)
		plt.savefig(output_folder + 'figures/fig4a_' + altName + '_heatmap_subunits.pdf', 
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def get_alternative_name(complex_id):
		altName = complex_id
		altName = '_'.join(altName.split(' '))
		altName = altName.replace('/','')
		altName = altName.replace(':','')
		return altName

	@staticmethod
	def iteration_complexes(data_list, name_list, **kwargs):
		do_ranking = kwargs.get('do_ranking', False)
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		relevant_complexes = figure4a.load_relevant_complexes()
		concatanated_complex_dfs = list()
		for complex_id in relevant_complexes:
			print('*************************')
			print(complex_id)
			altName = figure4a.get_alternative_name(complex_id)

			concat_list = list()
			for d,n in zip(data_list, name_list):
				sub = d[d.ComplexName == complex_id]
				if len(sub)>0:
					sub['fileName'] = pd.Series([n]*len(sub), index = sub.index)
					concat_list.append(sub)
			complex_df = pd.concat(concat_list)
			complex_df.index = [item.upper() for item in list(complex_df.index)]
			proteins = list(set(complex_df.index))			

			df = figure4a.prepare_variance_dataframe(name_list, data_list, complex_df, proteins)
			if do_ranking == True:
				df = figure4a.rank_dataset(df, proteins, name_list)
			df, dfList = figure4a.get_zscores(df)
			if complex_id.find('26S')!=-1:
				df = figure4a.manage_proteasome_data(df)

			protein_list = list(df.columns)
			label_list = list()
			median_list = list()
			for c,col in enumerate(df.columns):
				median_list.append(np.mean(utilsFacade.finite(list(np.array(df)[:,c]))))
				label_list.append(col)
			median_list, label_list = zip(*sorted(zip(median_list, label_list), reverse = False))
			df = df[list(label_list)]
			if complex_id.find('Kornberg')!=-1:
				complex_id = 'Kornbergs mediator (SRB) complex'

			df1 = df.T.copy()
			df1['complex_id'] = pd.Series([complex_id]*len(df1), index = df1.index)
			concatanated_complex_dfs.append(df1)
			print('plot_heatmap')
			print('*************************')

			figure4a.plot_heatmap(df, complex_id, altName, output_folder = output_folder)

		complex_data = pd.concat(concatanated_complex_dfs)
		complex_data.to_csv(output_folder + 'data/fig4a_underlyingData_complexes_RNA_newData_' + time.strftime('%Y%m%d') + '.tsv',
							sep = '\t')
		return complex_data

	@staticmethod
	def read_data():
		complex_data = DataFrameAnalyzer.getFile('/g/scb2/bork/romanov/wpc/',
					   'wpc_figure2B_underlyingData_complexes_RNA_newData.tsv')
		quant_data = complex_data.drop(['complex_id','mean','p.value','label'],1)
		mean_vector = quant_data.T.mean()
		median_vector = quant_data.T.median()
		pvalues = list()
		label_list = list()
		for zscore in median_vector:
			pval = utilsFacade.zscore_to_pvalue(zscore)[1]
			pvalues.append(pval)
			if pval<0.1 and zscore>0:
				label_list.append('variable')
			elif pval<0.1 and zscore<0:
				label_list.append('stable')
			else:
				label_list.append('')
		complex_data['median'] = pd.Series(list(median_vector), index = complex_data.index)
		complex_data['p.value'] = pd.Series(pvalues, index = complex_data.index)
		complex_data['label'] = pd.Series(label_list, index = complex_data.index)
		complex_data.to_csv('/g/scb2/bork/romanov/wpc/' + 
							'wpc_figure2B_underlyingData_complexes_RNA_newData.tsv',
							sep = '\t')

class figure4b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE4B: main_figure4b_26SProteasome_analysis')
		figure4b.main_figure4b_26SProteasome_analysis()

	@staticmethod
	def main_figure4b_26SProteasome_analysis(**kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print("load_data")
		dat_gygi3_26S, dat_battle_protein_26S = figure4b.load_data(folder = output_folder)
		print("map_variance")
		var_dict_gygi3, var_dict_bp = figure4b.map_variance(dat_gygi3_26S,
									  dat_battle_protein_26S, scatter_ranked = scatter_ranked)
		print("prepare_plotting_data")
		mutual_proteins, varList1, varList2 = figure4b.prepare_plotting_data(var_dict_gygi3,
											  var_dict_bp, scatter_ranked = scatter_ranked)
		print('make_scatter_plot')
		figure4b.make_scatter_plot(mutual_proteins, varList1, varList2,
								   scatter_ranked = scatter_ranked,
								   output_folder = output_folder)
		print("load_boxplot_data")
		sig_data_gygi3, sig_data_bp = figure4b.load_boxplot_data()
		print('plot_boxplot_complexVariance')
		figure4b.plot_boxplot_complexVariance(sig_data_gygi3,
											  output_folder =  output_folder)

	@staticmethod
	def load_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		dat_gygi3 = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_gygi3.tsv.gz')
		dat_battle_protein = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_battle_protein.tsv.gz')

		quant_cols_gygi3 = utilsFacade.get_quantCols(dat_gygi3)
		quant_cols_bp = utilsFacade.get_quantCols(dat_battle_protein)

		dat_gygi3_26S = dat_gygi3[dat_gygi3.ComplexName=="26S Proteasome"][quant_cols_gygi3].T
		dat_battle_protein_26S = dat_battle_protein[dat_battle_protein.ComplexName=="26S Proteasome"][quant_cols_bp].T
		return dat_gygi3_26S, dat_battle_protein_26S

	@staticmethod
	def map_variance(dat_gygi3_26S, dat_battle_protein_26S, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")

		varList_gygi3 = list()
		for c,col in enumerate(list(dat_gygi3_26S.columns)):
			varList_gygi3.append(np.var(utilsFacade.finite(list(dat_gygi3_26S.iloc[:,c]))))
		varList_bp = list()
		for c,col in enumerate(list(dat_battle_protein_26S.columns)):
			varList_bp.append(np.var(utilsFacade.finite(list(dat_battle_protein_26S.iloc[:,c]))))

		dat_gygi3_26S = dat_gygi3_26S.T
		dat_battle_protein_26S = dat_battle_protein_26S.T
		dat_gygi3_26S["variance"] = pd.Series(varList_gygi3, index=dat_gygi3_26S.index)
		dat_battle_protein_26S["variance"] = pd.Series(varList_bp, index=dat_battle_protein_26S.index)
		dat_gygi3_26S["ranked_variance"] = pd.Series(rankdata(varList_gygi3), index=dat_gygi3_26S.index)
		dat_battle_protein_26S["ranked_variance"] = pd.Series(rankdata(varList_bp), index=dat_battle_protein_26S.index)

		if scatter_ranked == "no":
			var_dict_gygi3 = dat_gygi3_26S.to_dict()["variance"]
			var_dict_bp = dat_battle_protein_26S.to_dict()["variance"]
		else:
			var_dict_gygi3 = dat_gygi3_26S.to_dict()["ranked_variance"]
			var_dict_bp = dat_battle_protein_26S.to_dict()["ranked_variance"]
		return var_dict_gygi3,var_dict_bp
	
	@staticmethod
	def prepare_plotting_data(var_dict_gygi3, var_dict_bp, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")

		mutual_proteins = list(set([item.upper() for item in list(var_dict_gygi3.keys())]).intersection(set(var_dict_bp.keys())))
		varList1 = list()
		varList2 = list()
		for protein in mutual_proteins:
			if scatter_ranked == "no":
				varList1.append(np.log10(var_dict_gygi3[protein[0] + protein[1:].lower()]))
				varList2.append(np.log10(var_dict_bp[protein]))
			else:
				varList1.append(var_dict_gygi3[protein[0] + protein[1:].lower()])
				varList2.append(var_dict_bp[protein])
		return mutual_proteins,varList1,varList2

	@staticmethod
	def make_scatter_plot(mutual_proteins, varList1, varList2, **kwargs):
		scatter_ranked = kwargs.get("scatter_ranked","no")
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		immuno_proteasome=["PSMB5","PSMB6","PSMB7","PSMB8","PSMB9","PSMB10"]
		#immuno_proteasome=["ATPIF1"]
		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(4,3))
		ax = fig.add_subplot(111)
		count = 0
		for var1,var2 in zip(varList1,varList2):
			if mutual_proteins[count]!="PSMA8":#not enough data points
				if mutual_proteins[count] in immuno_proteasome:
					color = "orange"
				else:
					if mutual_proteins[count].find("A")!=-1 or mutual_proteins[count].find("B")!=-1:
						color = "#95DCEC"
					else:
						color = "grey"
				ax.scatter(var1,var2,alpha=0.8,color=color,edgecolor="black",s=100,linewidth=1)
				if scatter_ranked=="no":
					if var1>=0.5 or var2>=-1.5:
						if mutual_proteins[count]=="PSMB8":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2-0.25),fontsize=12)
						elif mutual_proteins[count]=="PSMB10":
							ax.annotate(mutual_proteins[count],xy=(var1-0.25,var2-0.3),fontsize=12)
						elif mutual_proteins[count]=="PSMB6":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2+0.05),fontsize=12)
						elif mutual_proteins[count]=="PSMB5":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2-0.1),fontsize=12)
						elif mutual_proteins[count]=="PSMB7":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMD9":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMB4":
							ax.annotate(mutual_proteins[count],xy=(var1-0.05,var2+0.15),fontsize=12)
						else:
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2),fontsize=12)
				else:
					if var1>=20 or var2>=20:
						if mutual_proteins[count]=="PSMB8":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2-0.25),fontsize=12)
						elif mutual_proteins[count]=="PSMB10":
							ax.annotate(mutual_proteins[count],xy=(var1-0.25,var2-0.3),fontsize=12)
						elif mutual_proteins[count]=="PSMB6":
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2+0.05),fontsize=12)
						elif mutual_proteins[count]=="PSMB5":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2-0.1),fontsize=12)
						elif mutual_proteins[count]=="PSMB7":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMD9":
							ax.annotate(mutual_proteins[count],xy=(var1+0.1,var2),fontsize=12)
						elif mutual_proteins[count]=="PSMB4":
							ax.annotate(mutual_proteins[count],xy=(var1-0.05,var2+0.15),fontsize=12)
						else:
							ax.annotate(mutual_proteins[count],xy=(var1+0.05,var2),fontsize=12)
			count+=1
		lt = ["immunoproteasome","20S sub-complex","19S sub-complex"]
		lh = [plt.Rectangle((0,0),1,1,fc="orange"),plt.Rectangle((0,0),1,1,fc="#95DCEC"),plt.Rectangle((0,0),1,1,fc="grey")]
		if scatter_ranked=="no":
			ax.set_xlabel("DO mouse strains(P)\n[variance (-log10)]",fontsize=12)
			ax.set_ylabel("Human Individuals(P)\n[variance (-log10)]",fontsize=12)
		else:
			ax.set_xlabel("DO mouse strains(P)\n[ranked variance]",fontsize=12)
			ax.set_ylabel("Human Individuals(P)\n[ranked_variance]",fontsize=12)
		ax.legend(lh,lt,bbox_to_anchor=(0.75,-0.3),fontsize=10,frameon=True)
		if scatter_ranked=="no":
			plt.savefig(output_folder + "figures/fig4bX_variance_comparison_26SProteasome.pdf",
						bbox_inches="tight",dpi=600)
		else:
			plt.savefig(output_folder + "figures/fig4bX_variance_comparison_26SProteasome_rankedVector.pdf",
						bbox_inches="tight",dpi=600)

	@staticmethod
	def load_boxplot_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		sig_data_gygi3 = data[data.ComplexID=="26S Proteasome"]
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_battle_protein.tsv.gz')
		sig_data_bp = data[data.ComplexID=="26S Proteasome"]
		return sig_data_gygi3,sig_data_bp

	@staticmethod
	def plot_boxplot_complexVariance(sig_data, **kwargs):
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		dataset = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		dataset = dataset[dataset.ComplexID=="26S Proteasome"]
		quantCols = utilsFacade.get_quantCols(sig_data)
		dataset = dataset[quantCols]
		proteinList = list(dataset.index)
		dataset.index = proteinList
		dataset = dataset.drop_duplicates()

		complexID = "26S Proteasome"
		sub = sig_data[sig_data.ComplexID==complexID]
		sub = sub.sort_values("relative_variance")
		pvalueDict = sub.to_dict()["levene_pval.adj"]
		varianceDict = sub.to_dict()["relative_variance"]
		mean_variance = np.mean(utilsFacade.finite(sub["relative_variance"]))
		stateDict = sub.to_dict()["state"]
		quant_sub = sub[quantCols].T
		dsub = dataset.T[quant_sub.columns]
		dsub = dsub.T.drop_duplicates()
		dsub = dsub.T

		original_dataList = list()
		dataList1 = list()
		proteins1 = list()
		dataList2 = list()
		proteins = list()
		proteins2 = ["Psmb7","Psmd10","Psmb6","Psmb5","Psmb8","Psmd9","Psmb9","Psmb10"]
		for c,col in enumerate(dsub.columns):
			if col!="Psma8" and col!="Psmd4":#not enough datapoints; mention in Materials&Methods
				if col in proteins2:
					dataList2.append(utilsFacade.finite(dsub.iloc[:,c]))
					proteins.append(col)
				else:
					dataList1.append(utilsFacade.finite(dsub.iloc[:,c]))
					proteins1.append(col)
		proteins2 = proteins

		immuno_proteasome = ["Psmb5","Psmb6","Psmb7","Psmb8","Psmb9","Psmb10"]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize=(15,6))
		gs = gridspec.GridSpec(3,3)
		ax = plt.subplot(gs[0:,0:2])
		ax.axhline(1, color = 'k', linestyle = '--')
		ax.axhline(0.5, color = 'k', linestyle = '--')
		ax.axhline(-1, color = 'k', linestyle = '--')
		ax.axhline(0, color = 'k', linestyle = '--')
		ax.axhline(-0.5, color = 'k', linestyle = '--')
		bp=ax.boxplot(dataList1,notch=0,sym="",vert=1,patch_artist=True,widths=[0.8]*len(dataList1))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black")
		plt.setp(bp['whiskers'], color="black")
		for i,patch in enumerate(bp['boxes']):
			protein=proteins1[i]
			x = numpy.random.normal(i+1, 0.04, size=len(dataList1[i]))
			if protein in immuno_proteasome:
				patch.set_facecolor("orange")	
				patch.set_edgecolor("orange")
			else:
				patch.set_facecolor("grey")	
				patch.set_edgecolor("grey")
			patch.set_alpha(0.8)
		plt.xticks(list(xrange(len(proteins1)+1)))
		ax.set_xlim(0,len(proteins1)+1)
		ax.set_xticklabels([""]+proteins1,fontsize=13,rotation=90)
		ax.set_ylabel("Relative Abundance",fontsize=13)
		plt.tick_params(axis="y",which="both",bottom="off",top="off",labelsize=12)
		complex_name = complexID.replace("/","_")
		complex_name = complex_name.replace(":","_")
		ax.set_ylim(-1.5,1.5)

		ax = plt.subplot(gs[0:,2:])
		ax.axhline(1, color = 'k', linestyle = '--')
		ax.axhline(0.5, color = 'k', linestyle = '--')
		ax.axhline(-1, color = 'k', linestyle = '--')
		ax.axhline(-0.5, color = 'k', linestyle = '--')
		ax.axhline(0, color = 'k', linestyle = '--')
		bp = ax.boxplot(dataList2,notch=0,sym="",vert=1,patch_artist=True,widths=[0.8]*len(dataList2))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black")
		plt.setp(bp['whiskers'], color="black")
		for i,patch in enumerate(bp['boxes']):
			protein = proteins2[i]
			x = numpy.random.normal(i+1, 0.04, size=len(dataList2[i]))
			if protein in immuno_proteasome:
				patch.set_facecolor("orange")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="brown",s=20)		
			elif protein.find("a")!=-1 or protein.find("b")!=-1:
				patch.set_facecolor("#95DCEC")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="darkblue",s=20)		
			elif protein.find("c")!=-1 or protein.find("d")!=-1:
				patch.set_facecolor("grey")	
				ax.scatter(x,dataList2[i],color='white', alpha=0.9,edgecolor="black",s=20)		
			patch.set_edgecolor("black")
			patch.set_alpha(0.8)
		plt.xticks(list(xrange(len(proteins2)+1)))
		ax.set_xlim(0,len(proteins2)+1)
		ax.set_xticklabels([""]+[item.upper() for item in proteins2],fontsize=13,rotation=90)
		ax.set_yticklabels([])
		plt.tick_params(axis="y",which="both",bottom="off",top="off",labelsize=12)
		complex_name = complexID.replace("/","_")
		complex_name = complex_name.replace(":","_")
		ax.set_ylim(-1.5,1.5)
		plt.savefig(output_folder + "figures/fig4b_stochiometry_data_complexes_26SProteasome_gygi3.pdf",
					bbox_inches="tight",dpi=600)

class figure5a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5A: figure5a_complex_abundanceChange_demonstration')
		figure5a.figure5a_complex_abundanceChange_demonstration()
		print('FIGURE5A: figure5a_overview_stoichiometry')
		figure5a.figure5a_overview_stoichiometry()

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = DataFrameAnalyzer.read_pickle(folder + 'data/figure5a_data_dictionary.pkl')
		relevant_complexes = ['HC663:COPII','HC2402:COPI','HC1479:retromer complex',
					  		  'SMC_AM000047:Cohesin complex']
		#tRNA splicing endonuclease:tRNA splicing endonuclease''
		return data_dict, relevant_complexes

	@staticmethod
	def figure5a_complex_abundanceChange_demonstration(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict, relevant_complexes = figure5a.get_data(folder = folder)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		positions = [1,1.5]
		plt.clf()
		fig = plt.figure(figsize = (10,5))
		ax = fig.add_subplot(141)
		dataList = [data_dict[relevant_complexes[0]][0],data_dict[relevant_complexes[0]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(142)
		dataList = [data_dict[relevant_complexes[1]][0],data_dict[relevant_complexes[1]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(143)
		dataList = [data_dict[relevant_complexes[2]][0],data_dict[relevant_complexes[2]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		ax = fig.add_subplot(144)
		dataList = [data_dict[relevant_complexes[3]][0],data_dict[relevant_complexes[3]][1]]
		bp=ax.boxplot(dataList,notch=0,sym="",vert=1,patch_artist=True,
			widths=[0.4]*len(dataList),positions=positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			if i%2==0:
				patch.set_facecolor("#7FBF7F")	
			else:
				patch.set_facecolor("orange")	
			patch.set_edgecolor("black")
			patch.set_alpha(0.9)
			sub_group=dataList[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(dataList[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_ylim(7,14)
		plt.savefig(folder + 'figures/fig5a_complex_abundances.pdf',
					bbox_inches = 'tight',dpi = 400)

	@staticmethod
	def get_limma_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz',
				    							sep = '\t', compression = 'gzip')
		data = data.drop_duplicates()
		quant_data = data[data.analysis_type=='quant_male-female']		
		stoch_data = data[data.analysis_type=='stoch_male-female']

		'''
		data = DataFrameAnalyzer.open_in_chunks(folder, 'data/gygi3_complex_hs_merged_perGeneRun.tsv.gz',
				    							sep = '\t', compression = 'gzip')
		data = data.drop_duplicates()
		quant_data = data[data.analysis_type=='quant_highfat-chow']		
		stoch_data = data[data.analysis_type=='stoch_highfat-chow']
		'''
		return data, quant_data, stoch_data

	@staticmethod
	def prepare_stochiometry_hit_dictionary(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data = figure5a.get_limma_data(folder = folder)
		complex_set = list(set(data.complex_search))
		cov_sig_dict = dict()
		num_dict = dict()
		for complexID in complex_set:
			quant_sub = quant_data[quant_data.complex_search==complexID]
			stoch_sub = stoch_data[stoch_data.complex_search==complexID]
			if len(stoch_sub)>=4:
				num_dict[complexID] = len(stoch_sub)
				stoch_sig_sub = stoch_sub[stoch_sub['pval.adj']<=0.01]
				stoch_non_sub = stoch_sub[stoch_sub['pval.adj']>0.01]
				coverage = float(len(stoch_sig_sub))/float(len(stoch_sub))*100
				cov_sig_dict[complexID] = coverage
		DataFrameAnalyzer.to_pickle(cov_sig_dict, folder + 'data/figure5a_stoichiometric_hits_per_complex_dict.pkl')
		DataFrameAnalyzer.to_pickle(num_dict, folder + 'data/figure5a_stoichiometric_hitsNUM_per_complex_dict.pkl')
		#DataFrameAnalyzer.to_pickle(cov_sig_dict, folder + 'data/figure5a_stoichiometric_diet_hits_per_complex_dict.pkl')
		#DataFrameAnalyzer.to_pickle(num_dict, folder + 'data/figure5a_stoichiometric_diet_hitsNUM_per_complex_dict.pkl')
		return cov_sig_dict, num_dict

	@staticmethod
	def get_stoichiometric_hits_per_complex_dict(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		cov_sig_dict = DataFrameAnalyzer.read_pickle(folder + 'data/figure5a_stoichiometric_hits_per_complex_dict.pkl')

		'''
		for key in cov_sig_dict:
			if key.find('mitochondrial outer')!=-1:
				raise Exception()
		'''
		return cov_sig_dict

	@staticmethod
	def prepare_complex_abundance_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		ffolder = '/g/scb2/bork/romanov/wpc/'
		abu_data = DataFrameAnalyzer.getFile(ffolder,'gygi3_complex_total_abundance_difference.tsv')
		abu_data.to_csv(folder + 'data/figure5a_gygi3_complex_total_abundance_difference.tsv.gz',
						sep = '\t', compression = 'gzip')

		diet_abu_data = DataFrameAnalyzer.getFile(ffolder,'gygi3_complex_diet_total_abundance_difference.tsv')
		diet_abu_data.to_csv(folder + 'data/figure5a_gygi3_complex_diet_total_abundance_difference.tsv.gz',
							 sep = '\t', compression = 'gzip')				

	@staticmethod
	def get_complex_dictionary(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		complexDict = DataFrameAnalyzer.read_pickle(folder + 'data/complex_dictionary.pkl')
		return complexDict

	@staticmethod
	def get_complex_abundance_data(cov_sig_dict, num_dict, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		abu_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/figure5a_gygi3_complex_diet_total_abundance_difference.tsv.gz')
		key_list = list(set(abu_data.index).intersection(set(cov_sig_dict.keys())))

		cohen_list = list()
		tt_pval_list  = list()
		complex_list = list()
		stable_fractions = list()
		variable_fractions = list()
		for key in key_list:
			num = num_dict[key]
			complexID = key.split(':')[0]
			gold = complexDict[complexID]['goldComplex'][0]
			if gold=='yes' and num>=4:
				coverage = cov_sig_dict[key]
				cohen_list.append(abu_data.loc[key]['cohen_distance'])
				tt_pval_list.append(abu_data.loc[key]['ttest_pval'])
				complex_list.append(':'.join(key.split(':')[1:]))
				stable_fractions.append(100-coverage)
				variable_fractions.append(coverage)
			if complexID.find('tRNA splicing')!=-1:
				coverage = cov_sig_dict[key]
				cohen_list.append(abu_data.loc[key]['cohen_distance'])
				tt_pval_list.append(abu_data.loc[key]['ttest_pval'])
				complex_list.append(':'.join(key.split(':')[1:]))
				stable_fractions.append(100-coverage)
				variable_fractions.append(coverage)

		cohen_list,complex_list,tt_pval_list,stable_fractions, variable_fractions = utilsFacade.sort_multiple_lists([cohen_list, complex_list,tt_pval_list, stable_fractions, variable_fractions], reverse = True)
		tt_pvalCorrs = statsFacade.correct_pvalues(tt_pval_list)

		#gather numbers
		print(scipy.stats.spearmanr(cohen_list,variable_fractions))

		tt1 = 'bla'
		for idx,tt in enumerate(tt_pvalCorrs):
			if tt>=0.01 and tt1=='bla':
				tt1 = cohen_list[idx]
			if tt1!='bla' and tt<=0.01:
				tt2 = cohen_list[idx]
				break

		df = {'complex':complex_list, 'cohen':cohen_list, 'tt_pval':tt_pval_list,
			  'stable':stable_fractions, 'variable':variable_fractions,
			  'tt_pvalCorr':tt_pvalCorrs, 'tt1':[tt1]*len(complex_list),
			  'tt2':[tt2]*len(complex_list)}
		df = pd.DataFrame(df)
		df.to_csv(folder + 'data/figure5a_bigPicture_underlying_data.tsv.gz',
				  sep = '\t', compression = 'gzip')
		'''
		df.to_csv(folder + 'data/figure5a_bigPicture_diet_underlying_data.tsv.gz',
				  sep = '\t', compression = 'gzip')

		df = DataFrameAnalyzer.open_in_chunks(folder, 'data/figure5a_bigPicture_diet_underlying_data.tsv.gz')
		'''
		return df

	@staticmethod
	def prepare_data_for_second_graph(**kwargs):
		data, quant_data, stoch_data = figure5a.get_limma_data(folder = folder)
		cov_sig_dict, num_dict = figure5a.get_stoichiometric_hits_per_complex_dict(folder = folder)
		complexDict = figure5a.get_complex_dictionary(folder = folder)
		df = figure5a.get_complex_abundance_data(cov_sig_dict, num_dict, folder = folder)

	@staticmethod
	def figure5a_overview_stoichiometry(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder,'data/figure5a_bigPicture_underlying_data.tsv.gz')
		complex_list = list(df['complex'])
		cohen_list = list(df['cohen'])
		tt_pval_list = list(df['tt_pval'])
		stable_fractions = list(df['stable'])
		variable_fractions = list(df['variable'])
		tt_pvalCorrs = list(df['tt_pvalCorr'])
		tt1 = list(df.tt1)[0]
		tt2 = list(df.tt2)[0]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:5,0:])
		ind = np.arange(len(complex_list))
		width = 0.85
		ax.axhline(0, color = 'k')
		ax.axhline(0.5,linestyle = '--', color = 'k')
		ax.axhline(-0.5,linestyle = '--', color = 'k')
		ax.axhline(1.0,linestyle = '--', color = 'k')
		ax.axhline(-1.0,linestyle = '--', color = 'k')
		ax.axhline(1.5,linestyle = '--', color = 'k')
		ax.axhline(-1.5,linestyle = '--', color = 'k')
		ax.axhline(tt1, color = 'red')
		ax.axhline(tt2, color = 'red')
		colors = list()
		for p,pval in enumerate(tt_pval_list):
			if str(pval)=='nan':
				colors.append('grey')
			else:
				if cohen_list[p]>=tt1:
					colors.append('purple')
				elif cohen_list[p]<=tt2:
					colors.append('lightgreen')
				else:
					colors.append('grey')
		rects = ax.bar(ind, cohen_list, width, color=colors,
			edgecolor = 'white')
		ax.set_ylim(-2,2)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[5:7,0:])
		rects1 = ax.bar(ind, stable_fractions, width, color='darkblue',
						edgecolor = 'white')
		rects2 = ax.bar(ind, variable_fractions, width, color='red',
						bottom =np.array(stable_fractions),
						edgecolor = 'white')
		ax.set_ylim(0,100)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		plt.xticks(list(utilsFacade.frange(0.5,len(complex_list)+0.5,1)))
		ax.set_xticklabels(complex_list,rotation = 90, fontsize = 7)
		plt.savefig(folder + 'figures/fig5a_abundance_stoichiometry_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5a_overview_stoichiometry_diet(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		df = DataFrameAnalyzer.open_in_chunks(folder,'data/figure5a_bigPicture_diet_underlying_data.tsv.gz')
		complex_list = list(df['complex'])
		cohen_list = list(df['cohen'])
		tt_pval_list = list(df['tt_pval'])
		stable_fractions = list(df['stable'])
		variable_fractions = list(df['variable'])
		tt_pvalCorrs = list(df['tt_pvalCorr'])
		tt1 = list(df.tt1)[0]
		tt2 = list(df.tt2)[0]

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:5,0:])
		ind = np.arange(len(complex_list))
		width = 0.85
		ax.axhline(0, color = 'k')
		ax.axhline(0.5,linestyle = '--', color = 'k')
		ax.axhline(-0.5,linestyle = '--', color = 'k')
		ax.axhline(1.0,linestyle = '--', color = 'k')
		ax.axhline(-1.0,linestyle = '--', color = 'k')
		ax.axhline(1.5,linestyle = '--', color = 'k')
		ax.axhline(-1.5,linestyle = '--', color = 'k')
		ax.axhline(tt1, color = 'red')
		ax.axhline(tt2, color = 'red')
		colors = list()
		for p,pval in enumerate(tt_pval_list):
			if str(pval)=='nan':
				colors.append('grey')
			else:
				if cohen_list[p]>=tt1:
					colors.append('purple')
				elif cohen_list[p]<=tt2:
					colors.append('lightgreen')
				else:
					colors.append('grey')
		rects = ax.bar(ind, cohen_list, width, color=colors,
			edgecolor = 'white')
		ax.set_ylim(-2,2)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[5:7,0:])
		rects1 = ax.bar(ind, stable_fractions, width, color='darkblue',
						edgecolor = 'white')
		rects2 = ax.bar(ind, variable_fractions, width, color='red',
						bottom =np.array(stable_fractions),
						edgecolor = 'white')
		ax.set_ylim(0,100)
		ax.set_xlim(-0.1, len(complex_list)+0.1)
		plt.xticks(list(utilsFacade.frange(0.5,len(complex_list)+0.5,1)))
		ax.set_xticklabels(complex_list,rotation = 90, fontsize = 7)
		plt.savefig(folder + 'figures/fig5a_abundance_diet_stoichiometry_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure5b:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5B: main_figure5b_volcanoPlots')
		figure5b.main_figure5b_volcanoPlots()
		print('FIGURE5B: main_figure5b_boxPlots')
		figure5b.main_figure5b_boxPlots()

	@staticmethod
	def get_data(name,**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data[data.analysis_type=='stoch_male-female']
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains(name)]

		pval_list = -np.log10(np.array(data['pval.adj']))
		fc_list = np.array(data['logFC'])
		complex_pval_list = -np.log10(np.array(complex_data['pval.adj']))
		complex_fc_list = np.array(complex_data['logFC'])
		complex_label_list = list(complex_data.index)
		complex_colors = ['red' if item<=0.01 else 'darkblue' for item in list(complex_data['pval.adj'])]
		return {'pval':pval_list, 'fc': fc_list, 'complex_pval': complex_pval_list,
				'complex_fc':complex_fc_list, 'complex_label': complex_label_list,
				'complex_color':complex_colors}

	@staticmethod
	def figure5b_volcanoPlot_retromer_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('retromer')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_retromer_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cop1_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('HC2402:COPI')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cop1_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_ste5MAPK_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('MAPK')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black', s = 30, 
				   color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_mapkComplex_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cohesin_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('Cohesin')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cohesin_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_volcanoPlot_cop2_mfComparison(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data('HC663:COPII')
		pval_list = data_dict['pval']
		fc_list = data_dict['fc']
		complex_pval_list = data_dict['complex_pval']
		complex_fc_list = data_dict['complex_fc']
		complex_label_list = data_dict['complex_label']
		complex_colors = data_dict['complex_color']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		ax.scatter(fc_list, pval_list, edgecolor = 'black',
				   s = 30, color = 'white', alpha = 0.2)
		ax.scatter(complex_fc_list, complex_pval_list,
				   edgecolor = 'black', s = 50, color = complex_colors)
		for count, label in enumerate(complex_label_list):
			x,y = complex_fc_list[count], complex_pval_list[count]
			ax.annotate(label, xy = (x,y), color = complex_colors[count])
		ax.set_xlabel('logFC (male/female)')
		ax.set_ylabel('p.value[-log10]')
		ax.set_xlim(-1.25,1.25)
		ax.set_ylim(-0.01, 35)
		plt.savefig(folder + 'figures/fig5b_cop2_volcano_mf_comparison.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def main_figure5b_volcanoPlots(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('volcano_plot_stoichiometry:COPI')
		figure5b.figure5b_volcanoPlot_cop1_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:COPII')
		figure5b.figure5b_volcanoPlot_cop2_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:Cohesin')
		figure5b.figure5b_volcanoPlot_cohesin_mfComparison(folder = output_folder)
		print('volcano_plot_stoichiometry:retromer complex')
		figure5b.figure5b_volcanoPlot_retromer_mfComparison(folder = output_folder)

	@staticmethod
	def main_figure5b_boxPlots(**kwargs):
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('boxplot:COPI')
		figure5b.figure5b_cop1_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:COPII')
		figure5b.figure5b_cop2_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:retromer complex')
		figure5b.figure5b_retromer_boxplot_logFC_male_female(folder = output_folder)
		print('boxplot:Cohesin')
		figure5b.figure5b_cohesin_boxplot_logFC_male_female(folder = output_folder)

	@staticmethod
	def get_data_boxplot(name,**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		filename = 'data/gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data.drop_duplicates()
		data = data.drop(['C9','C2','Psd3'], axis = 0)
		complex_data = data[data.complex_search.str.contains(name)]
		male_data = complex_data[complex_data.analysis_type=='male']
		female_data = complex_data[complex_data.analysis_type=='female']
		stoch_maleFemale_data = complex_data[complex_data.analysis_type=='stoch_male-female']
		pval_dict = stoch_maleFemale_data['pval.adj'].to_dict()

		wpc_data = DataFrameAnalyzer.open_in_chunks(folder, 'data/complex_filtered_stoch_gygi3.tsv.gz')
		wpc_data = wpc_data[wpc_data.complex_search.str.contains(name)]
		quant_list = utilsFacade.filtering(list(wpc_data.columns),'quant_')
		male_list = utilsFacade.filtering(quant_list, 'M')
		female_list = utilsFacade.filtering(quant_list, 'F')
		male_data = wpc_data[male_list].T
		female_data = wpc_data[female_list].T

		protein_list = list(set(male_data.columns).intersection(set(female_data.columns)))
		pval_list = [pval_dict[protein] for protein in protein_list]
		pval_list, protein_list = zip(*sorted(zip(pval_list, protein_list)))
		data_list = list()
		color_list = list()
		sig_protein_list = list()
		positions = [0.5]
		for protein in protein_list:
			if pval_dict[protein]<=0.01:
				try:
					data_list.append(utilsFacade.finite(list(male_data[protein])))
					data_list.append(utilsFacade.finite(list(female_data[protein])))
					color_list.append('purple')
					color_list.append('green')
					sig_protein_list.append(protein)
					sig_protein_list.append(protein)
					positions.append(positions[-1]+0.4)
					positions.append(positions[-1]+0.6)
				except:
					data_list.append(utilsFacade.finite(list(male_data[protein].T.iloc[0].T)))
					data_list.append(utilsFacade.finite(list(female_data[protein].T.iloc[0].T)))
					color_list.append('purple')
					color_list.append('green')
					sig_protein_list.append(protein)
					sig_protein_list.append(protein)
					positions.append(positions[-1]+0.4)
					positions.append(positions[-1]+0.6)					

		male_median_data = male_data.T.median().T
		female_median_data = female_data.T.median().T
		data_list.append(list(male_median_data))
		data_list.append(list(female_median_data))
		color_list.append('grey')
		color_list.append('grey')
		if name=='HC2402:COPI':
			sig_protein_list.append('COPI-male')
			sig_protein_list.append('COPI-female')
		elif name == 'HC663:COPII':
			sig_protein_list.append('COPII-male')
			sig_protein_list.append('COPII-female')
		elif name=='MAPK':
			sig_protein_list.append('MAPK-male')
			sig_protein_list.append('MAPK-female')
		elif name == 'Cohesin':
			sig_protein_list.append('Cohesin-male')
			sig_protein_list.append('Cohesin-female')			
		positions.append(positions[-1]+0.5)
		return {'data':data_list, 'color': color_list,
				'sig_protein':sig_protein_list, 'positions':positions}

	@staticmethod
	def figure5b_retromer_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('retromer')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
			 			widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPI complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_retromer_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def figure5b_cop1_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('HC2402:COPI')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
			 			widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPI complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cop1_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)
	
	@staticmethod
	def figure5b_cop2_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('HC663:COPII')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('COPII complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cop2_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_ste5MAPK_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('MAPK')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('MAPK complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_mapk_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def figure5b_cohesin_boxplot_logFC_male_female(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure5b.get_data_boxplot('Cohesin')
		data_list = data_dict['data']
		color_list = data_dict['color']
		sig_protein_list = data_dict['sig_protein']
		positions = data_dict['positions']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (5,5))
		ax = fig.add_subplot(111)
		bp = ax.boxplot(data_list,notch=0,sym="",vert=1,patch_artist=True,
						widths=[0.4]*len(data_list), positions = positions)
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
			sub_group=data_list[i]
			x = numpy.random.normal(positions[i], 0.04, size=len(data_list[i]))
			ax.scatter(x,sub_group,color='white', alpha=0.5,edgecolor="black",s=5)
		ax.set_title('Cohesin complex')
		ax.set_xticklabels(sig_protein_list, rotation = 90)
		ax.set_ylim(-1,1)
		ax.set_ylabel('normalized abundances')
		plt.savefig(folder + 'figures/fig5b_cohesin_boxplot_males_females.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure5c_addendum:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE5C: plot_BP_go_for_main_figure')
		figure5c_addendum.plot_BP_go_for_main_figure()
		print('FIGURE5C: plot_CC_go_for_main_figure')
		figure5c_addendum.plot_CC_go_for_main_figure()

	@staticmethod
	def prepare_input():
		folder = '/g/scb2/bork/romanov/wpc/'
		fileName = 'gygi3_complex_mf_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		data = data.drop_duplicates()
		data.index = data.complex_search + '::' +data.index

		data = data[data['pval.adj']<=0.1]
		quant_data = data[data.analysis_type=='quant_male-female']
		stoch_data = data[data.analysis_type=='stoch_male-female']

		quant_indices = list(set(quant_data.index).difference(set(stoch_data.index)))
		stoch_indices = list(set(stoch_data.index).difference(set(quant_data.index)))

		quant_genes = list(set([item.split('::')[1] for item in quant_indices]))
		stoch_genes = list(set([item.split('::')[1] for item in stoch_indices]))
		quant_entrezgenes = Mapper(quant_genes, input = 'symbol', output = 'entrezgene')
		stoch_entrezgenes = Mapper(stoch_genes, input = 'symbol', output = 'entrezgene')
		quant_entrezgenes = map(int,utilsFacade.finite(list(quant_entrezgenes.trans_df['entrezgene'])))
		stoch_entrezgenes = map(int,utilsFacade.finite(list(stoch_entrezgenes.trans_df['entrezgene'])))
		o = open(folder + 'gender_quant_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,quant_entrezgenes))
		o.write(exportText)
		o.close()
		o = open(folder + 'gender_stoch_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,stoch_entrezgenes))
		o.write(exportText)
		o.close()

		fileName = 'gygi3_complex_hs_merged_perGeneRun.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fileName)
		data = data.drop_duplicates()
		data.index = data.complex_search + '::' +data.index
		data = data[data['pval.adj']<=0.1]
		quant_data = data[data.analysis_type=='quant_highfat-chow']
		stoch_data = data[data.analysis_type=='stoch_highfat-chow']

		quant_indices = list(set(quant_data.index).difference(set(stoch_data.index)))
		stoch_indices = list(set(stoch_data.index).difference(set(quant_data.index)))

		quant_genes = list(set([item.split('::')[1] for item in quant_indices]))
		stoch_genes = list(set([item.split('::')[1] for item in stoch_indices]))
		quant_entrezgenes = Mapper(quant_genes, input = 'symbol', output = 'entrezgene')
		stoch_entrezgenes = Mapper(stoch_genes, input = 'symbol', output = 'entrezgene')
		quant_entrezgenes = map(int,utilsFacade.finite(list(quant_entrezgenes.trans_df['entrezgene'])))
		stoch_entrezgenes = map(int,utilsFacade.finite(list(stoch_entrezgenes.trans_df['entrezgene'])))
		o = open(folder + 'diet_quant_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,quant_entrezgenes))
		o.write(exportText)
		o.close()
		o = open(folder + 'diet_stoch_go_input_data.tsv','wb')
		exportText = '\n'.join(map(str,stoch_entrezgenes))
		o.write(exportText)
		o.close()

	@staticmethod
	def get_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		mf_quant_go_data = DataFrameAnalyzer.getFile(folder,'gender_quant_go_output.txt')
		mf_stoch_go_data = DataFrameAnalyzer.getFile(folder,'gender_stoch_go_output.txt')
		hs_quant_go_data = DataFrameAnalyzer.getFile(folder,'diet_quant_go_output.txt')
		hs_stoch_go_data = DataFrameAnalyzer.getFile(folder,'diet_stoch_go_output.txt')

		mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.FDR<=0.01]
		hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.FDR<=0.01]
		mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.FDR<=0.01]
		hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.FDR<=0.01]

		categories = ['GOTERM_BP_FAT','GOTERM_CC_FAT','GOTERM_MF_FAT']
		mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.isin(categories)]
		hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.isin(categories)]
		mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.isin(categories)]
		hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.isin(categories)]

		bp_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('BP')]
		bp_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('BP')]
		bp_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('BP')]
		bp_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('BP')]
		mf_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('MF')]
		mf_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('MF')]
		mf_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('MF')]
		mf_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('MF')]
		cc_mf_quant_go_data = mf_quant_go_data[mf_quant_go_data.index.str.contains('CC')]
		cc_hs_quant_go_data = hs_quant_go_data[hs_quant_go_data.index.str.contains('CC')]
		cc_mf_stoch_go_data = mf_stoch_go_data[mf_stoch_go_data.index.str.contains('CC')]
		cc_hs_stoch_go_data = hs_stoch_go_data[hs_stoch_go_data.index.str.contains('CC')]

		bp_mf_quant_go_data.index = pd.Series(list(bp_mf_quant_go_data.Term), index = bp_mf_quant_go_data.index)
		bp_hs_quant_go_data.index = pd.Series(list(bp_hs_quant_go_data.Term), index = bp_hs_quant_go_data.index)
		bp_mf_stoch_go_data.index = pd.Series(list(bp_mf_stoch_go_data.Term), index = bp_mf_stoch_go_data.index)
		bp_hs_stoch_go_data.index = pd.Series(list(bp_hs_stoch_go_data.Term), index = bp_hs_stoch_go_data.index)
		mf_mf_quant_go_data.index = pd.Series(list(mf_mf_quant_go_data.Term), index = mf_mf_quant_go_data.index)
		mf_hs_quant_go_data.index = pd.Series(list(mf_hs_quant_go_data.Term), index = mf_hs_quant_go_data.index)
		mf_mf_stoch_go_data.index = pd.Series(list(mf_mf_stoch_go_data.Term), index = mf_mf_stoch_go_data.index)
		mf_hs_stoch_go_data.index = pd.Series(list(mf_hs_stoch_go_data.Term), index = mf_hs_stoch_go_data.index)
		cc_mf_quant_go_data.index = pd.Series(list(cc_mf_quant_go_data.Term), index = cc_mf_quant_go_data.index)
		cc_hs_quant_go_data.index = pd.Series(list(cc_hs_quant_go_data.Term), index = cc_hs_quant_go_data.index)
		cc_mf_stoch_go_data.index = pd.Series(list(cc_mf_stoch_go_data.Term), index = cc_mf_stoch_go_data.index)
		cc_hs_stoch_go_data.index = pd.Series(list(cc_hs_stoch_go_data.Term), index = cc_hs_stoch_go_data.index)

		bp_mf_quant_fdr_dict = bp_mf_quant_go_data['FDR'].to_dict()
		bp_hs_quant_fdr_dict = bp_hs_quant_go_data['FDR'].to_dict()
		bp_mf_stoch_fdr_dict = bp_mf_stoch_go_data['FDR'].to_dict()
		bp_hs_stoch_fdr_dict = bp_hs_stoch_go_data['FDR'].to_dict()
		mf_mf_quant_fdr_dict = mf_mf_quant_go_data['FDR'].to_dict()
		mf_hs_quant_fdr_dict = mf_hs_quant_go_data['FDR'].to_dict()
		mf_mf_stoch_fdr_dict = mf_mf_stoch_go_data['FDR'].to_dict()
		mf_hs_stoch_fdr_dict = mf_hs_stoch_go_data['FDR'].to_dict()
		cc_mf_quant_fdr_dict = cc_mf_quant_go_data['FDR'].to_dict()
		cc_hs_quant_fdr_dict = cc_hs_quant_go_data['FDR'].to_dict()
		cc_mf_stoch_fdr_dict = cc_mf_stoch_go_data['FDR'].to_dict()
		cc_hs_stoch_fdr_dict = cc_hs_stoch_go_data['FDR'].to_dict()

		bp_mf_quant_size_dict = bp_mf_quant_go_data['Count'].to_dict()
		bp_hs_quant_size_dict = bp_hs_quant_go_data['Count'].to_dict()
		bp_mf_stoch_size_dict = bp_mf_stoch_go_data['Count'].to_dict()
		bp_hs_stoch_size_dict = bp_hs_stoch_go_data['Count'].to_dict()
		mf_mf_quant_size_dict = mf_mf_quant_go_data['Count'].to_dict()
		mf_hs_quant_size_dict = mf_hs_quant_go_data['Count'].to_dict()
		mf_mf_stoch_size_dict = mf_mf_stoch_go_data['Count'].to_dict()
		mf_hs_stoch_size_dict = mf_hs_stoch_go_data['Count'].to_dict()
		cc_mf_quant_size_dict = cc_mf_quant_go_data['Count'].to_dict()
		cc_hs_quant_size_dict = cc_hs_quant_go_data['Count'].to_dict()
		cc_mf_stoch_size_dict = cc_mf_stoch_go_data['Count'].to_dict()
		cc_hs_stoch_size_dict = cc_hs_stoch_go_data['Count'].to_dict()

		bp_mf_quant_fe_dict = bp_mf_quant_go_data['Fold Enrichment'].to_dict()
		bp_hs_quant_fe_dict = bp_hs_quant_go_data['Fold Enrichment'].to_dict()
		bp_mf_stoch_fe_dict = bp_mf_stoch_go_data['Fold Enrichment'].to_dict()
		bp_hs_stoch_fe_dict = bp_hs_stoch_go_data['Fold Enrichment'].to_dict()
		mf_mf_quant_fe_dict = mf_mf_quant_go_data['Fold Enrichment'].to_dict()
		mf_hs_quant_fe_dict = mf_hs_quant_go_data['Fold Enrichment'].to_dict()
		mf_mf_stoch_fe_dict = mf_mf_stoch_go_data['Fold Enrichment'].to_dict()
		mf_hs_stoch_fe_dict = mf_hs_stoch_go_data['Fold Enrichment'].to_dict()
		cc_mf_quant_fe_dict = cc_mf_quant_go_data['Fold Enrichment'].to_dict()
		cc_hs_quant_fe_dict = cc_hs_quant_go_data['Fold Enrichment'].to_dict()
		cc_mf_stoch_fe_dict = cc_mf_stoch_go_data['Fold Enrichment'].to_dict()
		cc_hs_stoch_fe_dict = cc_hs_stoch_go_data['Fold Enrichment'].to_dict()

		bp_key_list = [set(bp_mf_quant_fdr_dict.keys()), set(bp_hs_quant_fdr_dict.keys()),
					   set(bp_mf_stoch_fdr_dict.keys()), set(bp_hs_stoch_fdr_dict.keys())]
		bp_key_list = set.union(*bp_key_list)
		mf_key_list = [set(mf_mf_quant_fdr_dict.keys()), set(mf_hs_quant_fdr_dict.keys()),
					   set(mf_mf_stoch_fdr_dict.keys()), set(mf_hs_stoch_fdr_dict.keys())]
		mf_key_list = set.union(*mf_key_list)
		cc_key_list = [set(cc_mf_quant_fdr_dict.keys()), set(cc_hs_quant_fdr_dict.keys()),
					   set(cc_mf_stoch_fdr_dict.keys()), set(cc_hs_stoch_fdr_dict.keys())]
		cc_key_list = set.union(*cc_key_list)

		fe_dict = dict()
		fdr_dict = dict()
		size_dict = dict()
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_fdr_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_fdr_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_fdr_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_fdr_dict.get(key,np.nan)
			fdr_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fdr_dict[key]['gender_quant'] = get_male_quant
			fdr_dict[key]['diet_quant'] = get_female_quant
			fdr_dict[key]['gender_stoch'] = get_male_stoch
			fdr_dict[key]['diet_stoch'] = get_female_stoch
		bp_fdr_data = pd.DataFrame(fdr_dict)
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_fe_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_fe_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_fe_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_fe_dict.get(key,np.nan)
			fe_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fe_dict[key]['gender_quant'] = get_male_quant
			fe_dict[key]['diet_quant'] = get_female_quant
			fe_dict[key]['gender_stoch'] = get_male_stoch
			fe_dict[key]['diet_stoch'] = get_female_stoch
		bp_fe_data = pd.DataFrame(fe_dict)
		for key in bp_key_list:
			get_male_quant = bp_mf_quant_size_dict.get(key,np.nan)
			get_female_quant = bp_hs_quant_size_dict.get(key,np.nan)
			get_male_stoch = bp_mf_stoch_size_dict.get(key,np.nan)
			get_female_stoch = bp_hs_stoch_size_dict.get(key,np.nan)
			size_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			size_dict[key]['gender_quant'] = get_male_quant
			size_dict[key]['diet_quant'] = get_female_quant
			size_dict[key]['gender_stoch'] = get_male_stoch
			size_dict[key]['diet_stoch'] = get_female_stoch
		bp_size_data = pd.DataFrame(size_dict)

		fe_dict = dict()
		fdr_dict = dict()
		size_dict = dict()
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_fdr_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_fdr_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_fdr_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_fdr_dict.get(key,np.nan)
			fdr_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fdr_dict[key]['gender_quant'] = get_male_quant
			fdr_dict[key]['diet_quant'] = get_female_quant
			fdr_dict[key]['gender_stoch'] = get_male_stoch
			fdr_dict[key]['diet_stoch'] = get_female_stoch
		cc_fdr_data = pd.DataFrame(fdr_dict)
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_fe_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_fe_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_fe_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_fe_dict.get(key,np.nan)
			fe_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			fe_dict[key]['gender_quant'] = get_male_quant
			fe_dict[key]['diet_quant'] = get_female_quant
			fe_dict[key]['gender_stoch'] = get_male_stoch
			fe_dict[key]['diet_stoch'] = get_female_stoch
		cc_fe_data = pd.DataFrame(fe_dict)
		for key in cc_key_list:
			get_male_quant = cc_mf_quant_size_dict.get(key,np.nan)
			get_female_quant = cc_hs_quant_size_dict.get(key,np.nan)
			get_male_stoch = cc_mf_stoch_size_dict.get(key,np.nan)
			get_female_stoch = cc_hs_stoch_size_dict.get(key,np.nan)
			size_dict[key] = dict((e1,list()) for e1 in ['gender_quant','diet_quant','gender_stoch','diet_stoch'])
			size_dict[key]['gender_quant'] = get_male_quant
			size_dict[key]['diet_quant'] = get_female_quant
			size_dict[key]['gender_stoch'] = get_male_stoch
			size_dict[key]['diet_stoch'] = get_female_stoch
		cc_size_data = pd.DataFrame(size_dict)

		bp_fdr_data.to_csv(folder + 'BP_go_output_fdrData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		bp_fe_data.to_csv(folder + 'BP_go_output_feData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		bp_size_data.to_csv(folder + 'BP_go_output_sizeData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_fdr_data.to_csv(folder + 'CC_go_output_fdrData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_fe_data.to_csv(folder + 'CC_go_output_feData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')
		cc_size_data.to_csv(folder + 'CC_go_output_sizeData_gender_diet_difference.tsv.gz', sep = '\t', compression = 'gzip')

	@staticmethod
	def plot_BP_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['gender_quant','diet_quant']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['gender_stoch','diet_stoch']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['gender_quant','diet_quant']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['gender_stoch','diet_stoch']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['gender_quant','diet_quant']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['gender_stoch','diet_stoch']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('gender_quant', ascending = False)
		q1 = quant_fe_data[quant_fe_data['gender_quant']>0]
		q2 = quant_fe_data[quant_fe_data['gender_quant']<=0]
		q2 = q2.sort_values('diet_quant')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('gender_stoch', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data['gender_stoch']>0]
		q2 = stoch_fe_data[stoch_fe_data['gender_stoch']<=0]
		q2 = q2.sort_values('diet_stoch')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq)
		female_my_cmap = colorFacade.make_colormap(female_seq)

		male_quant_fe_list = list(quant_fe_data['gender_quant'])
		female_quant_fe_list = list(quant_fe_data['diet_quant'])
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data['gender_quant']))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data['diet_quant']))
		male_stoch_fe_list = list(stoch_fe_data['gender_stoch'])
		female_stoch_fe_list = list(stoch_fe_data['diet_stoch'])
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data['gender_stoch']))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data['diet_stoch']))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'BP_abundances_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(folder + 'BP_stoichiometry_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)
	
	@staticmethod
	def plot_CC_go_output():
		folder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'CC_go_output_sizeData_gender_diet_difference.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		quant_fdr_data = fdr_data[['gender_quant','diet_quant']]
		quant_fdr_data = quant_fdr_data.dropna(thresh=1)
		stoch_fdr_data = fdr_data[['gender_stoch','diet_stoch']]
		stoch_fdr_data = stoch_fdr_data.dropna(thresh=1)
		quant_size_data = size_data[['gender_quant','diet_quant']]
		quant_size_data = quant_size_data.dropna(thresh=1)
		stoch_size_data = size_data[['gender_stoch','diet_stoch']]
		stoch_size_data = stoch_size_data.dropna(thresh=1)
		quant_fe_data = fe_data[['gender_quant','diet_quant']]
		quant_fe_data = quant_fe_data.dropna(thresh=1)
		stoch_fe_data = fe_data[['gender_stoch','diet_stoch']]
		stoch_fe_data = stoch_fe_data.dropna(thresh=1)

		quant_fe_data = quant_fe_data.replace(np.nan, 0)
		stoch_fe_data = stoch_fe_data.replace(np.nan,0)
		quant_fdr_data = quant_fdr_data.replace(np.nan, 0)
		stoch_fdr_data = stoch_fdr_data.replace(np.nan,0)

		quant_fe_data = quant_fe_data.sort_values('gender_quant', ascending = False)
		q1 = quant_fe_data[quant_fe_data['gender_quant']>0]
		q2 = quant_fe_data[quant_fe_data['gender_quant']<=0]
		q2 = q2.sort_values('diet_quant')
		quant_fe_data = pd.concat([q1,q2])

		stoch_fe_data = stoch_fe_data.sort_values('gender_stoch', ascending = False)
		q1 = stoch_fe_data[stoch_fe_data['gender_stoch']>0]
		q2 = stoch_fe_data[stoch_fe_data['gender_stoch']<=0]
		q2 = q2.sort_values('diet_stoch')
		stoch_fe_data = pd.concat([q1,q2])

		stoch_fdr_data = stoch_fdr_data.T[list(stoch_fe_data.index)].T
		quant_fdr_data = quant_fdr_data.T[list(quant_fe_data.index)].T

		import matplotlib.colors as mcolors
		c = mcolors.ColorConverter().to_rgb
		male_seq = [c('pink'),c('pink'),c('magenta'),c('magenta'),c('purple'),c('purple')]
		female_seq = [c('lightgreen'),c('lightgreen'), c('green'),c('green'), c('darkgreen'),c('darkgreen')]
		male_my_cmap = colorFacade.make_colormap(male_seq)
		female_my_cmap = colorFacade.make_colormap(female_seq)

		male_quant_fe_list = list(quant_fe_data['gender_quant'])
		female_quant_fe_list = list(quant_fe_data['diet_quant'])
		quant_label_list = [item.split('~')[1] for item in list(quant_fe_data.index)]
		sc, quant_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(quant_fe_data['gender_quant']))
		sc, quant_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(quant_fe_data['diet_quant']))
		male_stoch_fe_list = list(stoch_fe_data['gender_stoch'])
		female_stoch_fe_list = list(stoch_fe_data['diet_stoch'])
		stoch_label_list = [item.split('~')[1] for item in list(stoch_fe_data.index)]
		sc, stoch_male_color_list = colorFacade.get_specific_color_gradient(male_my_cmap, np.array(stoch_fe_data['gender_stoch']))
		sc, stoch_female_color_list = colorFacade.get_specific_color_gradient(female_my_cmap, np.array(stoch_fe_data['diet_stoch']))
		
		quant_ind = np.arange(len(quant_fe_data))
		stoch_ind = np.arange(len(stoch_fe_data))

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		ax = fig.add_subplot(111)

		width = 0.85
		colors = [sc.to_rgba(item) for item in male_quant_fe_list]
		rects = ax.bar(quant_ind, male_quant_fe_list, width, color = quant_male_color_list, edgecolor = 'white')
		rects = ax.bar(quant_ind, (-1)*np.array(female_quant_fe_list), width, color = quant_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(quant_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(quant_label_list)+0.5,1)))
		ax.set_xticklabels(quant_label_list, rotation = 90)
		plt.savefig(folder + 'CC_abundances_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (7,5))
		ax = fig.add_subplot(111)
		width = 0.85
		colors = [sc.to_rgba(item) for item in male_stoch_fe_list]
		rects = ax.bar(stoch_ind, male_stoch_fe_list, width, color = stoch_male_color_list, edgecolor = 'white')
		rects = ax.bar(stoch_ind, (-1)*np.array(female_stoch_fe_list), width, color = stoch_female_color_list, edgecolor = 'white')
		ax.set_xlim(0,len(stoch_ind))
		plt.xticks(list(utilsFacade.frange(0.5,len(stoch_label_list)+0.5,1)))
		ax.set_xticklabels(stoch_label_list, rotation = 90)
		plt.savefig(folder + 'CC_stoichiometry_gender_diet_barplot.pdf', bbox_inches = 'tight', dpi = 300)

	@staticmethod
	def export_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		ffolder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				   'BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  'BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  	'BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data.to_csv(folder + 'data/fig5c_BP_go_output_fdrData_gender_diet_difference.tsv.gz',
						sep = '\t', compression = 'gzip')
		fe_data.to_csv(folder + 'data/fig5c_BP_go_output_feData_gender_diet_difference.tsv.gz',
					   sep = '\t', compression = 'gzip')
		size_data.to_csv(folder + 'data/fig5c_BP_go_output_sizeData_gender_diet_difference.tsv.gz',
						 sep = '\t', compression = 'gzip')


		ffolder = '/g/scb2/bork/romanov/wpc/'
		fdr_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				   'CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  'CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(ffolder,
				  	'CC_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data.to_csv(folder + 'data/fig5c_CC_go_output_fdrData_gender_diet_difference.tsv.gz',
						sep = '\t', compression = 'gzip')
		fe_data.to_csv(folder + 'data/fig5c_CC_go_output_feData_gender_diet_difference.tsv.gz',
					   sep = '\t', compression = 'gzip')
		size_data.to_csv(folder + 'data/fig5c_CC_go_output_sizeData_gender_diet_difference.tsv.gz',
						 sep = '\t', compression = 'gzip')

	@staticmethod
	def plot_BP_go_for_main_figure(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/fig5c_BP_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/fig5c_BP_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/fig5c_BP_go_output_sizeData_gender_diet_difference.tsv.gz')

		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		size_data = size_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.dropna(thresh=1)
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		fe_data = fe_data.dropna(thresh=1)

		fe_data = fe_data.replace(np.nan, 0)
		size_data = size_data.replace(np.nan,0)

		fe_data, protList = utilsFacade.recluster_matrix_only_rows(fe_data, method = 'complete')
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.T[list(fe_data.index)].T
		sc_gender,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greens, np.arange(0,20,0.001))
		sc_diet,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys, np.arange(0,20,0.001))


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,10)

		ax = plt.subplot(gs[0:8,0:8])
		count=0
		for col in ['gender_quant','diet_quant','gender_stoch','diet_stoch']:
			values = np.array(fe_data[col])
			sizes = np.array(size_data[col])*30
			if col.startswith('gender')==True:
				colors = [sc_gender.to_rgba(item) for item in values]
			else:
				colors = [sc_diet.to_rgba(item) for item in values]
			ax.scatter(list(xrange(len(values))),[count]*len(values), s = sizes, 
				edgecolor = 'black', color = colors, alpha = 1)
			count+=1
		plt.yticks(list(xrange(len(size_data.columns))))
		ax.set_yticklabels(list(size_data.columns))
		plt.xticks(list(xrange(len(size_data))))
		ax.set_xticklabels([item.split('~')[1] for item in list(size_data.index)], rotation = 90)
		ax.set_xlim(-1, len(size_data))

		ax = plt.subplot(gs[0:8,8:9])
		ax.axis('off')
		cbar_gender = fig.colorbar(sc_gender)
		ax = plt.subplot(gs[0:8,9:])
		ax.axis('off')
		cbar_diet = fig.colorbar(sc_diet)

		ax = plt.subplot(gs[8:,8:])
		ax.axis('off')
		lh, lt = plottingFacade.get_legendHandles([0,5,10,20],30, edgecolor = 'k', color = 'white')
		ax.legend(lh,lt, loc = 'center',ncol=4, fontsize = 12)
		plt.savefig(output_folder + 'figures/fig5c_go_data.pdf',
					bbox_inches = 'tight', dpi = 400)

	@staticmethod
	def plot_CC_go_for_main_figure(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		fdr_data = DataFrameAnalyzer.open_in_chunks(folder,
				   'data/fig5c_CC_go_output_fdrData_gender_diet_difference.tsv.gz')
		fe_data = DataFrameAnalyzer.open_in_chunks(folder,
				  'data/fig5c_CC_go_output_feData_gender_diet_difference.tsv.gz')
		size_data = DataFrameAnalyzer.open_in_chunks(folder,
				  	'data/fig5c_CC_go_output_sizeData_gender_diet_difference.tsv.gz')
		fdr_data = fdr_data.T
		fe_data = fe_data.T
		size_data = size_data.T
		size_data = size_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.dropna(thresh=1)
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		fe_data = fe_data.dropna(thresh=1)

		fe_data = fe_data.replace(np.nan, 0)
		size_data = size_data.replace(np.nan,0)

		fe_data, protList = utilsFacade.recluster_matrix_only_rows(fe_data, method = 'complete')
		fe_data = fe_data[['gender_quant','diet_quant','gender_stoch','diet_stoch']]
		size_data = size_data.T[list(fe_data.index)].T
		sc_gender,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greens, np.arange(0,20,0.001))
		sc_diet,color_list = colorFacade.get_specific_color_gradient(plt.cm.Greys, np.arange(0,20,0.001))



		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = True

		plt.clf()
		fig = plt.figure(figsize = (15,5))
		gs = gridspec.GridSpec(10,10)

		ax = plt.subplot(gs[0:8,0:8])
		count=0
		for col in ['gender_quant','diet_quant','gender_stoch','diet_stoch']:
			values = np.array(fe_data[col])
			sizes = np.array(size_data[col])*30
			if col.startswith('gender')==True:
				colors = [sc_gender.to_rgba(item) for item in values]
			else:
				colors = [sc_diet.to_rgba(item) for item in values]
			ax.scatter(list(xrange(len(values))),[count]*len(values), s = sizes, 
				edgecolor = 'black', color = colors, alpha = 1)
			count+=1
		plt.yticks(list(xrange(len(size_data.columns))))
		ax.set_yticklabels(list(size_data.columns))
		plt.xticks(list(xrange(len(size_data))))
		ax.set_xticklabels([item.split('~')[1] for item in list(size_data.index)], rotation = 90)
		ax.set_xlim(-1, len(size_data))

		ax = plt.subplot(gs[0:8,8:9])
		ax.axis('off')
		cbar_gender = fig.colorbar(sc_gender)
		ax = plt.subplot(gs[0:8,9:])
		ax.axis('off')
		cbar_diet = fig.colorbar(sc_diet)

		ax = plt.subplot(gs[8:,8:])
		ax.axis('off')
		lh, lt = plottingFacade.get_legendHandles([0,5,10,20],30, edgecolor = 'k', color = 'white')
		ax.legend(lh,lt, loc = 'center',ncol=4, fontsize = 12)
		plt.savefig(output_folder + 'figures/fig5c_CC_supp_go_data.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure6a:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE6A: get_considered_pathways')
		considered_pathways = figure6a.get_considered_pathways(folder = folder)
		print('FIGURE6A: plot')
		figure6a.plot(considered_pathways, folder = folder)

	@staticmethod
	def load_multivariate_module_specific_data(dataset, module_type, datatype, covariate, **kwargs):
		n_subunit = kwargs.get('n_subunit',0)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_' + dataset + '/' + module_type + '/'
		joint_name = '_'.join([dataset, module_type, datatype,covariate])
		filename = joint_name + '_data_all_multi_covariate_moduleSpecific_empiricalFDR.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data[data['n.subunits']>=n_subunit]
		return data	

	@staticmethod
	def load_multivariate_combined_module_specific_data(dataset, module_type, datatype, **kwargs):
		n_subunit = kwargs.get('n_subunit',0)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_' + dataset + '/' + module_type + '/'
		joint_name = '_'.join([dataset, module_type, datatype])
		filename = joint_name + '_data_all_multi_covariate_moduleSpecific_COMBINEDEFFECT.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, filename)
		data = data[data['n.subunits']>=n_subunit]
		return data

	@staticmethod
	def load_combined_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		comb_all_quant = data[data.covariates=='sex_diet']

		comb_mquant_complex = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							  'complex','quant', n_subunit = su)
		comb_mquant_corum = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							'corum','quant', n_subunit = su)
		comb_mstoch_complex = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							  'complex','stoichiometry', n_subunit = su)
		comb_mstoch_corum = figure6a.load_multivariate_combined_module_specific_data('gygi3',
							'corum','stoichiometry', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mquant_pathway = comb_mquant_pathway[comb_mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mstoch_pathway = comb_mstoch_pathway[comb_mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_loc = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mquant_loc = comb_mquant_loc[comb_mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder,filename)
		comb_mstoch_loc = comb_mstoch_loc[comb_mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_COMBINEDEFFECT_woCORUM.tsv.gz'
		comb_mquant_string = DataFrameAnalyzer.open_in_chunks(folder,filename)

		comb_dict = {'complex':{'quant':comb_mquant_complex,'stoichiometry':comb_mstoch_complex},
					 'corum':{'quant':comb_mquant_corum,'stoichiometry':comb_mstoch_corum},
					 'pathway':{'quant':comb_mquant_pathway,'stoichiometry':comb_mstoch_pathway},
					 'loc':{'quant':comb_mquant_loc,'stoichiometry':comb_mstoch_loc},
					 'string':{'quant':comb_mquant_string},
					 'all':{'quant':comb_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(comb_dict, output_folder + 'combined_dictionary.pkl')

		return {'complex':{'quant':comb_mquant_complex,'stoichiometry':comb_mstoch_complex},
				'corum':{'quant':comb_mquant_corum,'stoichiometry':comb_mstoch_corum},
				'pathway':{'quant':comb_mquant_pathway,'stoichiometry':comb_mstoch_pathway},
				'loc':{'quant':comb_mquant_loc,'stoichiometry':comb_mstoch_loc},
				'string':{'quant':comb_mquant_string},
				'all':{'quant':comb_all_quant}}

	@staticmethod
	def load_sex_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		sex_all_quant = data[data.covariates=='sex']

		mquant_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','quant','sex', n_subunit = su)
		mstoch_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','stoichiometry','sex', n_subunit = su)
		mquant_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','quant','sex', n_subunit = su)
		mstoch_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','stoichiometry','sex', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_pathway = mquant_pathway[mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_pathway = mstoch_pathway[mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_loc = mquant_loc[mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_loc = mstoch_loc[mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant','sex'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_string = DataFrameAnalyzer.open_in_chunks(folder, filename)

		sex_dict = {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
					'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
					'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
					'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
					'string':{'quant':mquant_string},
					'all':{'quant':sex_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(sex_dict, output_folder + 'sex_effect_dictionary.pkl')


		return {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
				'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
				'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
				'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
				'string':{'quant':mquant_string},
				'all':{'quant':sex_all_quant}}

	@staticmethod
	def load_diet_data(su):
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		fname = 'protein_variation.tsv.gz'
		data = DataFrameAnalyzer.open_in_chunks(folder, fname)
		diet_all_quant = data[data.covariates=='diet']

		mquant_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','quant','diet', n_subunit = su)
		mstoch_complex = figure6a.load_multivariate_module_specific_data('gygi3',
						 'complex','stoichiometry','diet', n_subunit = su)
		mquant_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','quant','diet', n_subunit = su)
		mstoch_corum = figure6a.load_multivariate_module_specific_data('gygi3',
					   'corum','stoichiometry','diet', n_subunit = su)

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/pathway/'
		joint_name = '_'.join(['gygi3','pathway','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_pathway = mquant_pathway[mquant_pathway['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','pathway','stoichiometry','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_pathway = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_pathway = mstoch_pathway[mstoch_pathway['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/loc/'
		joint_name = '_'.join(['gygi3','loc','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mquant_loc = mquant_loc[mquant_loc['n.subunits']>=su]
		joint_name = '_'.join(['gygi3','loc','stoichiometry','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mstoch_loc = DataFrameAnalyzer.open_in_chunks(folder, filename)
		mstoch_loc = mstoch_loc[mstoch_loc['n.subunits']>=su]

		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/string/'
		joint_name = '_'.join(['gygi3','string','quant','diet'])
		filename = joint_name + '_data_all_multi_covariate_subunitSpecific_empiricalFDR_woCORUM.tsv.gz'
		mquant_string = DataFrameAnalyzer.open_in_chunks(folder, filename)

		diet_dict = {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
					 'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
					 'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
					 'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
					 'string':{'quant':mquant_string},
					 'all':{'quant':diet_all_quant}}
		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/data/expl_variance/'					 
		DataFrameAnalyzer.to_pickle(diet_dict, output_folder + 'diet_effect_dictionary.pkl')

		return {'complex':{'quant':mquant_complex,'stoichiometry':mstoch_complex},
				'corum':{'quant':mquant_corum,'stoichiometry':mstoch_corum},
				'pathway':{'quant':mquant_pathway,'stoichiometry':mstoch_pathway},
				'loc':{'quant':mquant_loc,'stoichiometry':mstoch_loc},
				'string':{'quant':mquant_string},
				'all':{'quant':diet_all_quant}}

	@staticmethod
	def export_considered_pathways():
		#pathways are considered when not different from random, but different from complex (<0.5)
		folder = '/g/scb2/bork/romanov/wpc/covariate_ml_gygi3/'
		df = DataFrameAnalyzer.getFile(folder,'corr_classification_pathways.tsv')
		sub = df[df.pval1>0.1]
		sub = sub[sub.pval1_complex<0.1]
		considered_pathways = list(sub.index)

		df.to_csv('/g/scb2/bork/romanov/wpc/wpc_package/data/corr_classification_pathways.tsv.gz', sep = '\t', compression = 'gzip')
		return considered_pathways

	@staticmethod
	def get_considered_pathways(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		df = DataFrameAnalyzer.getFile(folder,'data/corr_classification_pathways.tsv.gz')
		sub = df[df.pval1>0.1]
		sub = sub[sub.pval1_complex<0.1]
		considered_pathways = list(sub.index)		
		return considered_pathways

	@staticmethod
	def plot(considered_pathways, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		comb_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/combined_dictionary.pkl')
		sex_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/sex_effect_dictionary.pkl')
		diet_dict = DataFrameAnalyzer.read_pickle(folder + 'data/expl_variance/diet_effect_dictionary.pkl')

		#comb_dict = figure6a.load_combined_data(5)
		#sex_dict = figure6a.load_sex_data(5)
		#diet_dict = figure6a.load_diet_data(5)

		for key in ['all','complex','pathway','loc','string']:
			if key == 'all':
				quant_sex_all = list(set(sex_dict[key]['quant']['r2.all.module']))
				quant_diet_all = list(set(diet_dict[key]['quant']['r2.all.module']))
				quant_comb_all = list(set(comb_dict[key]['quant']['r2.all.module']))
			elif key == 'complex':
				quant_sex_complex = list(set(sex_dict[key]['quant']['r2.all.module']))
				stoch_sex_complex = list(set(sex_dict[key]['stoichiometry']['r2.all.module']))
				quant_diet_complex = list(set(diet_dict[key]['quant']['r2.all.module']))
				stoch_diet_complex = list(set(diet_dict[key]['stoichiometry']['r2.all.module']))
				quant_comb_complex = list(set(comb_dict[key]['quant']['r2.all.module']))
				stoch_comb_complex = list(set(comb_dict[key]['stoichiometry']['r2.all.module']))

				quant_sex_corum = list(set(sex_dict['corum']['quant']['r2.all.module']))
				stoch_sex_corum = list(set(sex_dict['corum']['stoichiometry']['r2.all.module']))
				quant_diet_corum = list(set(diet_dict['corum']['quant']['r2.all.module']))
				stoch_diet_corum = list(set(diet_dict['corum']['stoichiometry']['r2.all.module']))
				quant_comb_corum = list(set(comb_dict['corum']['quant']['r2.all.module']))
				stoch_comb_corum = list(set(comb_dict['corum']['stoichiometry']['r2.all.module']))
			elif key == 'pathway':
				mquant_sex = sex_dict[key]['quant']
				mquant_sex = mquant_sex[~mquant_sex['complex.name'].isin(considered_pathways)]
				quant_sex_pathway = list(set(mquant_sex['r2.all.module']))
				mstoch_sex = sex_dict[key]['stoichiometry']
				mstoch_sex = mstoch_sex[~mstoch_sex['complex.name'].isin(considered_pathways)]
				stoch_sex_pathway = list(set(mstoch_sex['r2.all.module']))
				mquant_diet = diet_dict[key]['quant']
				mquant_diet = mquant_diet[~mquant_diet['complex.name'].isin(considered_pathways)]
				quant_diet_pathway = list(set(mquant_diet['r2.all.module']))
				mstoch_diet = diet_dict[key]['stoichiometry']
				mstoch_diet = mstoch_diet[~mstoch_diet['complex.name'].isin(considered_pathways)]
				stoch_diet_pathway = list(set(mstoch_diet['r2.all.module']))
				mquant_comb = comb_dict[key]['quant']
				mquant_comb = mquant_comb[~mquant_comb['complex.name'].isin(considered_pathways)]
				quant_comb_pathway = list(set(mquant_comb['r2.all.module']))
				mstoch_comb = comb_dict[key]['stoichiometry']
				mstoch_comb = mstoch_comb[~mstoch_comb['complex.name'].isin(considered_pathways)]
				stoch_comb_pathway = list(set(mstoch_comb['r2.all.module']))
			elif key == 'loc':
				quant_sex_loc = list(set(sex_dict[key]['quant']['r2.all.module']))
				stoch_sex_loc = list(set(sex_dict[key]['stoichiometry']['r2.all.module']))
				quant_diet_loc = list(set(diet_dict[key]['quant']['r2.all.module']))
				stoch_diet_loc = list(set(diet_dict[key]['stoichiometry']['r2.all.module']))
				quant_comb_loc = list(set(comb_dict[key]['quant']['r2.all.module']))
				stoch_comb_loc = list(set(comb_dict[key]['stoichiometry']['r2.all.module']))
			elif key == 'string':
				quant_sex_string = list(set(sex_dict[key]['quant']['r2.all.module']))
				quant_diet_string = list(set(diet_dict[key]['quant']['r2.all.module']))
				quant_comb_string = list(set(comb_dict[key]['quant']['r2.all.module']))

		r2_quant_sex_complex = quant_sex_complex# + quant_sex_corum
		r2_stoch_sex_complex = stoch_sex_complex# + stoch_sex_corum
		r2_quant_diet_complex = quant_diet_complex# + quant_diet_corum
		r2_stoch_diet_complex = stoch_diet_complex# + stoch_diet_corum
		r2_quant_comb_complex = quant_comb_complex# + quant_comb_corum
		r2_stoch_comb_complex = stoch_comb_complex# + stoch_comb_corum
		r2_quant_sex_complex = quant_sex_complex + quant_sex_pathway
		r2_stoch_sex_complex = stoch_sex_complex + stoch_sex_pathway
		r2_quant_diet_complex = quant_diet_complex + quant_diet_pathway
		r2_stoch_diet_complex = stoch_diet_complex + stoch_diet_pathway
		r2_quant_comb_complex = quant_comb_complex + quant_comb_pathway
		r2_stoch_comb_complex = stoch_comb_complex + stoch_comb_pathway

		r2_complex_list = [r2_quant_sex_complex, r2_stoch_sex_complex,
						   r2_quant_diet_complex, r2_stoch_diet_complex,
						   r2_quant_comb_complex, r2_stoch_comb_complex]
		r2_pathway_list = [quant_sex_pathway, stoch_sex_pathway,
						   quant_diet_pathway, stoch_diet_pathway,
						   quant_comb_pathway, stoch_comb_pathway]
		r2_string_list = [quant_sex_string, quant_diet_string, quant_comb_string]
		r2_all_list = [quant_sex_all, quant_diet_all, quant_comb_all]

		r2_list = [r2_stoch_comb_complex, r2_quant_comb_complex, quant_comb_all,
				   r2_stoch_diet_complex, r2_quant_diet_complex, quant_diet_all,
				   r2_stoch_sex_complex, r2_quant_sex_complex, quant_sex_all]

		label_all_list = ['qsex_all','qdiet_all','qcomb_all']
		label_complex_list = ['qsex_complex','ssex_complex', 
							  'qdiet_complex', 'sdiet_complex',
					  		  'qcomb_complex','scomb_complex']
		label_pathway_list = ['qsex_pathway','ssex_pathway',
							  'qdiet_pathway','sdiet_pathway',
							  'qcomb_pathway','scomb_pathway']
		label_string_list = ['qsex_string','qdiet_string','qcomb_string']
		label_list = ['scomb_complex','qcomb_complex','qcomb_all',
					  'sdiet_complex','qdiet_complex','qdiet_all',
					  'ssex_complex','qsex_complex','qsex_all']

		color_list = ['lightblue','blue','lightgreen','green','grey','black']
		big_color_list = ['black','grey','grey','green','lightgreen','lightgreen','blue','lightblue','lightblue']
		small_color_list = ['lightblue','lightgreen','grey']

		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (10,10))
		#gs = gridspec.GridSpec(15,15)
		gs = gridspec.GridSpec(9,9)
		
		'''
		ax = plt.subplot(gs[0:2,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_all_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_all_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(small_color_list[i])
		plt.yticks(list(xrange(len(label_all_list))))
		ax.set_yticklabels(label_all_list)
		'''

		ax = plt.subplot(gs[0:6,0:])
		ax.set_xlim(-0.01,0.4)
		bp = ax.boxplot(r2_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.8]*len(r2_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(big_color_list[i])
		plt.yticks(list(xrange(len(label_list))))
		ax.set_yticklabels(label_list)
		print(scipy.stats.f_oneway(r2_list[0]+r2_list[1],r2_list[2])[1])
		print(scipy.stats.f_oneway(r2_list[3]+r2_list[4],r2_list[5])[1])
		print(scipy.stats.f_oneway(r2_list[6]+r2_list[7],r2_list[8])[1])
		ax.axvline(0.1, color = 'black', linestyle = '--')
		ax.axvline(0.2, color = 'black', linestyle = '--')
		ax.axvline(0.3, color = 'black', linestyle = '--')
		'''
		ax = plt.subplot(gs[6:10,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_pathway_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_pathway_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(color_list[i])
		plt.yticks(list(xrange(len(label_pathway_list))))
		ax.set_yticklabels(label_pathway_list)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[10:12,0:])
		ax.set_xlim(-0.01,0.3)
		bp = ax.boxplot(r2_string_list,notch=0,sym="",vert=0,patch_artist=True,
			widths=[0.4]*len(r2_string_list))
		plt.setp(bp['medians'], color="black")
		plt.setp(bp['whiskers'], color="black",linestyle="--",alpha=0.8)
		for i,patch in enumerate(bp['boxes']):
			patch.set_edgecolor("black")
			patch.set_alpha(0.6)
			patch.set_color(small_color_list[i])
		plt.yticks(list(xrange(len(label_string_list))))
		ax.set_yticklabels(label_string_list)
		ax.set_xticklabels([])
		'''

		ax = plt.subplot(gs[6:9,0:])
		ax.axis('off')
		plt.savefig(folder + 'figures/fig6a_combinedEffect_analysis.pdf',
					bbox_inches = 'tight', dpi = 400)

class figure6c:
	@staticmethod
	def execute(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('FIGURE6C: plot_effectSize_complexDistribution')
		figure6c.plot_effectSize_complexDistribution(folder = folder)

	@staticmethod
	def export_data():
		folder = '/g/scb2/bork/romanov/wpc/'
		mouse_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv')
		mouse_pval_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv')
		human_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv')
		human_pval_df = DataFrameAnalyzer.getFile(folder,'suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv')


		output_folder = '/g/scb2/bork/romanov/wpc/wpc_package/'
		mouse_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		mouse_pval_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		human_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv.gz', sep = '\t', compression = 'gzip')
		human_pval_df.to_csv(output_folder + 'data/suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv.gz', sep = '\t', compression = 'gzip')

		return {'mouse': (mouse_df, mouse_pval_df),
				'human': (human_df, human_pval_df)}

	@staticmethod
	def get_data(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		mouse_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_gygi3_complex_effectSizeMatrix.tsv.gz')
		mouse_pval_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_gygi3_complex_pvalMatrix.tsv.gz')
		human_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_battleProtein_complex_effectSizeMatrix.tsv.gz')
		human_pval_df = DataFrameAnalyzer.open_in_chunks(folder,'data/suppFigure4a_underlyingData_battleProtein_complex_pvalMatrix.tsv.gz')
		return {'mouse': (mouse_df, mouse_pval_df),
				'human': (human_df, human_pval_df)}		

	@staticmethod
	def plot_effectSize_complexDistribution(**kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		data_dict = figure6c.get_data()
		mouse_df, mouse_pval_df = data_dict['mouse']
		human_df, human_pval_df = data_dict['human']

		mouse_df.columns = ['mouse_'+item for item in list(mouse_df.columns)]
		mouse_pval_df.columns = ['mouse_'+item for item in list(mouse_pval_df.columns)]
		human_df.columns = ['human_'+item for item in list(human_df.columns)]
		human_pval_df.columns = ['human_'+item for item in list(human_pval_df.columns)]

		human_dict_quant = human_df['human_sex_quant'].to_dict()
		human_dict_stoch = human_df['human_sex_stoch'].to_dict()
		human_dict_pval_quant = human_pval_df['human_sex_quant'].to_dict()
		human_dict_pval_stoch = human_pval_df['human_sex_stoch'].to_dict()

		human_quant_list = list()
		human_stoch_list = list()
		human_quant_pvallist = list()
		human_stoch_pvallist = list()
		for key in list(mouse_df.index):
			try:
				human_quant_effectSize = human_dict_quant[key]
				human_quant_pval = human_dict_pval_quant[key]
			except:
				human_quant_effectSize = np.nan
				human_quant_pval = np.nan
			try:
				human_stoch_effectSize = human_dict_stoch[key]
				human_stoch_pval = human_dict_pval_stoch[key]
			except:
				human_stoch_effectSize = np.nan
				human_stoch_pval = np.nan
			human_quant_list.append(human_quant_effectSize)
			human_stoch_list.append(human_stoch_effectSize)
			human_quant_pvallist.append(human_quant_pval)
			human_stoch_pvallist.append(human_stoch_pval)
		'''
		mouse_df['human_sex_quant'] = pd.Series(human_quant_list, index = mouse_df.index)
		mouse_df['human_sex_stoch'] = pd.Series(human_stoch_list, index = mouse_df.index)
		mouse_pval_df['human_sex_quant'] = pd.Series(human_quant_pvallist, index = mouse_df.index)
		mouse_pval_df['human_sex_stoch'] = pd.Series(human_stoch_pvallist, index = mouse_df.index)
		'''

		mouse_df = mouse_df.replace(np.nan,-100)
		mdf, proteinList = utilsFacade.recluster_matrix_only_rows(mouse_df)
		mdf = mdf.replace(-100, np.nan)
		mdf = mdf.T

		ranked_sorted_list = list()
		for i,row in mdf.iterrows():
			temp = list()
			for item in list(row):
				if str(item)!='nan':
					temp.append(item)
			ranked_temp = rankdata(temp)
			rank_dict = dict()
			for t,r in zip(temp, ranked_temp):
				rank_dict[t] = r
			final_temp = list()
			for t in list(row):
				if str(t)!='nan':
					final_temp.append(rank_dict[t])
				else:
					final_temp.append(np.nan)
			ranked_sorted_list.append(final_temp)
		ranked_df = pd.DataFrame(ranked_sorted_list)
		ranked_df.index = mdf.index
		ranked_df.columns = mdf.columns

		sex_quant_list = [item*100 for item in list(mdf.T['mouse_sex_quant'])]
		sex_stoch_list = [item*100 for item in list(mdf.T['mouse_sex_stoch'])]
		sex_sum_list =  np.array(sex_quant_list) + np.array(sex_stoch_list)
		diet_quant_list = [item*100 for item in list(mdf.T['mouse_diet_quant'])]
		diet_stoch_list = [item*100 for item in list(mdf.T['mouse_diet_stoch'])]
		key_list = list(mdf.columns)
		lists = [sex_sum_list, sex_quant_list, sex_stoch_list, diet_quant_list, diet_stoch_list, key_list]
		sorted_lists = utilsFacade.sort_multiple_lists(lists, reverse = True)
		sex_sum_list, sex_quant_list, sex_stoch_list, diet_quant_list, diet_stoch_list, key_list = sorted_lists
		ranked_df = ranked_df[key_list]


		sns.set(context='notebook', style='white', 
			palette='deep', font='Liberation Sans', font_scale=1, 
			color_codes=False, rc=None)
		plt.rcParams["axes.grid"] = False

		plt.clf()
		fig = plt.figure(figsize = (17,10))
		gs = gridspec.GridSpec(10,32)
		ax = plt.subplot(gs[0:4,0:])
		ax.axhline(10,color = 'k', linestyle = '--')
		ax.axhline(20,color = 'k', linestyle = '--')
		ax.axhline(30,color = 'k', linestyle = '--')
		ax.axhline(40,color = 'k', linestyle = '--')
		ax.axhline(50,color = 'k', linestyle = '--')
		ind = list(xrange(len(sex_quant_list)))
		width = 1
		rects = ax.bar(ind, sex_quant_list, width, color='lightblue',
			edgecolor = 'white')
		rects = ax.bar(ind, sex_stoch_list, width, color='darkblue',
			edgecolor = 'white', bottom = np.array(sex_quant_list))

		ax.set_xlim(-0.5,len(sex_quant_list)+0.5)
		ax.set_xticklabels([])

		ax = plt.subplot(gs[4:8,0:])
		ax.set_ylim(-60,0)
		ax.axhline(-10,color = 'k', linestyle = '--')
		ax.axhline(-20,color = 'k', linestyle = '--')
		ax.axhline(-30,color = 'k', linestyle = '--')
		ax.axhline(-40,color = 'k', linestyle = '--')
		ax.axhline(-50,color = 'k', linestyle = '--')		
		ind = list(xrange(len(sex_quant_list)))
		width = 1
		rects = ax.bar(ind, (-1)*np.array(diet_quant_list), width, color='lightgreen',
			edgecolor = 'white')
		rects = ax.bar(ind, (-1)*np.array(diet_stoch_list), width, color='darkgreen',
			edgecolor = 'white', bottom = (-1)*np.array(diet_quant_list))
		ax.set_xlim(-0.5,len(diet_quant_list)+0.5)
		plt.xticks(list(utilsFacade.frange(0.5,len(ranked_df.columns)+0.5,1)))
		ax.set_xticklabels([':'.join(item.split(':')[1:]) for item in list(ranked_df.columns)],
						   rotation = 90, fontsize = 5)
		plt.savefig(folder + 'figures/fig6b_complex_effectSize_Distribution.pdf',
					bbox_inches = 'tight', dpi = 400)

class main_figures(object):
	def __init__(self, **kwargs):
		folder = kwargs.get('folder','/g/scb2/bork/romanov/wpc/wpc_package/')
		output_folder = kwargs.get('output_folder','/g/scb2/bork/romanov/wpc/wpc_package/')

		print('*****************************')
		print('FIGURE1')
		print('*****************************')
		fig1 = figure1.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('FIGURE2A')
		print('*****************************')
		fig2a = figure2a.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE2B')
		print('*****************************')
		fig2b = figure2b.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE2C_ADDENDUM')
		print('*****************************')
		fig2c = figure2c_addendum.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE3')
		print('*****************************')
		fig3 = figure3.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE4A')
		print('*****************************')
		fig4a = figure4a.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE4B')
		print('*****************************')
		fig4b = figure4b.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE5A')
		print('*****************************')
		fig5a = figure5a.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE5B')
		print('*****************************')
		fig5b = figure5b.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE5C_ADDENDUM')
		print('*****************************')
		fig5c = figure5c_addendum.execute(folder = folder, output_folder = output_folder)
		
		print('*****************************')
		print('FIGURE6A')
		print('*****************************')
		fig6a = figure6a.execute(folder = folder, output_folder = output_folder)

		print('*****************************')
		print('FIGURE6C')
		print('*****************************')
		fig6c = figure6c.execute(folder = folder, output_folder = output_folder)

main_fig = main_figures()

