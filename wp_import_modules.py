
from statsmodels.distributions.empirical_distribution import ECDF #empirical cumulative distribution function
from scipy.special import chdtrc as chi2_cdf
import json
import urllib2
import mygene
import requests
import pandas as pd
import seaborn as sns
from textwrap import wrap
import numpy
import time
import random
import os
import gzip
import argparse
import tempfile
from os import system
import statsmodels
import itertools
import os.path
import sqlite3
import sys
import operator
import scipy
import scipy.stats.stats as stats
from matplotlib import pyplot as plt
from scipy.stats import gaussian_kde
import scipy.cluster.hierarchy as sch
from statsmodels.sandbox.stats import multicomp
from matplotlib import gridspec
import matplotlib as mpl
mpl.rcParams["axes.grid"]=True;mpl.rcParams["axes.facecolor"]="#E7EBEF"
mpl.rcParams["grid.color"]="white";mpl.rcParams["grid.linestyle"]="solid";mpl.rcParams["grid.alpha"]=0.5
import cPickle as pickle
from numpy import trapz
from scipy.spatial import distance
from scipy.cluster import hierarchy
from collections import defaultdict
from matplotlib.colors import ListedColormap
from scipy.cluster.hierarchy import dendrogram, fcluster, leaves_list, set_link_color_palette
from operator import itemgetter
from itertools import groupby
import numpy as np
from matplotlib.mlab import PCA as mlabPCA
from sklearn.decomposition import PCA as sklearnPCA
from mpl_toolkits.mplot3d import Axes3D
from statsmodels.sandbox.stats import multicomp
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.lines import Line2D 
from scipy.spatial.distance import pdist
from scipy.cluster.hierarchy import linkage
import glob
import rpy2

import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
import rpy2.rpy_classic as rpy
#import pandas.rpy.common as com
from rpy2.robjects import pandas2ri
from rpy2.robjects.vectors import FloatVector

import scipy.cluster.hierarchy as hier
import scipy.spatial.distance as dist
from collections import defaultdict
from matplotlib.colors import rgb2hex, colorConverter
import cPickle
import matplotlib.colors as colors
import mygene
from matplotlib.backends.backend_pdf import PdfPages
from scipy.stats import rankdata
from numpy import std, mean, sqrt
from matplotlib.patches import Circle, PathPatch
from matplotlib.path import Path
from matplotlib.colors import ColorConverter
from matplotlib.pyplot import gca
import matplotlib_venn

